/** Environment controller **/

import { models, JS } from '../models';

const Environment = models.environment;

export function getAllEnvironments(request, reply) {
	Environment.findAll({
		include: {
			model: Environment,
			as: 'siblings',
			attributes: ['name', 'url', 'staticURL'],
			through: {
				attributes: []
			},
			required: false
		}
	}).then((environments) => reply(environments));
}

export function getEnvironmentById(request, reply) {
	Environment.findById(request.params.id).then((environment) => reply(environment));
}

export function createEnvironment(request, reply) {
	Environment.create(request.payload).then((environment) => reply(environment));
}

export function updateEnvironment(request, reply) {
	Environment.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));
}

export function deleteEnvironment(request, reply) {
	Environment.destroy({
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));
}