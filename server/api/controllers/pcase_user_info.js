/** Branch controller **/

import { models, JS } from '../models';

const PCaseUserInfo = models.pcase_user_info;

export function getAllPCaseUserInfo(request, reply) {
	PCaseUserInfo.findAll().then((pcase_user_info) => reply(pcase_user_info));
}

export function getAllPCaseUserInfoByUser(request, reply) {
	PCaseUserInfo.findAll({
		where: {
			user_id: request.params.user_id
		}
	}).then((pcase_user_info) => reply(pcase_user_info));
}

export function createPCaseUserInfo(request, reply) {
	PCaseUserInfo.create(request.payload).then((pcase_user_info) => reply(pcase_user_info));
}

export function updatePCaseUserInfo(request, reply) {
	PCaseUserInfo.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));	
}