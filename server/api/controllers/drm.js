const request = require('request');
import HapiConfig from '../../cfg/HapiConfig';

const configDrm = HapiConfig.drm;
const env = 'dev';
const userId = env === 'prod' ? 'samba' : 'smbUserTest';

export function createSession(request, reply) {
	makeRequest(`/CreateSession?CrmId=${configDrm.crmId}&UserId=${userId}`, request,
		(body) => reply(body).type('text/xml'),
		(error) => reply(error), 'post');
}

export function authorize(request, reply) {
	makeRequest(`/Authorize?CrmId=${configDrm.crmId}&AccountId=${configDrm.AccountId}`, request,
		(body) => reply({success: true}),
		(error) => reply(error), 'post');
}

export function deauthorize(request, reply) {
	makeRequest(`/Authorize?CrmId=${configDrm.crmId}&AccountId=${configDrm.AccountId}`, request,
		(body) => reply({success: true}),
		(error) => reply(error), 'post');
}

export function querySessionAuthorization(request, reply) {
	makeRequest(`/QuerySessionAuthorization?CrmId=${configDrm.CrmId}&AccountId=${configDrm.AccountId}` +
		`&ContentId=${request.params.contentId}&SessionId=${request.params.sessionId}`,
		(body) => reply({success: true}),
		(error) => reply(error), 'post');
}

export function deleteSession(request, reply) {
	makeRequest(`/DeleteSession?SessionId=${request.params.sessionId}`,
		(body) => reply({success: true}),
		(error) => reply(error), 'post');
}

/* Request helper */
function makeRequest(endpoint, request, successHandler, errorHandler, type) {
	const baseUrl = configDrm[env === 'prod' ? 'baseUrl' : 'baseUrlStaging'];

	request[type]({
		url: `${baseUrl}${endpoint}&${getQueryString(request)}`,
		headers: {
			'MAN-user-id': configDrm.user,
			'MAN-user-password': configDrm.password
		}
	}, function(er, response, body) {
		if (er || response.statusCode < 200 || response.statusCode > 200) {
			errorHandler(`<pre>${new Error(`${response.statusCode}: ${response.statusMessage}`).stack}</pre>`);
			return;
		}

		successHandler(body);
	});
}

function getQueryString(request) {
	let qs = '';
	let u = '';

	for (let k in request.query)
		(qs += `${u}${k}=${request.query[k]}`, u = '&');

	return qs;
}
