/** Branch controller **/

import { models, JS } from '../models';

const Browser = models.browser;

export function getAllBrowsers(request, reply) {
	Browser.findAll().then((browsers) => reply(browsers));
}

export function getBrowserById(request, reply) {
	Browser.findById(request.params.id).then((browser) => reply(browser));
}

export function createBrowser(request, reply) {
	Browser.create(request.payload).then((browser) => reply(browser));
}

export function updateBrowser(request, reply) {
	Browser.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));	
}

export function deleteBrowser(request, reply) {
	Browser.destroy({
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));
}