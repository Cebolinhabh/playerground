/** Auth controller **/

import { models } from '../models';
import { findOrCreateUser } from './user';
import HapiConfig from '../../cfg/HapiConfig';

const User = models.user;
const Roles = HapiConfig.github.roles;
const BasicAuthUser = HapiConfig.basicauth;

export function githubAuthHandler(request, reply) {
    if (request.auth.isAuthenticated) {

        const { login, name, avatar_url, email, id, company } = request.auth.credentials.profile.raw;

		let allowed = false;
		let role;
		Object.keys(Roles).forEach((key) => {
			if(Roles[key].indexOf(id) > -1) {
				allowed = true;
				role = key;
			}
		});

		if(!allowed) {
			//reply('Seu GithubID não é permitido. Por favor contate o administrador').code(401);
			reply.redirect('/login?auth_error=github_unauthorized');
			return;
		}

        const user = {
        	name: name || login,
        	avatar_url,
        	email: email || '@sambatech.com.br',
			github_id: id,
			password: 'samba123',
			role: role.toUpperCase()
        };

        //Creates a user
        findOrCreateUser(user).spread((user, created) => {
        	request.cookieAuth.set({ sid: user.github_id });
        	reply.redirect('/');
        }).catch(error => {
			reply(`Unexpected error: \n ${JSON.stringify(error.errors)}`).code(400);
		});
    } else {
    	reply('Not logged in...').code(401);
    }
}

export function getUserByGithubId(id) {
	return User.findOne({
		where: {
			github_id: id
		},
		attributes: ['id', 'github_id', 'email', 'avatar_url', 'name', 'role']
	});
}

export function githubAuthLogoutHandler(request, reply) {
    request.cookieAuth.clear();
    reply.redirect('/login');
}

//TODO: Melhor lugar?
export function basicAuth(request, username, password, callback) {
	const user = BasicAuthUser[username];

	if(!user) {
		return callback(null, false);
	}

	if(password === user.password) {
		return callback(null, true, user);
	}else {
		return callback(null, false);
	}

}

export function simpleLoginAuthHandler(request, reply) {
	const pl = request.payload;

	User.findOne({
		where: {
			email: pl.email
		}
	}).then((user)=> {
		if(user && user.password === pl.password) {
			request.cookieAuth.set({ sid: user.github_id });
			reply({
				success: true,
				user
			});
		}else {
			reply({
				success: false,
				error: 'INVALID_USER'
			}).code(403);
		}
	});
}