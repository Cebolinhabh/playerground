/** Branch controller **/

import { models, JS } from '../models';

const Branch = models.branch;

export function getAllBranches(request, reply) {
	Branch.findAll().then((branches) => reply(branches));
}

export function getBranchById(request, reply) {
	Branch.findById(request.params.id).then((branch) => reply(branch));
}

export function createBranch(request, reply) {
	Branch.create(request.payload).then((branch) => reply(branch));
}

export function updateBranch(request, reply) {
	Branch.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));	
}

export function deleteBranch(request, reply) {
	Branch.destroy({
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));	
}

