/** Label controller **/

import { models, JS } from '../models';

const Label = models.label;
const Pcase = models.pcase;
const PCaseUserInfo = models.pcase_user_info;
const Category = models.category;

export function getAllLabels(request, reply) {
	Label.findAll().then((labels) => reply(labels));
}

export function getLabelById(request, reply) {
	Label.findById(request.params.id, {
        include: [{
            model: Pcase,
			where: {
				environment_id: request.query.env
			},
            attributes: ['id', 'title', 'created_at', 'environment_id', 'description'],
            through: {
                attributes: []
            },
			required: false,
			include: [
				{
					model: PCaseUserInfo,
					where: {
						user_id: (request.auth.isAuthenticated == true) ? request.auth.credentials.id : null
					},
					attributes: ['status', 'favorite', 'updated_at'],
					required: false
				}, {
					model: Category,
					attributes: ['slug']
				}, {
					model: Label,
					attributes: ['id', 'name', 'color'],
					through: {
						attributes: []
					},
					required: false,
				}
			]
        }]
    }).then((label) => reply(label));
}

export function createLabel(request, reply) {
	Label.create(request.payload).then((label) => reply(label));
}

export function updateLabel(request, reply) {
	Label.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));
}

export function deleteLabel(request, reply) {
	Label.destroy({
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));
}