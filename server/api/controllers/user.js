/** Environment controller **/

import { models, sequelize, JS } from '../models';

const User = models.user;
const PCaseUserInfo = models.pcase_user_info;

export function getAllUsers(request, reply) {
	User.findAll().then((users) => reply(users));
}

export function getUserById(request, reply) {
	User.findById(request.params.id).then((user) => reply(user));
}

export function createUser(request, reply) {
	User.create(request.payload).then((user) => reply(user));
}

export function updateUser(request, reply) {
	User.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));
}

export function deleteUser(request, reply) {
	User.destroy({
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));
}

export function findOrCreateUser(user) {
	return User.findOrCreate({
		where: {
			github_id: user.github_id
		},
		defaults: user
	});
}

export function resetAllPCaseUserInfoStatusByUserId(request, reply) {

	PCaseUserInfo.update({
		status: 'PENDING'
	}, {
		where: {
			user_id: request.params.id
		}
	}).then(function () {
		return reply(request.payload);
	});

}
