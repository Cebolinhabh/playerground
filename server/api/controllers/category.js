/** Category controller **/

import { models, sequelize, JS } from '../models';

const Category = models.category;
const PCase = models.pcase;
const PCaseUserInfo = models.pcase_user_info;
const Label = models.label;

export function getAllCategories(request, reply) {
	const env = request.query.env ? {environment_id: request.query.env} : {};
	let include = {
		model: PCase,
		attributes: ['id', 'title']
	};
	if(request.query.env)
		include = Object.assign(include, {where: {environment_id: request.query.env}, required: false});

	Category.findAll({
		include: [include, {
			model: PCaseUserInfo,
			attributes: ['status'],
			where: {
				user_id: (request.auth.isAuthenticated == true) ? request.auth.credentials.id : null
			},
			required: false
		}]
	}).then((categories) => reply(categories));
}

export function getCategoryBySlug(request, reply) {
	const env = request.query.env ? {environment_id: request.query.env} : {};
	let include = {
		model: PCase,
		attributes: ['id', 'title', 'created_at', 'environment_id', 'description'],
		where: {
			environment_id: request.query.env
		},
		include: [{
			model: PCaseUserInfo,
			where: {
				user_id: (request.auth.isAuthenticated == true) ? request.auth.credentials.id : null
			},
			attributes: ['status', 'favorite', 'updated_at'],
			required: false
		},{
			model: Label,
			attributes: ['id', 'name', 'color'],
			through: {
				attributes: []
			}
		}],
		required: false
	};

	if(request.query.env)
		include = Object.assign(include, {where: {environment_id: request.query.env}});

	Category.findOne({
		where: {
			slug: request.params.cat_slug
		},
		include: [include, {
			model: PCaseUserInfo,
			attributes: ['status'],
			where: {
				status: {
					$in: ['ERROR', 'SUCCESS', 'WARNING']
				},
				user_id: (request.auth.isAuthenticated == true) ? request.auth.credentials.id : null
			},
			required: false
		}],
		order: [[PCase, 'title']]
	}).then((category) => reply(category));
}

export function createCategory(request, reply) {
	if(request.auth.credentials.role != 'ADMIN') {
		reply({success: false, message: 'You cannot create category'}).code(403);
		return;
	}
	const category = {...request.payload, slug: makeSlug(request.payload.name)};
	Category.create(category).then((category) => reply(category));
}

export function updateCategory(request, reply) {
	if(request.auth.credentials.role != 'ADMIN') {
		reply({success: false, message: 'You cannot update category'}).code(403);
		return;
	}
	Category.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));
}

export function updatePCaseUserInfoByCategory(request, reply) {
	PCaseUserInfo.update(request.payload, {
		where: {
			category_id: request.params.id,
			user_id: request.auth.credentials.id
		}
	}).then(() => reply(request.payload));
}

export function deleteCategory(request, reply) {
	Branch.destroy({
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));
}

/**
 * Utilities
 */

//Make category name slug
function makeSlug(val, replaceBy) {
  let replaceByParam = replaceBy || '-';
  const mapaAcentosHex 	= {
  	a : /[\xE0-\xE6]/g,
  	A : /[\xC0-\xC6]/g,
  	e : /[\xE8-\xEB]/g,
  	E : /[\xC8-\xCB]/g,
  	i : /[\xEC-\xEF]/g,
  	I : /[\xCC-\xCF]/g,
  	o : /[\xF2-\xF6]/g,
  	O : /[\xD2-\xD6]/g,
  	u : /[\xF9-\xFC]/g,
  	U : /[\xD9-\xDC]/g,
  	c : /\xE7/g,
  	C : /\xC7/g,
  	n : /\xF1/g,
  	N : /\xD1/g,
  };

  for ( let letra in mapaAcentosHex ) {
  	const expressaoRegular = mapaAcentosHex[letra];
  	val = val.replace( expressaoRegular, letra );
  }

  val = val.toLowerCase();
  val = val.replace(/[^a-z0-9\-]/g, " ");

  val = val.replace(/ {2,}/g, " ");

  val = val.trim();
  val = val.replace(/\s/g, replaceByParam);

  return val;
}