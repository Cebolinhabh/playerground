/** Pcases controller **/
import { models, JS } from '../models';

const Pcase = models.pcase;
const Environment = models.environment;
const User = models.user;
const Label = models.label;
const PCaseUserInfo = models.pcase_user_info;

export function getAllPCases(request, reply) {
	Pcase.findAll().then(pcases => reply(pcases));
}

export function getPCaseById(request, reply) {
	Pcase.findById(request.params.id, {
		include: [
			{
				model: User,
				attributes: ['id', 'name', 'email']
			},
			{
				model: Environment,
				attributes: ['url', 'staticURL']
			},
			{
				model: Label,
				attributes: ['id'],
				through: {
					attributes: []
				}
			},
			{
				model: PCaseUserInfo,
				attributes: ['status', 'favorite', 'updated_at'],
				where: {
					user_id: (request.auth.isAuthenticated == true) ? request.auth.credentials.id : null
				},
				required: false
			}
			
		]
	}).then((pcase) => reply(pcase));	
}

export function getPCaseByIdAndEnvironmentAndCategory(request, reply) {
	Pcase.findOne({
		where: {
			id: request.params.id,
			environment_id: request.query.env_id || null,
			category_id: request.query.cat_id || null
		},
		include: [
			{
				model: User,
				attributes: ['id', 'name', 'email']
			},
			{
				model: Environment,
				attributes: ['url', 'staticURL']
			},
			{
				model: Label,
				attributes: ['id'],
				through: {
					attributes: []
				}
			},
			{
				model: PCaseUserInfo,
				attributes: ['status', 'favorite', 'updated_at'],
				where: {
					user_id: (request.auth.isAuthenticated == true) ? request.auth.credentials.id : null
				},
				required: false
			}
			
		]
	}).then((pcase) => reply(pcase).code(pcase ? 200 : 404));	
}

export function getPCaseByName(request, reply) {
	Pcase.findAll({
		where: {
			title: {
				$like: `%${request.query.q}%`
			},
			environment_id: request.query.env
		},
		attributes: ['id', 'title'],
		include: [{
			model: models.category,
			attributes: ['name', 'slug']
		}]
	}).then((pcases) => reply(pcases));	
}

export function getFavoritesPCase(request, reply) {
	Pcase.findAll({
			where: {
				environment_id: request.query.env
			},
			attributes: ['id', 'title'],
			include: [{
				model: models.category,
				attributes: ['name', 'slug']
			},{
				model: PCaseUserInfo,
				attributes: ['status', 'favorite', 'updated_at'],
				where: {
					user_id: request.auth.credentials.id,
					favorite: true
				},
				required: true
			}]
	}).then(pcases => reply(pcases));
}

export function createPCase(request, reply) {
	Pcase.create(request.payload).then((pcase) => reply(pcase));
}

export function updatePCase(request, reply) {
	Pcase.update(request.payload, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply(request.payload));	
}

export function updatePCaseUserInfo(request, reply) {
	PCaseUserInfo.findOrCreate({
		where: {
			user_id: request.auth.credentials.id,
			pcase_id: request.params.id
		},
		defaults: request.payload
	}).spread(function(pcase_user_info, created){
		if(!created) {
			PCaseUserInfo.update(request.payload, {
				where: {
					id: pcase_user_info.id
				}
			}).then(() => {
				reply(pcase_user_info);	
			});
		}else {
			reply(pcase_user_info);
		}
		
	});
}

export function setFavoritePCase(request, reply) {
	Pcase.update({
		star: request.params.flag
	}, {
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));	
}

export function deletePCase(request, reply) {
	Pcase.destroy({
		where: {
			id: request.params.id
		}
	}).then(()=> reply({success: true}));	
}

export function addLabelToPcase(request, reply) {
	Pcase.findById(request.params.id).then((pcase) => {
		pcase.addLabel(request.params.label_id).then((pc) => {
			reply(pc);
		});
	});
}

export function removeLabelFromPcase(request, reply) {
	Pcase.findById(request.params.id).then((pcase) => {
		pcase.removeLabel(request.params.label_id).then((pc) => {
			reply(pc);
		});
	});
}