/** Liquid controller **/
import { Medias, LiveChannels } from '../liquid/';
import { models, JS } from '../models';

const TYPES = {
	VOD: 'VOD',
	LIVE: 'LIVE'
};
const cache = 600000;

const Pcase = models.pcase;
const Environment = models.environment;
const mediasProd = new Medias('67a1d7ed-7dde-4387-a673-7cbf6b325ced', {
	cache
});
const mediasDev = new Medias('32ece55d-2993-4fed-92ec-3a02d3fa146c', {
	type: 'dev',
	cache
});
const liveChannelsProd = new LiveChannels('67a1d7ed-7dde-4387-a673-7cbf6b325ced', {
	cache
});
const liveChannelsDev = new LiveChannels('32ece55d-2993-4fed-92ec-3a02d3fa146c', {
	type: 'dev',
	cache
});

export async function getMediasByCategory(request, reply) {
	const pcases = await getPcasesByCategory(request.query.cat_id);
	const pcasesFromMedias = await getVODS(pcases);
	const pcasesFromLives = await getLives(pcases);

	reply([... pcasesFromLives, ...pcasesFromMedias]);
}

/**
 * Gets all the VOD medias from the playerground category
 * @param {Pcases} pcases Playerground pcases
 * @returns {PcasesFromMedias} pcasesFromMedias
 */
async function getVODS(pcases) {
	let pcasesFromMedias = [];
	let dataProd = [];
	let dataDev = [];

	try {
		const responseProd = await mediasProd.getMediasByProject(7648);
		dataProd = responseProd.data || [];
	} catch (error) {console.log(error)}

	try {
		const responseDev = await mediasDev.getMediasByProject(576);
		dataDev = responseDev.data || [];
	} catch (error) {}

	const medias = [...dataDev, ...dataProd];
	if(medias.length) {
		pcases.forEach(pcase => {
			const pcaseConfig = JSON.parse(pcase.code);
			const media = medias.find(media => media.id === pcaseConfig.m);

			//Duplicate to prod
			const env = pcase.environment.name.toUpperCase();
			if(media) pcasesFromMedias.push(pcaseFromMedia(pcase, media, env));
			if(env === 'STAGING')
				if(media) pcasesFromMedias.push(pcaseFromMedia(pcase, media, 'PROD'));
		});
	}
	return pcasesFromMedias;

	/**
	 * Returns a parsed Pcase merged with Media
	 *
	 * @param {Pcase} pcase Object pcase
	 * @param {Media} media Media from Liquid
	 * @param {Medias} mediaObject Liquid Instance
	 */
	function pcaseFromMedia(pcase, media, environment) {
		const pcaseConfig = JSON.parse(pcase.code);

		return {
			ph: pcaseConfig.ph,
			id: pcaseConfig.m,
			title: pcase.title,
			description: pcase.description,
			env: environment,
			type: TYPES.VOD,
			params: (Object.keys(pcaseConfig.playerParams).length > 0) ? pcaseConfig.playerParams : undefined,
			thumbnail: (media.thumbs && media.thumbs.length > 0) ? media.thumbs[media.thumbs.length - 1].url : undefined,
			qualifier: media.qualifier
		};
	}
}

/**
 * Gets all the LIVE medias from the playerground category
 * @param {Pcases} pcases Playerground pcases
 * @returns {PcasesFromMedias} pcasesFromMedias
 */
async function getLives(pcases) {
	let pcasesFromLives = [];
	let dataProd = [];
	let dataDev = [];

	try {
		const responseProd = await liveChannelsProd.getLiveChannels(7648);
		dataProd = responseProd.data || [];
	} catch (error) {}

	try {
		const responseDev = await liveChannelsDev.getLiveChannels(576);
		dataDev = responseDev.data || [];
	} catch (error) {}

	const lives = [...dataDev, ...dataProd];
	if(lives.length) {
		pcases.forEach(pcase => {
			const pcaseConfig = JSON.parse(pcase.code);
			const live = lives.find(live => live.id === pcaseConfig.live);
			if(live) pcasesFromLives.push(pcaseFromLive(pcase, live));

			if(!pcaseConfig.m)
				pcasesFromLives.push(pcaseFromLive(pcase));
		});
	}
	return pcasesFromLives;

	/**
	 * Returns a parsed Pcase merged with Media
	 *
	 * @param {Pcase} pcase Object pcase
	 * @param {Live} live Live from Liquid
	 * @param {Medias} mediaObject Liquid Instance
	 */
	function pcaseFromLive(pcase, live) {
		const pcaseConfig = JSON.parse(pcase.code);

		return {
			ph: pcaseConfig.ph,
			liveChannelId: pcaseConfig.live,
			title: pcase.title,
			description: pcase.description,
			env: pcase.environment.name.toUpperCase(),
			type: TYPES.LIVE,
			params: (Object.keys(pcaseConfig.playerParams).length > 0) ? pcaseConfig.playerParams : undefined,
			thumbnail: (live) ? live.thumbnailURL : (pcaseConfig.playerParams.thumbnailURL) ?
				pcaseConfig.playerParams.thumbnailURL : undefined
		};
	}
}

/** Retrieves from the database all pcases from that category
 * @param {Integer} category_id
 */
function getPcasesByCategory(cat_id) {
	return Pcase.findAll({
		where: {
			category_id: cat_id || null
		},
		include: [
			{
				model: Environment,
				attributes: ['id', 'name', 'url']
			}
		]
	})
}


