"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('browser', {
		id: {
			allowNull: true,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER,
			description: 'Browser`s id'
		},
		name: {
			type: DataTypes.STRING(64),
			allowNull: false,
			description: 'Browser`s name'
		},
		mobile: {
			type: DataTypes.BOOLEAN,
			defaulValue: false,
			allowNull: true,
			description: 'Browser mobile or not'
		},
		icon: {
			type: DataTypes.STRING(20),
			allowNull: true,
			description: 'Browser icon: see options'
		}
	}, {
		timestamps: false
	});
}