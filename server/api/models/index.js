"use strict";

import Sequelize from 'sequelize';
import fs from 'fs';
import path from 'path';
import JoiSequelize from 'joi-sequelize';
import HapiConfig from '../../cfg/HapiConfig';

const basename = path.basename(module.filename);
const env = 'development';
const log = true;
const db = {};

const sequelize = new Sequelize(HapiConfig.database['database'], HapiConfig.database['user'], HapiConfig.database['password'], {
	host: HapiConfig.database['host'],
	pool: {
		max: 50,
		min: 0,
		idle: 1000000
	},
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        underscored: true
    }
});

const models = {};
const JS = {};

/** Joi sequelize **/

/** Read dir **/
fs.readdirSync(path.join(__dirname, '../models')).filter(file => (
		(file.indexOf('.') !== 0) &&
		(file != basename) &&
		(file.slice(-3) === '.js') 
	)).forEach(file => {
		const model = sequelize.import(path.join(__dirname, '../models/' + file));
		models[model.name] = model;
		JS[model.name] = new JoiSequelize(require(path.join(__dirname, '../models/' + file)));
	});

/** Assocations **/
//Pcase
models.pcase.belongsTo(models.category);
models.pcase.belongsTo(models.user);
models.pcase.belongsTo(models.environment);

//Category	
models.category.hasMany(models.pcase);
//Environment
models.environment.hasMany(models.pcase);
//User
models.user.hasMany(models.pcase);

//PCase status
models.user.hasMany(models.pcase_user_info);
models.pcase.hasMany(models.pcase_user_info);
models.category.hasMany(models.pcase_user_info);

//Label Pcase
models.pcase.belongsToMany(models.label, {through: 'PcaseLabel'});
models.label.belongsToMany(models.pcase, {through: 'PcaseLabel'});

//Enviromnment
models.environment.belongsToMany(models.environment, {as: 'siblings', through: 'EnvironmentSiblings'});

/**Associate **/
Object.keys(db).forEach(modelName => {
	if(db[modelName].associate)
		db[modelName].associate(db);
});

/** AddScope **/
Object.keys(db).forEach(modelName => {
	if(db[modelName].addScopes)
		db[modelName].addScopes(db);

    if (db[modelName].addHooks) 
      	db[modelName].addHooks(db);
});

export { sequelize, JS, models };
