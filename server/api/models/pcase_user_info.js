"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('pcase_user_info', {
		id: {
			allowNull: true,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER,
			description: 'Pcase user info status id'
		},
		status: {
			type: DataTypes.ENUM('PENDING', 'WARNING', 'ERROR', 'SUCCESS'),
			allowNull: false,
			description: 'Pcase user info`s Status'
		},
		favorite: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			description: 'Pcase user info`s Favorite'
		}
	});
}