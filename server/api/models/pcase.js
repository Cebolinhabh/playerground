"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('pcase', {
			id: {
				allowNull: true,
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.INTEGER,
				description: 'Pcase`s id'
			},
			title: {
				type: DataTypes.STRING(64),
				allowNull: false,
				description: 'Pcase`s name'
			},
			description: {
				type: DataTypes.STRING(255),
				allowNull: true,
				description: 'Pcase´s description'
			},
			code: {
				type: DataTypes.TEXT,
				allowNull: true,
				description: 'Pcase´s code in case of api'
			},
			api: {
				type: DataTypes.BOOLEAN,
				allowNull: true,
				defaultValue: false,
				description: 'Pcase`s is api or not'
			},
			drm: {
				type: DataTypes.BOOLEAN,
				allowNull: true,
				defaultValue: false,
				description: 'Whether pcase has DRM security'
			},
			browsers: {
				type: DataTypes.STRING,
				allowNull: true,
				defaultValue: '',
				description: 'Pcase`s browser supported'
			},
			environment_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				description: 'Pcase`s enviroment'			
			},
			category_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				description: 'Pcase`s category'			
			},
			user_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				description: 'Pcase`s user'			
			},
			api_code: {
				type: DataTypes.TEXT,
				allowNull: true,
				description: 'Pcase´s api code in case of api'
			}								
		},
		{
			hooks: {
				afterCreate: (pcase) => {
					sequelize.models.pcase.findOne({
						where: {
							title: pcase.title,
							created_at: pcase.created_at,
							user_id: pcase.user_id,
							environment_id: pcase.environment_id
						}
					}).then((pc) => {
						sequelize.models.pcase_user_info.create({
							user_id: pc.user_id,
							pcase_id: pc.id,
							status: 'PENDING',
							favorite: false,
							category_id: pc.category_id
						});					
					});
				},
				afterBulkCreate: (pcases) => {
					pcases.forEach((pcase) => {
						sequelize.models.pcase.findOne({
							where: {
								title: pcase.title,
								created_at: pcase.created_at,
								user_id: pcase.user_id,
								environment_id: pcase.environment_id
							}
						}).then((pcaseF) => {
							sequelize.models.pcase_user_info.create({
								user_id: pcaseF.user_id,
								pcase_id: pcaseF.id,
								status: 'PENDING',
								favorite: false,
								category_id: pcaseF.category_id
							});
						});
					});
				}
			}
		}
	);
}






















