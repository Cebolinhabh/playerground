"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('category', {
		id: {
			allowNull: true,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER,
			description: 'Category`s id'
		},
		name: {
			type: DataTypes.STRING(64),
			allowNull: false,
			description: 'Category`s name'
		},
		slug: {
			type: DataTypes.STRING(64),
			allowNull: true,
			description: 'Category`s slug URL'
		},
		description: {
			type: DataTypes.STRING,
			allowNull: true,
			description: 'Category`s description'
		},
		type: {
			type: DataTypes.ENUM('ADMIN', 'SUPPORT', 'CLIENT'),
			allowNull: true,
			defaultValue: 'ADMIN',
			description: 'Categories`s type'
		}
	}, {
		timestamps: false
	});
}