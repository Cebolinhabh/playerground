"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('user', {
		id: {
			allowNull: true,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER,
			description: 'User`s id'
		},
		name: {
			type: DataTypes.STRING(64),
			allowNull: false,
			description: 'User`s name'
		},
		email: {
			type: DataTypes.STRING(64),
			allowNull: true,
			description: 'User´s email'
		},
		github_id: {
			type: DataTypes.STRING(64),
			allowNull: true,
			description: 'User´s github id'	
		},
		avatar_url: {
			type: DataTypes.STRING(255),
			allowNull: true,
			description: 'User´s avatar'	
		},
		password: {
			type: DataTypes.STRING(64),
			allowNull: true,
			description: 'User´s password'
		},
		role: {
			type: DataTypes.ENUM('ADMIN', 'SUPPORT', 'CLIENT'),
			allowNull: true,
			defaultValue: 'SUPPORT',
			description: 'User`s role'
		}
	});
}
