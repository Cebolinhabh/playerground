"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('label', {
		id: {
			allowNull: true,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER,
			description: 'Label`s id'
		},
		name: {
			type: DataTypes.STRING(32),
			allowNull: false,
			description: 'Label`s name'
		},
        description: {
			type: DataTypes.TEXT,
			allowNull: false,
			description: 'Label`s description'
		},
		url: {
			type: DataTypes.STRING(255),
			allowNull: false,
			description: 'Label`s Git or Zendesk URL'
		},
		color: {
			type: DataTypes.STRING(10),
			allowNull: true,
			description: 'Label`s Color'
		}
	});
}