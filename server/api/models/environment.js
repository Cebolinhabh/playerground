"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('environment', {
		id: {
			allowNull: true,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER,
			description: 'Environment`s id'
		},
		name: {
			type: DataTypes.STRING(64),
			allowNull: false,
			description: 'Environment`s name'
		},
		url: {
			type: DataTypes.STRING(64),
			allowNull: false,
			description: 'Environment´s URL'
		},
		staticURL: {
			type: DataTypes.STRING(255),
			allowNull: false,
			description: 'Environment´s player static URL'
		},
		isDefault: {
			type: DataTypes.BOOLEAN,
			defaultValue: false,
			allowNull: true,
			description: 'Environment´s isDefault'			
		}
	}, {
		timestamps: false
	});
}