"use strict";

module.exports = (sequelize, DataTypes) => {
	return sequelize.define('branch', {
		id: {
			allowNull: true,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER,
			description: 'Branch`s id'
		},
		name: {
			type: DataTypes.STRING(64),
			allowNull: false,
			description: 'Branch`s name'
		},
		gitURL: {
			type: DataTypes.STRING,
			allowNull: false,
			description: 'Branch`s Git URL'
		},
		scriptURL: {
			type: DataTypes.STRING,
			allowNull: true,
			description: 'Branch`s Script URL'
		},
		isDefault: {
			type: DataTypes.BOOLEAN,
			defaultValue: false,
			allowNull: true,
			description: 'Branch`s is or not default'
		},
		status: {
			type: DataTypes.ENUM('PENDING', 'WARNING', 'ERROR', 'SUCCESS'),
			allowNull: false,
			description: 'Branch`s Status'
		}
	});
}