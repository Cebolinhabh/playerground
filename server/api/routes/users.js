"use strict";

import { JS } from '../models';
import { getAllUsers, getUserById, 
	createUser, updateUser, deleteUser, resetAllPCaseUserInfoStatusByUserId } from '../controllers/user';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/users',
			config : {
				handler: getAllUsers,
				description: 'Get all users',
				notes: 'Returns all users',
				tags: ['api']				
			}
		},
		{
			method: 'GET',
			path: '/v1/users/{id}',
			config : {
				handler: getUserById,
				description: 'Get user by id',
				notes: 'Returns a unique User',
				tags: ['api'],
				validate: {
		            params: {
			            id: Joi.number().required().description('the user id')
			        }
		        }				
			}
		},
		{
			method: 'DELETE',
			path: '/v1/users/{id}',
			config: {
				handler: deleteUser,
				description: 'Delete a user',
				notes: 'Delete a user',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the user id')
					}
				}
			}
		},
		{
			method: 'PUT',
			path: '/v1/users/{id}/resetStatus',
			config: {
				handler: resetAllPCaseUserInfoStatusByUserId,
				description: 'Reset all status to PENDING',
				notes: 'Pcase status reset',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the pcase status id')
					}
				}
			}
		}
	];
}();

