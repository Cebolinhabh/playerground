"use strict";

import { JS } from '../models';
import { githubAuthHandler, githubAuthLogoutHandler, simpleLoginAuthHandler } from '../controllers/auth';

module.exports = function() {
	return [
		{	method: ['GET', 'POST'],
			path: '/v1/auth/github',
			config: {
				handler: githubAuthHandler,
				auth: 'github-oauth'
			}
		},
		{
			method: ['GET', 'POST'],
			path: '/v1/auth/logout',
			config: {
				handler: githubAuthLogoutHandler
				//auth: 'github-oauth'
			}
		},
		{
			method: ['POST'],
			path: '/v1/auth',
			config: {
				handler: simpleLoginAuthHandler,
				auth: {
					strategy: 'pg-cookie',
		        	mode: 'optional'
				}
			}
		}
	];
}();

