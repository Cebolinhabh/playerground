"use strict";

import HapiConfig from '../../cfg/HapiConfig';

const sFiles = HapiConfig.staticFiles;

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/assets/{param*}',
			config: {
				auth: false,
				handler: {
					directory: {
						path: [HapiConfig.staticFiles.baseDir],
						listing: true	
					}
				}
			}	
		}	
	]
}();
	