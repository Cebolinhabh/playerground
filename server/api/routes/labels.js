"use strict";

import { JS } from '../models';
import { getAllLabels, getLabelById, 
	createLabel, updateLabel, deleteLabel } from '../controllers/label';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/labels',
			config : {
				handler: getAllLabels,
				description: 'Get all labels',
				notes: 'Returns all labels',
				tags: ['api']				
			}
		},
		{
			method: 'GET',
			path: '/v1/labels/{id}',
			config : {
				handler: getLabelById,
				description: 'Get label by id with their pcases',
				notes: 'Returns a unique label',
				tags: ['api'],
				validate: {
		            params: {
			            id: Joi.number().required().description('the label id')
			        }
		        }				
			}
		},
		{
			method: 'POST',
			path: '/v1/labels',
			config: {
				handler: createLabel,
				description: 'Creates a new label',
				notes: 'Creates a new label and returns it',
				tags: ['api'],
				validate: {
					payload: JS.label.withRequired()
				}
			}
		},
		// {
		// 	method: 'PUT',
		// 	path: '/v1/environments/{id}',
		// 	config: {
		// 		handler: updateEnvironment,
		// 		description: 'Updates environment',
		// 		notes: 'Updates a environment and returns it',
		// 		tags: ['api'],
		// 		validate: {
		// 			payload: JS.environment.withRequired(),
		// 			params: {
		// 				id: Joi.number().required().description('the environment id')
		// 			}
		// 		}
		// 	}
		// },
		{
			method: 'DELETE',
			path: '/v1/labels/{id}',
			config: {
				handler: deleteLabel,
				description: 'Delete a label',
				notes: 'Delete a label',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the label`s id')
					}
				}
			}
		}
	];
}();

