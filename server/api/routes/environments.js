"use strict";

import { JS } from '../models';
import { getAllEnvironments, getEnvironmentById, 
	createEnvironment, updateEnvironment, deleteEnvironment } from '../controllers/environment';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/environments',
			config : {
				handler: getAllEnvironments,
				description: 'Get all environments',
				notes: 'Returns all environments',
				tags: ['api']				
			}
		},
		{
			method: 'GET',
			path: '/v1/environments/{id}',
			config : {
				handler: getEnvironmentById,
				description: 'Get environment by id',
				notes: 'Returns a unique environment',
				tags: ['api'],
				validate: {
		            params: {
			            id: Joi.number().required().description('the environment id')
			        }
		        }				
			}
		},
		{
			method: 'POST',
			path: '/v1/environments',
			config: {
				handler: createEnvironment,
				description: 'Creates a new environment',
				notes: 'Creates a new environment and returns it',
				tags: ['api'],
				validate: {
					payload: JS.environment.withRequired()
				}
			}
		},
		// {
		// 	method: 'PUT',
		// 	path: '/v1/environments/{id}',
		// 	config: {
		// 		handler: updateEnvironment,
		// 		description: 'Updates environment',
		// 		notes: 'Updates a environment and returns it',
		// 		tags: ['api'],
		// 		validate: {
		// 			payload: JS.environment.withRequired(),
		// 			params: {
		// 				id: Joi.number().required().description('the environment id')
		// 			}
		// 		}
		// 	}
		// },
		{
			method: 'DELETE',
			path: '/v1/environments/{id}',
			config: {
				handler: deleteEnvironment,
				description: 'Delete a environment',
				notes: 'Delete a environment',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the environment id')
					}
				}
			}
		}
	];
}();

