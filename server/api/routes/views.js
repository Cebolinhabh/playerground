"use strict";

import HapiConfig from '../../cfg/HapiConfig';

const sFiles = HapiConfig.staticFiles;

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/',
			config: {
	        	handler: (request, reply) => {
		            reply.view(sFiles.template, {
		            	user: request.auth.credentials,
		            	env: process.env.NODE_ENV,
						mainJS: sFiles.staticJS
		            });
		        },
		        auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'try'
		        }
			}
		},
		{
			method: 'GET',
			path: '/login',
			config: {
	        	handler: (request, reply) => {
		            reply.view(sFiles.template, {
		            	user: request.auth.credentials,
		            	env: process.env.NODE_ENV,
						mainJS: sFiles.staticJS
		            });
				},
				auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'try'
		        }
			}
		},
		{
			method: 'GET',
			path: '/new/{action?}',
			config: {
	        	handler: (request, reply) => {
		            reply.view(sFiles.template, {
		            	user: request.auth.credentials,
		            	env: process.env.NODE_ENV,
						mainJS: sFiles.staticJS
		            });
		        },
		        auth: {
		        	strategy: 'pg-cookie'
		        }
			}
		},
		{
			method: 'GET',
			path: '/{category}',
			config: {
	        	handler: (request, reply) => {
		            reply.view(sFiles.template, {
		            	user: request.auth.credentials,
		            	env: process.env.NODE_ENV,
						mainJS: sFiles.staticJS
		            });
		        },
		        auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'optional'
		        }
			}
		},
		{
			method: 'GET',
			path: '/labels/{label_id}',
			config: {
	        	handler: (request, reply) => {
		            reply.view(sFiles.template, {
		            	user: request.auth.credentials,
		            	env: process.env.NODE_ENV,
						mainJS: sFiles.staticJS
		            });
		        },
		        auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'optional'
		        }
			}
		},
		{
			method: 'GET',
			path: '/{category}/pcases/{id?}',
			config: {
	        	handler: (request, reply) => {
		            reply.view(sFiles.template, {
		            	user: request.auth.credentials,
		            	env: process.env.NODE_ENV,
						mainJS: sFiles.staticJS
		            });
		        },
		        auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'optional'
		        }
			}
		},
		{
			method: 'GET',
			path: '/{category}/pcases/{id}/edit',
			config: {
	        	handler: (request, reply) => {
		            reply.view(sFiles.template, {
		            	user: request.auth.credentials,
		            	env: process.env.NODE_ENV,
						mainJS: sFiles.staticJS
		            });
		        },
		        auth: {
		        	strategy: 'pg-cookie'
		        }
			}
		}
	]
}();
