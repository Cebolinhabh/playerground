"use strict";

import { JS } from '../models';
import { getMediasByCategory } from '../controllers/liquid';
import Joi from 'joi';

const baseURL = '/v1/liquid';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: `${baseURL}/medias`,
			config : {
				handler: getMediasByCategory,
				description: 'Get all medias by category',
				notes: 'Returns all medias by category',
				tags: ['api'],
				auth: {
					strategy: 'pg-cookie',
					mode: 'optional'
				},
				validate: {
					query: {
						cat_id: Joi.number().required().description('the pcase name query')
					}
				},
				cors: true
			}
		}
	];
}();

