"use strict";

import { JS } from '../models';
import { getAllBranches, getBranchById, createBranch, 
	updateBranch, deleteBranch } from '../controllers/branch';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/branches',
			config : {
				handler: getAllBranches,
				description: 'Get all branches',
				notes: 'Returns all branches',
				tags: ['api', 'branches']			
			}
		},
		{
			method: 'GET',
			path: '/v1/branches/{id}',
			config : {
				handler: getBranchById,
				description: 'Get branch by id',
				notes: 'Returns a unique branch',
				tags: ['api', 'branches'],
				validate: {
		            params: {
			            id: Joi.number().required().description('the branch id')
			        }
		        }			
			}
		},
		{
			method: 'POST',
			path: '/v1/branches',
			config: {
				handler: createBranch,
				description: 'Creates a new branch',
				notes: 'Creates a new branch and returns it',
				tags: ['api', 'branches'],
				validate: {
					payload: JS.branch.withRequired()
				}
			}
		},
		// {
		// 	method: 'PUT',
		// 	path: '/v1/branches/{id}',
		// 	config: {
		// 		handler: updateBranch,
		// 		description: 'Updates branch',
		// 		notes: 'Updates a branch and returns it',
		// 		tags: ['api'],
		// 		validate: {
		// 			payload: JS.branch.withRequired(),
		// 			params: {
		// 				id: Joi.number().required().description('the branch id')
		// 			}
		// 		}
		// 	}
		// },
		{
			method: 'DELETE',
			path: '/v1/branches/{id}',
			config: {
				handler: deleteBranch,
				description: 'Delete a branch',
				notes: 'Delete a branch',
				tags: ['api', 'branches'],
				validate: {
					params: {
						id: Joi.number().required().description('the branch id')
					}
				}
			}
		}
	];
}();

