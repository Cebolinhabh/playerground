"use strict";

import { JS } from '../models';
import { getAllPCaseUserInfo, getAllPCaseUserInfoByUser, createPCaseUserInfo, 
	updatePCaseUserInfo, resetAllPCaseUserInfoStatusByUserId } from '../controllers/pcase_user_info';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/pcase_user_info',
			config : {
				handler: getAllPCaseUserInfo,
				description: 'Get all pcase status',
				notes: 'Returns all pcase status',
				tags: ['api']				
			}
		},
		{
			method: 'GET',
			path: '/v1/pcase_user_info/{user_id}',
			config : {
				handler: getAllPCaseUserInfoByUser,
				description: 'Get all pcase status',
				notes: 'Returns all pcase status',
				tags: ['api']				
			}
		},
		{
			method: 'POST',
			path: '/v1/pcase_user_info',
			config: {
				handler: createPCaseUserInfo,
				description: 'Creates a pcase status',
				notes: 'Creates a pcase status',
				tags: ['api'],
				validate: {
					payload: JS.browser.withRequired()
				}
			}
		},
		{
			method: 'PUT',
			path: '/v1/pcase_user_info/{id}',
			config: {
				handler: updatePCaseUserInfo,
				description: 'Update a pcase status',
				notes: 'Pcase status update',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the pcase status id')
					}
				}
			}
		}
	];
}();

