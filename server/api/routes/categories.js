"use strict";

import { JS } from '../models';
import { getAllCategories, getCategoryBySlug, 
	createCategory, updateCategory, updatePCaseUserInfoByCategory, deleteCategory } from '../controllers/category';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/categories',
			config : {
				handler: getAllCategories,
				description: 'Get all categories',
				notes: 'Returns all categories',
				tags: ['api'],
		        auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'optional'
		        }					
			}
		},
		{
			method: 'GET',
			path: '/v1/categories/name/{cat_slug}',
			config : {
				handler: getCategoryBySlug,
				description: 'Get category by slug name',
				notes: 'Returns a unique category',
				tags: ['api'],
				validate: {
		            params: {
			            cat_slug: Joi.string().required().description('the category name')
			        }
		        },
		        auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'optional'
		        }				
			}
		},
		{
			method: 'POST',
			path: '/v1/categories',
			config: {
				handler: createCategory,
				description: 'Creates a new category',
				notes: 'Creates a new category and returns it',
				tags: ['api'],
				validate: {
					payload: JS.category.withRequired()
				},
				auth: 'pg-cookie'
			}
		},
		{
			method: 'PUT',
			path: '/v1/categories/{id}/pcase_user_info',
			config: {
				handler: updatePCaseUserInfoByCategory,
				description: 'Updates all states of category',
				notes: 'Updates all states of category',
				tags: ['api'],
				auth: 'pg-cookie'
			}
		},
		{
			method: 'DELETE',
			path: '/v1/categories/{id}',
			config: {
				handler: deleteCategory,
				description: 'Delete a category',
				notes: 'Delete a category',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the category id')
					}
				},
				auth: 'pg-cookie'
			}
		}
	];
}();

