"use strict";

module.exports = function() {
	return [
		{	method: ['GET'],
			path: '/v1/health',
			config: {
				handler: (request, reply) => {
                    reply({success: true}).code(200);
                },
				auth: false
			}
		}
	];
}();
