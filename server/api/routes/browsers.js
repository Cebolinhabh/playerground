"use strict";

import { JS } from '../models';
import { getAllBrowsers, getBrowserById, createBrowser, 
	updateBrowser, deleteBrowser } from '../controllers/browser';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/browsers',
			config : {
				handler: getAllBrowsers,
				description: 'Get all browsers',
				notes: 'Returns all browsers',
				tags: ['api']				
			}
		},
		// {
		// 	method: 'GET',
		// 	path: '/v1/browsers/{id}',
		// 	config : {
		// 		handler: getBrowserById,
		// 		description: 'Get browser by id',
		// 		notes: 'Returns a unique browser',
		// 		tags: ['api'],
		// 		validate: {
		//             params: {
		// 	            id: Joi.number().required().description('the browser id')
		// 	        }
		//         }				
		// 	}
		// },
		{
			method: 'POST',
			path: '/v1/browsers',
			config: {
				handler: createBrowser,
				description: 'Creates a new user',
				notes: 'Creates a new user and returns it',
				tags: ['api'],
				validate: {
					payload: JS.browser.withRequired()
				}
			}
		},
		// {
		// 	method: 'PUT',
		// 	path: '/v1/browsers/{id}',
		// 	config: {
		// 		handler: updateBrowser,
		// 		description: 'Updates a user and returns it',
		// 		notes: 'Updates a user and returns it',
		// 		tags: ['api'],
		// 		validate: {
		// 			payload: JS.browser.withRequired(),
		// 			params: {
		// 				id: Joi.number().required().description('the browser id')
		// 			}
		// 		}
		// 	}
		// },
		{
			method: 'DELETE',
			path: '/v1/browsers/{id}',
			config: {
				handler: deleteBrowser,
				description: 'Delete a browser',
				notes: 'Delete a browser',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the user id')
					}
				}
			}
		}
	];
}();

