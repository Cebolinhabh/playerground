"use strict";

import { JS } from '../models';
import { getAllPCases, getPCaseById, getPCaseByIdAndEnvironmentAndCategory, getFavoritesPCase, getPCaseByName, createPCase, 
	updatePCase, updatePCaseUserInfo, setFavoritePCase, deletePCase, addLabelToPcase, removeLabelFromPcase } from '../controllers/pcase';
import Joi from 'joi';

module.exports = function() {
	return [
		{
			method: 'GET',
			path: '/v1/pcases',
			config : {
				handler: getAllPCases,
				description: 'Get all pcases',
				notes: 'Returns all pcases',
				tags: ['api']				
			}
		},
		{
			method: 'GET',
			path: '/v1/pcases/{id}',
			config : {
				handler: getPCaseByIdAndEnvironmentAndCategory,
				description: 'Get pcase by id',
				notes: 'Returns a unique pcase',
				tags: ['api'],
				validate: {
		            params: {
			            id: Joi.number().required().description('the pcase id')
			        },
					query: {
						cat_id: Joi.string().description('the pcase name query'),
						env_id: Joi.string().description('the pcase environment')
					}
		        },
		        auth: {
		        	strategy: 'pg-cookie',
		        	mode: 'optional'
		        }				
			}
		},
		{
			method: 'GET',
			path: '/v1/pcases/favorite',
			config: {
				handler: getFavoritesPCase,
				description: 'Get all pcases favorites',
				notes: 'Get all pcases favorites by environment',
				tags: ['api'],
				validate: {
					query: {
						env: Joi.number().required().description('the pcase environment')
					}
				},
				auth: 'pg-cookie'
			}
		},
		{
			method: 'GET',
			path: '/v1/pcases/search',
			config: {
				handler: getPCaseByName,
				description: 'Search by name',
				notes: 'Get all pcases within a query string',
				tags: ['api'],
				validate: {
					query: {
						q: Joi.string().required().description('the pcase name query'),
						env: Joi.string().required().description('the pcase environment')
					}
				}
			}
		},
		{
			method: 'POST',
			path: '/v1/pcases',
			config: {
				handler: createPCase,
				description: 'Creates a new pcase',
				notes: 'Creates a new pcase and returns it',
				tags: ['api'],
				validate: {
					payload: JS.pcase.withRequired()
				},
				auth: 'pg-cookie'
			}
		},
		{
			method: 'PUT',
			path: '/v1/pcases/{id}',
			config: {
				handler: updatePCase,
				description: 'Updates pcase',
				notes: 'Updates a pcase and returns it',
				tags: ['api'],
				validate: {
					payload: JS.pcase.withRequired(),
					params: {
						id: Joi.number().required().description('the pcase id')
					}
				},
				auth: 'pg-cookie'
			}
		},
		{
			method: 'PUT',
			path: '/v1/pcases/{id}/user_info',
			config: {
				handler: updatePCaseUserInfo,
				description: 'Updates the user info',
				notes: 'Updates the user info and returns it',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the pcase id')
					}
				},
				auth: 'pg-cookie'
			}
		},
		{
			method: 'DELETE',
			path: '/v1/pcases/{id}',
			config: {
				handler: deletePCase,
				description: 'Delete a pcase',
				notes: 'Delete a pcase',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the pcase id')
					}
				},
				auth: 'pg-cookie'
			}
		},
		{
			method: 'PUT',
			path: '/v1/pcases/{id}/label/{label_id}',
			config: {
				handler: addLabelToPcase,
				description: 'Add label to a pcase',
				notes: 'Add label to a pcase',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the pcase id'),
						label_id: Joi.number().required().description('the label id')
					}
				},
				auth: 'pg-cookie'
			}
		},
		{
			method: 'DELETE',
			path: '/v1/pcases/{id}/label/{label_id}',
			config: {
				handler: removeLabelFromPcase,
				description: 'Delete a label from pcase',
				notes: 'Delete a label from pcase',
				tags: ['api'],
				validate: {
					params: {
						id: Joi.number().required().description('the pcase id'),
						label_id: Joi.number().required().description('the label id')
					}
				},
				auth: 'pg-cookie'
			}
		}
	];
}();

