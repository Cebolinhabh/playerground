export default function getConfig() {
	return {
		"prod": {
			"baseURL": "http://api.sambavideos.sambatech.com/v1"
		},
		"dev": {
			"baseURL": "http://web1.qa.sambatech.com:10000/v1"
		},
		"staging": {
			"baseURL": "http://staging.sambavideos.sambatech.com/v1"
		}
	}
}