import Liquid from './liquid';

class LiveChannels extends Liquid {
	/**
	 * Constructor for Liquid Class
	 *
	 * @private
	 * @param {String} token Access_token
	 * @param {String} type Type of environment. Defaults: prod
	 * @returns {Liquid} Returns a liquid object
	 */
	constructor(token, type) {
		super(token, type);
	}

	/**
	 * Get LIVES by Project
	 *
	 * @private
	 * @param {integer} pid Category ID
	 * @returns {Object} Returns a list of medias from that category
	 */
	getLiveChannels(pid) {
		return this.makeRequest('/livechannel', 'GET', {
			params: {
				pid
			}
		}).catch(error => {
			throw new Error(error);
		});
	}
}

export default LiveChannels;