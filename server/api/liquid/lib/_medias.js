import Liquid from './liquid';

class Medias extends Liquid {
	/**
	 * Constructor for Liquid Class
	 *
	 * @private
	 * @param {String} token Access_token
	 * @param {String} type Type of environment. Defaults: prod
	 * @returns {Liquid} Returns a liquid object
	 */
	constructor(token, type) {
		super(token, type);
	}

	getMedias(params) {
		return this.makeRequest('/medias', 'GET', {
			params
		}).catch(error => {
			throw new Error(error);
		});
	}

	getActiveMedias() {

	}

	/**
	 * Get medias by Project
	 *
	 * @private
	 * @param {integer} pid Category ID
	 * @returns {Object} Returns a list of medias from that category
	 */
	getMediasByProject(pid) {
		return this.getMedias({
			pid
		});
	}
}

export default Medias;