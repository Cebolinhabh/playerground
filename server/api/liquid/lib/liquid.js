import axios from 'axios';
import cache from 'memory-cache';
import getConfig from '../config';

class Liquid {
	/**
	 * Constructor for Liquid Class
	 *
	 * @private
	 * @param {String} token Access_token
	 * @param {Object} options Type of environment. Defaults: prod
	 * @returns {Liquid} Returns a liquid object
	 */
	constructor(token, options) {
		this.opt = {... options}; //TODO pensar em defaults
		this.access_token = token;
		this.baseURL = getConfig()[this.opt.type] ?
			getConfig()[this.opt.type].baseURL : getConfig().prod.baseURL;
	}

	/**
	 * Constructor for Liquid Class
	 *
	 * @private
	 * @param {String} url Request url
	 * @param {String} type Type of the request
	 * @param {Object} config Configuration of the object
	 * @returns {Liquid} Returns a liquid object
	*/
	makeRequest(url, type, config) {
		const rURL = `${this.baseURL}${url}`;
		return (this.opt.cache && cache.get(`${rURL}|${this.access_token}`))
			? this.cacheRequest(`${rURL}|${this.access_token}`) : this.httpRequest(rURL, type, config);
	}

	/**
	 * Constructor for Liquid Class
	 *
	 * @private
	 * @param {String} url Request url
	 * @param {String} type Type of the request
	 * @param {Object} config Configuration of the object
	 * @returns {Liquid} Returns a liquid object
	*/
	httpRequest(rURL, type, config) {
		config.params = {... config.params, access_token: this.access_token};

		return axios[type.toLowerCase()](rURL, config).then(response => {
			if(response.status === 200 && response.data.length > 0 && this.opt.cache)
				cache.put(`${rURL}|${this.access_token}`, response, this.opt.cache);
			return response;
		});
	}

	/**
	 * Gets from cache
	 *
	 * @param {*} key
	 */
	cacheRequest(key) {
		return new Promise((resolve, reject) => {
			resolve(cache.get(key));
		});
	}
}

export default Liquid;