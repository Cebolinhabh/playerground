'use strict';

import React from 'react';
class Default extends React.Component {
    constructor(props) {
      super(props);
      const json = JSON.stringify(props);
      
      this.propStore = <script type="application/javascript"
        dangerouslySetInnerHTML={{__html: "var envVariables = " + json}}>
      </script>;
    }

    render() {
      return (
        <html>
        <head>
          <meta charSet="utf-8"/>
          <title>Playerground Sambatech</title>
          <link rel="icon" type="image/png" href="/assets/favicon.ico"/>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
          <meta name="description" content=""/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
          <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500' rel='stylesheet' type='text/css'/>
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
        </head>
        <body>
          <div id="app"></div>

          <script>__REACT_DEVTOOLS_GLOBAL_HOOK__ = parent.__REACT_DEVTOOLS_GLOBAL_HOOK__</script>
          {this.propStore}
          <script type="text/javascript" src={this.props.mainJS}></script>
        </body>
        </html>   
      )   
    }
}

export default Default;

