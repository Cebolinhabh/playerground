'use strict';

let path = require('path');
let webpack = require('webpack');
let baseConfig = require('./base');
let defaultSettings = require('./defaults');

// Add needed plugins here
let BowerWebpackPlugin = require('bower-webpack-plugin');
let BrowserSyncPlugin = require('browser-sync-webpack-plugin');

let config = Object.assign({}, baseConfig, {
  entry: [
    //'webpack/hot/dev-server',
    //'webpack-hot-middleware/client?http://localhost:' + defaultSettings.port,
    //'webpack-hot-middleware/client?http://' + require("ip").address() + ':' + defaultSettings.port,
    path.join(__dirname, '../../src/index')
  ],
  cache: true,
  devtool: 'eval-source-map',
  plugins: [
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 9090,
      proxy: 'http://localhost:3000'
    }),
    //new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new BowerWebpackPlugin({
      searchResolveModulesDirectories: false
    }),
    new webpack.ProvidePlugin({Promise: 'bluebird'})
  ],
  module: defaultSettings.getDefaultModules()
});

// Add needed loaders to the defaults here
config.module.loaders.push({
  test: /\.(js|jsx)$/,
  loader: 'babel-loader',
  include: [].concat(
    config.additionalPaths,
    [ path.join(__dirname, '../../src') ]
  )
});

module.exports = config;
