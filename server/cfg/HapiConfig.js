"use strict";

module.exports = function() {

	var env = process.env.NODE_ENV || 'development';
	var dbContants = databaseConfig();
	var appConstants = applicationConfig();
	var githubConstants = githubConfig();

	var obj = {
		application : {
			url : appConstants[env]['url'],
			host : appConstants[env]['host'],
			port : appConstants[env]['port'],
		},
		database : {
			host     : dbContants[env]['host'],
			user     : dbContants[env]['user'],
			password : dbContants[env]['password'],
			database : dbContants[env]['database']
		},
		github: {
			github_client_id: githubConstants[env]['github_client_id'],
			github_client_secret: githubConstants[env]['github_client_secret'],
			roles: {
				admin: [141922, 16780687, 10501332, 484062, 20074534, 10449832, 1610108, 18405622],
				support: [11819108, 2347420, 12154841],
				client: []
			}
		},
		staticFiles: staticFiles()[env],
		server : {
			defaultHost : 'http://localhost:8001'
		},
		drm: drmConfig(),
		basicauth: authUser()
	};

	return obj;

	function githubConfig() {
		return {
			'production' : {
				'github_client_id' : '6eebf9b6abd746d950fd',
				'github_client_secret' : 'a4e246962d962ec91d5566ec5bff94ce25dba19d',
			},
			'development' : {
				'github_client_id' : '2c57f5e09bd36a2032b3',
				'github_client_secret' : '40d377f804fc6ef680e80741febe12b7992e76c9'
			},
			'test' : {
				'github_client_id' : '2c57f5e09bd36a2032b3',
				'github_client_secret' : '40d377f804fc6ef680e80741febe12b7992e76c9'
			}
		};
	}

	function databaseConfig(){
		return {
			'production' : {
				'host' : "playerground.cszw0ctbadn5.us-west-2.rds.amazonaws.com",
				'user' : "player",
				'password' : "N77r5rKb7ENwRsVg",
				'database' : "playerground"
			},
			'development' : {
				'host' : process.env.DB_HOST || 'localhost',
				'user' : 'root',
				'password' : 'root',
				'database' : 'playerground'
			},
			'test' : {
				'host' : 'localhost',
				'user' : 'root',
				'password' : 'root',
				'database' : 'playerground-test'
			}
		};
	}

	function applicationConfig(){
		return {
			'production' : {
				'url' : 'localhost',
				'host' : '0.0.0.0',
				'port' : process.env.PORT || 3000
			},
			'development' : {
				'host' : '0.0.0.0',
				'port' : process.env.PORT || 3000
			},
			'test' : {
				'url' : 'localhost',
				'host' : '0.0.0.0',
				'port' : process.env.PORT || 3000
			}
		};
	}

	function drmConfig() {
		return {
				"crmId": "sambatech",
				"accountId": "sambatech",
				"user": "app@sambatech.com",
				"password": "c5kU6DCTmomi9fU",
				"baseUrl": "http://sambatech.live.ott.irdeto.com/services",
				"baseUrlStaging": "http://sambatech.stage.ott.irdeto.com/services"
		}
	}

	function authUser() {
		return {
			"sambaPG": {
				"username": 'Sambatech Playeground',
				"password": 'AuEHR3dM',
				"name": 'Sambatech Playerground user',
				"id": 1
			}
		};
	}

	function staticFiles() {
		return {
			"production": {
				"staticJS" : "/assets/app.js",
				"template" : "Default",
				"baseDir"  : "public",
			},
			"development": {
				"staticJS": "/app.js",
				"template": "Default",
				"baseDir"  : "src"
			}
		}
	}
}();