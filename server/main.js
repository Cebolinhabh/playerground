/*eslint no-console:0 */
'use strict';

/** Import dependencies **/
import { Server } from 'hapi';
import WebpackPlugin from 'hapi-webpack-plugin';
import Inert from 'inert';
import Vision from 'vision';
import path from 'path';
import os from 'os';
import HapiSwagger from 'hapi-swagger';
import HapiReactViews from 'hapi-react-views';
import HapiAuthBasic from 'hapi-auth-basic';

import HapiConfig from './cfg/HapiConfig';
import apiRoutes from './api/routes';
import { sequelize } from './api/models';
import { basicAuth } from './api/controllers/auth';

//Github
//import HapiGithubAuth from 'hapi-auth-github';
import Bell from 'bell';
import AuthCookie from 'hapi-auth-cookie';
import { getUserByGithubId } from './api/controllers/auth';

//Environment
const environment = process.env.NODE_ENV;

/**Create server**/
const server = new Server({
	connections: {
		routes: {
			cors: true
		}		
	}
});

/** Swagger **/
const swaggerOptions = {
	basePath: '/v1',
    info: {
            'title': 'Test API Documentation',
            'version': "1.0"
    },
	    tags: [{
	        'name': 'branches',
	        'description': 'Api for github branches'
	    },{
	        'name': 'store',
	        'description': 'Storing a sum',
	        'externalDocs': {
	            'description': 'Find out more about storage',			
	            'url': 'http://example.org'
	        }
	    }, {
	        'name': 'sum',
	        'description': 'API of sums',
	        'externalDocs': {
	            'description': 'Find out more about sums',
	            'url': 'http://example.org'
        }
    }]
};

//Server listen properties
server.connection({host: HapiConfig.application['host'], port: HapiConfig.application['port']});

/** Register plugin and start server **/
let hapiPlugins = [{
		register: HapiSwagger,
		options: swaggerOptions
	},{
		register: Inert
	},{
		register: Vision
	},
	HapiAuthBasic,
	AuthCookie,
	Bell
];

//If development load webpack plugin
if(environment === 'development') {
	let webpackOptions = path.join(__dirname, '/webpack.config.js');
	//Webpack plugin
	hapiPlugins.push({
		register: WebpackPlugin,
		options: webpackOptions
	});
}


/** Register plugin and start server **/
server.register(hapiPlugins,
	error => {
		if(error) {
			return console.error(error);
		}

		//Basic auth
		server.auth.strategy('simple', 'basic', {validateFunc: basicAuth});	

		//Github && cookie
		const authCookieOptions = {
			password: 'password-should-be-32-characters',
			cookie: 'pg-auth',
			isSecure: false,
			validateFunc: function (request, session, callback) {
				getUserByGithubId(session.sid).then((user, err) => {
					if(user == null) {
						request.cookieAuth.clear();
						return callback(null, false);
					}
					
					return callback(null, true, user.dataValues);
				});
	        }
		}

		server.auth.strategy('pg-cookie', 'cookie', false, authCookieOptions);

		const bellAuthOptions = {
		    provider: 'github',
		    password: 'password-should-be-32-characters', 
		    clientId: HapiConfig.github['github_client_id'],
		    clientSecret: HapiConfig.github['github_client_secret'],
		    isSecure: false
		};

		server.auth.strategy('github-oauth', 'bell', false, bellAuthOptions);

		//server.auth.default('simple');

		 // Add the React-rendering view engine
	    server.views({
	        engines: {
				js: HapiReactViews,
	            jsx: HapiReactViews
	        },
			defaultExtension: environment === 'development' ? 'jsx' : 'js',
	        relativeTo: __dirname,
	        path: './public/',
	        compileOptions: {
	        	renderToString: true
	        }
	    });

		for(let i in apiRoutes) 
			server.route(apiRoutes[i]);	

		//Sequelize sync
		sequelize.sync({force: false}).then(() => {
			server.start(() => console.log('Server running at:', server.info.uri));
		}, (error) => {
			console.info("Could not connect to sequelize at " + (new Date).toString(), error);
		});
	}
);

export default server;

//Process events
process.on('SIGINT', function() { console.log('Caught Ctrl+C...'); process.exit(); });
process.on('SIGTERM', function() { console.log('Caught kill...'); process.exit(); });
process.on('uncaughtException', err => {
	console.warn(err);
	process.exit(1);
});
