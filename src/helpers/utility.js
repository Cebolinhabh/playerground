export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const checkValidity = ( value, rules ) => {
    let isValid = true;
    if ( !rules ) {
        return true;
    }

    if ( rules.required ) {
        isValid = value.trim() !== '' && isValid;
    }

    if ( rules.minLength ) {
        isValid = value.length >= rules.minLength && isValid
    }

    if ( rules.maxLength ) {
        isValid = value.length <= rules.maxLength && isValid
    }

    if ( rules.isEmail ) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test( value ) && isValid
    }

    if ( rules.isNumeric ) {
        const pattern = /^\d+$/;
        isValid = pattern.test( value ) && isValid
	}

    return isValid;
};

//  OLD UTILITIES

import moment from 'moment';
moment.locale('pt');

const calendarFormat = {
	sameDay: '[Hoje]',
	nextDay: '[Amanhã]',
	nextWeek: 'dddd',
	lastDay: '[Ontem]',
	lastWeek: '[Na última] dddd',
	sameElse: 'DD [de] MMMM, YYYY'
};

/** Functions used over the code**/

export function addURLParameter(uri, key, val) {
	return uri
		.replace(new RegExp('([?&]'+key+'(?=[=&#]|$)[^#&]*|(?=#|$))'), '&'+key+'='+encodeURIComponent(val))
		.replace(/^([^?&]+)&/, '$1?');
}

export function removeURLParameter(url, parameter) {
	//prefer to use l.search if you have a location/link object
	let urlparts= url.split('?');
	if (urlparts.length>=2) {

		let prefix= encodeURIComponent(parameter)+'=';
		let pars= urlparts[1].split(/[&;]/g);

		//reverse iteration as may be destructive
		for (let i= pars.length; i-- > 0;) {
			//idiom for string.startsWith
			if (pars[i].lastIndexOf(prefix, 0) !== -1) {
				pars.splice(i, 1);
			}
		}

		url= urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
		return url;
	} else {
		return url;
	}
}

export function getClassByStatus(status) {
	const statusMap = {
		'PENDING': '',
		'WARNING': 'warning',
		'ERROR': 'error',
		'SUCCESS': 'success'
	};

	return statusMap[status];
}

export function treatDate(dateStr) {
	return moment(dateStr).calendar(null, calendarFormat);
}

export function createAndAppendScripts(url, attrs, cb, remove) {
	const body = document.querySelector('body');
	const scripts = [].slice.call(body.querySelectorAll('script'));

	if (scripts && scripts.some((script) => script.src === url)) {
		cb && cb();
		return;
	}

	const script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = url;

	if (attrs)
		for (let k in attrs)
			script.setAttribute(k, attrs[k]);

	script.onload = script.onreadystatechange = function() {
		if (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete') {
			cb && cb();

			//remove script
			if(remove)
				script.parentNode.removeChild(script);
		}
	}

	body.appendChild(script);
}

export function objToQuery(obj, alone = true) {
	let params = '';
	let sep = alone ? '?' : '';

	for (let k in obj) {
		params += `${sep}${k}=${obj[k]}`;
		sep = '&';
	}

	return params;
}

export function queryToObj(url) {
	url = decodeURI(url).split(/[\&\?]/);

	let result = {};
	let t = url.length;
	let reKvPair = /(?=\=)/;
	let reBool = /^(true|false)$/i;
	let brackets = 0;
	let numOpen;
	let numClose;
	let kv;
	let k;
	let v;
	let ki = null;

	for (let i = 1; i < t; ++i) {
		kv = url[i].split(reKvPair);

		if (kv.length < 2)
			kv[1] = '';

		// merging all exceeding elements (> 2) in second position
		kv.splice(1, kv.length - 1, kv.slice(1).join(''));

		k = kv[0];
		v = kv[1];
		numOpen = v.split(/\[/).length - 1;
		numClose = v.split(/\]/).length - 1;

		if (numOpen > 0 && brackets === 0)
			ki = k;

		brackets += numOpen - numClose;

		if (numClose > 0 && brackets === 0)
			v = v.substr(0, v.length - 1);

		if (ki === null)
			result[k] = reBool.test(v = v.substr(1)) ? v.toLowerCase() === 'true' : decodeURIComponent((v.match(/[^\#]*/) || [''])[0]);
		else if (result[ki]) {
			result[ki] += (result[ki].indexOf('=') === -1 ? '?' : '&') + k + v;
		}
		else { result[ki] = decodeURIComponent(v.substr(2)); } // rm /^\=\[/

		if (brackets === 0)
			ki = null;
	}

	return result;
}

export function urlToMediaObj(url) {
	const m = url.match(/(\w{32})\/?(\w{0,32})\/?(\w{0,32})?\??[$]?/);

	let obj = {
		ph: m[1],
		m: (typeof m[3] == 'undefined') ? m[2] : null,
		live: (typeof m[3] !== 'undefined' && m[2] === 'live') ? m[3] : null,
		playlistId: (typeof m[3] !== 'undefined' && m[2] === 'playlist') ? m[3] : null,
		playerParams: queryToObj(url)
	};

	return obj;
}

export function searchToObject(search) {
	let pairs = [search.substring(1)] || window.location.search.substring(1).split("&"),
	  obj = {},
	  pair,
	  i;

    for ( i in pairs ) {
	  if ( pairs[i] === "" ) continue;

	  pair = pairs[i].split("=");
	  obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
	}

	return obj;
  };
