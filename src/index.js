import 'core-js/fn/object/assign';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

//Pages and components
import App from './App';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store, { history } from './store/store';

//Stylesheet
require('./styles/main.css');
require('./styles/_sass/style.scss');
require('./styles/hybrid.css');

//	Main app
const app = (
	<Provider store={store}>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</Provider>
);

// Render the main component into the dom
ReactDOM.render(app, document.getElementById('app'));




