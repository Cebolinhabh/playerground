import React, { Component, Fragment } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';
import { searchToObject } from '../../helpers/utility';

// import Home from './Home';
// import PCases from './PCases';
// import NewAction from './NewAction';

import HeaderComponent from '../../components/header/Header';
import SideNavigation from '../../components/navigation/SideNavigation';
import SearchResults from '../../components/modal/SearchResults';
import Pcases from '../../containers/Pcases/Pcases';
import Categories from '../../containers/Categories/Categories';
import CreateUpdateView from '../../containers/CreateUpdateView/CreateUpdateView';
//import SideBar from '../../components/sidebarMenu/Sidebar';

import Home from '../../containers/Home/Home';
//import CreateUpdateView from '../../containers/CreateUpdateView/CreateUpdateView';
//import PCase from '../../containers/Pcase/Pcase';

class MainLayout extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modalActive: false,
			navigationMenu: 'wrap-menu',
			searchType: ''
		};
	}

	componentDidMount() {
		this.fetchData();
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.activeEnvironment !== nextProps.activeEnvironment) {
			this.props.fetchCategories(nextProps.activeEnvironment.id);
		}
	}

	fetchData() {
		const envObj = searchToObject(this.props.location.search);

		this.props.fetchEnvironments(envObj);
	}

	toggleNavigationMenu = (event) => {
		const { navigationMenu } = this.state;
		let navClassName;

		if (navigationMenu === 'wrap-menu') {
			navClassName = 'wrap-menu active';
		} else {
			navClassName = 'wrap-menu';
		}

		this.setState({
			navigationMenu: navClassName
		});
	}

	changeModalState(flag) {
		this.setState({
			modalActive: flag
		})
	}

	render() {
		return (
			<Fragment>
				<HeaderComponent toggleNavigationMenu={this.toggleNavigationMenu} />
				<SideNavigation toggleNavigationMenu={this.toggleNavigationMenu} slideMenu={this.state.navigationMenu} />
				<SearchResults />
				<Switch>
					<Route path="/new/:action" component={CreateUpdateView}/>
					<Route path="/:category_slug/pcases/:pcase_id" component={Pcases}/>
					<Route path="/:category_slug" component={Categories}/>
					<Route path="/" component={Home}/>
				</Switch>
			</Fragment>
		)
	}
}

//	Redux
const mapStateToProps = (state, ownProps) => {
	return {
		activeEnvironment: state.environments.active.environment
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchEnvironments: (envId) => dispatch(actions.fetchEnvironments(envId)),
		fetchCategories: (env) => dispatch(actions.fetchCategories({env}))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);