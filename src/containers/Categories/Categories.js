import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import SideBar from '../../components/sidebarMenu/Sidebar';
import Pcase from '../../components/pcase/Pcase';
import * as actions from '../../store/actions';

class Categories extends Component {

	componentWillUnmount() {
		this.props.resetCategory();
	}

	render() {
		return (
			<div className="container-fluid page-content">
				<SideBar className="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 page-playlist single-category"/>
			</div>
		)
	}
};

const mapStateToProps = (state, ownProps) => {
	return {
		activeEnvironment: state.environments.active.environment,
		activeCategory: state.categories.active
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchCategory: (name, env) => dispatch(actions.fetchCategory(name, env)),
		resetCategory: () => dispatch(actions.resetCategory())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);

