import React from 'react';

import store from '../stores/store';
import SettingsForm from '../components/forms/SettingsComponent';

class Settings extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<SettingsForm user={this.props.auth.user}/>
		)
	}
}

export default Settings;

