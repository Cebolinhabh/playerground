import React, { Component, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import SideBar from '../../components/sidebarMenu/Sidebar';
import Pcase from '../../components/pcase/Pcase';
import EditPcase from '../../components/forms/Pcase';
import * as actions from '../../store/actions';

class Pcases extends Component {
	componentDidMount() {
		const { match, activeEnvironment, fetchCategory } = this.props;
		
		if (activeEnvironment) fetchCategory(match.params.category_slug, { env: activeEnvironment.id });
	}

	shouldComponentUpdate(nextProps) {
		const shouldUpdate = (
			!this.props.activeEnvironment ||
			this.props.match.params.category_slug !== nextProps.match.params.category_slug &&
			this.props.location !== nextProps.location ||
			nextProps.activeCategory.category != null
		);
		return shouldUpdate;
	}

	componentWillUpdate(nextProps) {
		const { match, activeEnvironment, fetchCategory, activeCategory } = nextProps;
		
		if (activeCategory.category && this.props.match.params.category_slug != match.params.category_slug) fetchCategory(match.params.category_slug, { env: activeEnvironment.id });
	}

	render() {
		return (
			<Switch>
				<Route path="/:category_slug/pcases/:pcase_id/edit" exact>
					<div className="container-fluid page-content">
						<EditPcase />
						<SideBar className="col-md-4 col-sm-12 page-playlist"/>
					</div>
				</Route>
				<Route path="/:category_slug/pcases/:pcase_id" exact>
					<div className="container-fluid page-content">
						<Pcase/>
						<SideBar className="col-md-4 col-sm-12 page-playlist"/>
					</div>
				</Route>
			</Switch>
		)
	}

	componentWillUnmount() {
		const { resetPcase, resetCategory } = this.props;

		resetPcase();
		resetCategory();
	}
};

const mapStateToProps = (state, ownProps) => {
	return {
		activeEnvironment: state.environments.active.environment,
		activeCategory: state.categories.active
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchCategory: (name, env) => dispatch(actions.fetchCategory(name, env)),
		resetCategory: () => dispatch(actions.resetCategory()),
		resetPcase: () => dispatch(actions.resetPcase())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Pcases);

