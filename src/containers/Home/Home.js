import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';

import TitleCard from '../../components/cards/TitleCard';

class Home extends Component {

	getPcasesByCategory() {
		const { categories } = this.props;
		let pcases = [];
		let pcasesSuccess = 0;
		let pcasesError = 0;

		if(categories.length > 0) {
			categories.map((cat)=> {

				if(cat.pcases != null) {
					pcases = pcases.concat(cat.pcases);

					cat.pcase_user_infos.map(info => {
						if(info.status === 'SUCCESS') {
							pcasesSuccess++;
						} else if(info.status === 'ERROR') {
							pcasesError++;
						}
					});
				}
			});
		}
		return {pcases, pcasesSuccess, pcasesError};
	}

	resetAllStatus = () => {
		const { user, resetAllPCaseUserInfo, fetchCategories } = this.props;

		resetAllPCaseUserInfo(user.id).then(() => {
			fetchCategories();
		});
	}

	render() {
		const { categories } = this.props;
		const { pcases, pcasesSuccess, pcasesError } = this.getPcasesByCategory();
		const pcasesWithStatus = (pcasesSuccess + pcasesError > 0);

		const categoriesList = categories.map((cat) => {
			const url = '/' + cat.slug;
			const pcasesLength = (cat.pcases) ? cat.pcases.length : 0;
			const style = cat.type === 'ADMIN' ? { color: '#ffffff' } :
				cat.type === 'SUPPORT' ? { color: '#f7ce5e' } : { color: '#367a00' };
			const path = this.props.location.query;

			return <tr key={cat.id}>
				<td className="link">
					<Link to={url} query={path} style={style} >{cat.name}</Link>
				</td>
				<td>{pcasesLength}</td>
			</tr>
		});

		return (
			<div className="container-fluid page-content">
				<div className="col-md-12">
					<div className="row" id="PCases" >
						<TitleCard mainClassName="col-md-3">
							<span className="title-card title-total">Total</span>
							<div className="information">
								<div className="graphic">
									<div className="pie" data-value-sucess="12" data-value-error="20" data-value-warning="40">
									</div>
								</div>
								<div className="dados">
									<span className="number">{pcases.length}</span>
								</div>
							</div>
						</TitleCard>
						<TitleCard mainClassName="col-md-3">
							<span className="title-card title-total">Sucesso</span>
							<div className="information">
								<div className="graphic">
									<div className="pie" data-value-sucess="12" data-value-error="20" data-value-warning="40">
									</div>
								</div>
								<div className="dados">
									<span className="number">{pcasesSuccess}</span>
								</div>
							</div>
						</TitleCard>
						<TitleCard mainClassName="col-md-3">
							<span className="title-card title-total">Bug</span>
							<div className="information">
								<div className="graphic">
									<div className="pie" data-value-sucess="12" data-value-error="20" data-value-warning="40">
									</div>
								</div>
								<div className="dados">
									<span className="number">{pcasesError}</span>
								</div>
							</div>
						</TitleCard>
					</div>
					<div className="row" id="list-category">
						<TitleCard mainClassName="long-card col-md-4">
								<div className="card-information long-card">
 									<span className="title-card">
									 	<span className="number-of-category">{categories.length} </span> Categorias {pcasesWithStatus > 0 ? <a href="javascript:;" onClick={this.resetAllStatus}>
										 <span style={{
											backgroundColor: '#334C5A',
											cursor: 'pointer'
											}} className="label">limpar status <i data-type='html5' className="fa fa-remove"></i></span></a> : '' }
										</span>
									<table className="list-information">
										<thead>
											<tr>
												<th>Nome</th>
												<th>Casos</th>
											</tr>
										</thead>
										<tbody>
											{categoriesList}
										</tbody>
									</table>
								</div>
						</TitleCard>
					</div>
				</div>
			</div>
		);
	}
};

//	Redux

const mapStateToProps = (state, ownProps) => {
	return {
		categories: state.categories.list.categories,
		user: state.auth
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		resetAllPCaseUserInfo: (userId) => dispatch(actions.resetAllPCaseUserInfo(userId)),
		fetchCategories: () => dispatch(actions.fetchCategories())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
