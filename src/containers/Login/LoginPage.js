import React, { Component, Fragment } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { LoginInputTemplate } from '../../components/inputs/FieldsTemplate';

import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Messages from '../../components/UI/Messages/Messages';
import Spinner from '../../components/UI/Spinner/Spinner';

import { updateObject, checkValidity, searchToObject} from '../../helpers/utility';

import * as actions from '../../store/actions';

const githubImage = require('../../images/icon-github.svg');
const ROOT_URL = '';

class LoginPage extends Component {

	state = {
		controls: {
			email: {
				elementType: 'input',
				elementConfig: {
					type: 'email',
					placeholder: 'Login email'
				},
				value: '',
				validation: {
					required: true,
					isEmail: true
				},
				valid: false,
				touched: false,
				wrapperCss: 'gmd-group-input',
				labelCss: ''
			},
			password: {
				elementType: 'input',
				elementConfig: {
					type: 'password',
					placeholder: 'Password'
				},
				value: '',
				validation: {
					required: true,
					minLength: 6
				},
				valid: false,
				touched: false,
				wrapperCss: 'gmd-group-input',
				labelCss: ''
			}
		}
	}

	inputChangedHandler = (event, controlName) => {
		const updatedControls = updateObject(this.state.controls, {
			[controlName]: updateObject(this.state.controls[controlName], {
				value: event.target.value,
				valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
				touched: true
			})
		});
		this.setState({
			controls: updatedControls
		});
	}

	handleSubmit = (event) => {
		event.preventDefault();
		this.props.onAuth(this.state.controls.email.value, this.state.controls.password.value);
	}

	githubLogin() {
		const redirectURL = '/v1/auth/github';
		window.location.href = redirectURL;
	}

	render() {
		const { loading, error, isAuthenticated, location } = this.props;
		let formElementsArray = [], message, view, form, query,
		redirectURL = (location.originalRoute) ? location.originalRoute : '/';

		for (let key in this.state.controls) {
			formElementsArray.push({
				id: key,
				config: this.state.controls[key]
			});
		}

		form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
				changed={( event ) => this.inputChangedHandler( event, formElement.id )}
				wrapperCss={formElement.config.wrapperCss}
				labelCss={formElement.config.labelCss} />
		));

		query = searchToObject(this.props.location.search);

		if(error)
			message = <Messages text={"Usuário ou senha inválidos"} className="alert alert-danger"/>

		if(loading) {
			view = <Spinner text="Autenticando..."/>
		} else {
			view =	<Fragment>
						<span className="description">
							<span className="">Playerground</span>
						</span>
						{message}
						<div className="popup-session-login">
							<button onClick={this.githubLogin} className="gmd gmd-shadow-1 gmd-waves black lighten-4 white-text  icon-btn-login">
								<span className="icon-login"><img src={githubImage} /></span>Login com GitHub
							</button>
							<span className="separador">OU</span>
							<form onSubmit={this.handleSubmit}>
								{form}
								<Button className="gmd gmd-shadow-1 gmd-waves black lighten-4 white-text btn-login"
									btnType="Success">SUBMIT</Button>
							</form>
						</div>
					</Fragment>
		}

		if (isAuthenticated)
			view = <Redirect to={{
				pathname: redirectURL,
				search: this.props.location.search
			}} />

		return (
			<div className="popup-cadastro login-cadastro-dev-and-test">
				{view}
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		loading: state.auth.loading,
		error: state.auth.error,
		isAuthenticated: state.auth.id != null,
		user: state.auth
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onAuth: (email, password) => dispatch(actions.auth(email, password))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);