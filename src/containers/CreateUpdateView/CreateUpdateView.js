import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Pcase from '../../components/forms/Pcase';
import Category from '../../components/forms/Category';
// import NewLabel from '../../components/forms/LabelComponent';

class Actions extends React.Component {
	passWithProps(ChildrenComponent, props) {
		return (<ChildrenComponent {...this.props} {...props} />);
	}

	resolveRender() {
		switch(this.props.params.action) {
			case 'pcase':
				return <NewPcase environment={this.props.environments.active.environment} category={this.props.categories.active.category} createPCase={this.props.createPCase} categoriesList={this.props.categories.list} user={this.props.auth.user} location={this.props.location}/>;
			case 'category':
				return <NewCategory createCategory={this.props.createCategory}/>;
			case 'label':
				return <NewLabel createLabel={this.props.createLabel}/>;
			default:
				return <div/>;
		}
	}

	render() {
		return (
			<div className="container-fluid page-content">
				<Switch>
					{/* <Route path="/new/pcase" exact render={props => (
						<NewPcase environment={this.props.environments.active.environment} category={this.props.categories.active.category} createPCase={this.props.createPCase} categoriesList={this.props.categories.list} user={this.props.auth.user} location={this.props.location}/>
					)} /> */}
					<Route path="/new/category" component={Category}/>
					<Route path="/new/pcase" component={Pcase}/>
					{/* <Route path="/new/label" exact render={props => (
						<NewLabel createLabel={this.props.createLabel}/>
					)} /> */}
				</Switch>
			</div>
		)
	}
}

export default Actions;

