import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import createHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import { createLogger } from 'redux-logger';

// //import the reducers
// import rootReducer from '../reducers/index';

// // Build the middleware for intercepting and dispatching navigation actions
// const middleware = routerMiddleware(history)
// const loggerMiddleware = createLogger();

// let store;
// const logger = queryToObj(window.location.href).logger;

// if (logger == null) {
//     store = createStore(rootReducer, applyMiddleware(thunk, middleware));
// } else if (logger == true) {
//     store = createStore(rootReducer, applyMiddleware(thunk, loggerMiddleware, middleware));
// }
// export const history = createHistory();

import authReducer from './reducers/auth';
import environmentsReducer from './reducers/environments';
import categoriesReducer from './reducers/categories';
import pcasesReducer from './reducers/pcases';
import searchReducer from './reducers/search';

const rootReducer = combineReducers({
	auth: authReducer,
	environments: environmentsReducer,
	categories: categoriesReducer,
	pcases: pcasesReducer,
	search: searchReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;