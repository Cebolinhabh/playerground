import axios from 'axios';
import * as actionTypes from '../actions/actionTypes';

const ROOT_URL = '/v1';

export function requestCategories() {
	return {
		type: actionTypes.FETCH_CATEGORIES_START
	};
};

export function fetchCategoriesSuccess(categories) {
	return {
		type: actionTypes.FETCH_CATEGORIES_SUCCESS,
		categories
	};
};

export function fetchCategoriesFailure(error) {
	return {
		type: actionTypes.FETCH_CATEGORIES_ERROR,
		error
	};
};

export function requestCategory() {
	return {
		type: actionTypes.FETCH_CATEGORY_START
	};
};

export function fetchCategorySuccess(category) {
	return {
		type: actionTypes.FETCH_CATEGORY_SUCCESS,
		category
	};
};

export function fetchCategoryFailure(error) {
	return {
		type: actionTypes.FETCH_CATEGORY_ERROR,
		error
	};
};

export function requestCreateCategory(category) {
	return {
		type: actionTypes.CREATE_CATEGORY_START
	};
};

export function createCategorySuccess(category) {
	return {
		type: actionTypes.CREATE_CATEGORY_SUCCESS,
		category
	};
};

export function resetCategory() {
	return {
		type: actionTypes.RESET_CATEGORY
	};
};

/** Async actions **/
export function createCategory(category) {
	return function(dispatch) {
		dispatch(requestCreateCategory(category));
		return axios.post(`${ROOT_URL}/categories`, category).then((response) => {
			dispatch(createCategorySuccess(response.data));
		}).catch((error) => {
			dispatch(fetchCategoriesFailure(error));
		});
	};
};

export function fetchCategories(params) {
	return function(dispatch) {
		dispatch(requestCategories());
		return axios.get(`${ROOT_URL}/categories`, {params}).then((response) => {
			dispatch(fetchCategoriesSuccess(response.data))
		}).catch((error) => {
			dispatch(fetchCategoriesFailure(error));
		});
	};

};

export function updatePCaseUserInfoByCategory(id) {
	return function(dispatch) {
		//dispatch(requestCategory());
		return axios.put(`${ROOT_URL}/categories/${id}/pcase_user_info`, {status: 'PENDING'}).then((response) => {
			//dispatch(fetchCategorySuccess(response.data))
		}).catch((error) => {
			//dispatch(fetchCategoriesFailure(error));
		});
	};
};

export function fetchCategory(name, params) {
	return function(dispatch) {
		dispatch(requestCategory());
		return axios.get(`${ROOT_URL}/categories/name/${name}`, {params}).then((response) => {
			dispatch(fetchCategorySuccess(response.data))
		}).catch((error) => {
			dispatch(fetchCategoryFailure(error));
		});
	};
};
