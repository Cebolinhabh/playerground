import axios from 'axios';
import * as actionTypes from '../actions/actionTypes';

const ROOT_URL = '/v1';

export function requestPCase() {
	return {
		type: actionTypes.FETCH_PCASE_START
	};
};

export function fetchPCaseSuccess(pcase) {
	return {
		type: actionTypes.FETCH_PCASE_SUCCESS,
		pcase
	};
};

export function fetchPCaseFailure(error) {
	return {
		type: actionTypes.FETCH_PCASE_ERROR,
		error
	};
};

export function requestSetFavPCase() {
	return {
		type: actionTypes.SET_FAV_PCASE_START
	};
};

export function setFavPCaseSuccess(id, flag) {
	return {
		type: actionTypes.SET_FAV_PCASE_SUCCESS,
		favPcase: {
			id,
			flag
		}
	};
};

export function requestFetchFavPCases() {
	return {
		type: actionTypes.FETCH_FAV_PCASES_START
	};
};

export function fetchPCasesFavSuccess(favPcases) {
	return {
		type: actionTypes.FETCH_FAV_PCASES_SUCCESS,
		favPcases
	};
};

export function requestCreatePCase() {
	return {
		type: actionTypes.CREATE_PCASE_START
	};
};

export function createPCaseSuccess(pcase) {
	return {
		type: actionTypes.CREATE_PCASE_SUCCESS,
		pcase
	};
};

export function requestUpdatePCase(pcase) {
	return {
		type: actionTypes.UPDATE_PCASE_START,
		pcase
	};
};

export function updatePCaseSuccess(pcase) {
	return {
		type: actionTypes.UPDATE_PCASE_SUCCESS,
		pcase
	};
};

export function requestDeletePCase(id) {
	return {
		type: actionTypes.DELETE_PCASE_START
	};
};

export function deletePCaseSuccess(pcaseId) {
	return {
		type: actionTypes.DELETE_PCASE_SUCCESS,
		pcaseId
	};
};

export function resetPcase() {
	return {
		type: actionTypes.RESET_PCASE
	}
}

/** Async actions **/
export function fetchPcase(id, envId, catId) {
	return function(dispatch) {
		dispatch(requestPCase());
		return axios.get(`${ROOT_URL}/pcases/${id}?env_id=${envId}&cat_id=${catId}`).then((response) => {
			dispatch(fetchPCaseSuccess(response.data));
		}).catch((error) => {
			dispatch(fetchPCaseFailure(error));
		});
	};
};

export function fetchFavPcases(obj) {
	return function(dispatch) {
		dispatch(requestFetchFavPCases());
		return axios.get(`${ROOT_URL}/pcases/favorite`, {
			params: obj
		}).then((response) => {
			dispatch(fetchPCasesFavSuccess(response.data))
		}).catch((error) => {
			dispatch(fetchPCaseFailure(error));
		});
	};
};

export function setFavPCase(id, flag) {
	return function(dispatch) {
		dispatch(requestSetFavPCase(id, flag));
		return axios.put(`${ROOT_URL}/pcases/${id}/favorite/${flag}`).then((response) => {
			dispatch(setFavPCaseSuccess(id, flag));
		}).catch((error) => {
			dispatch(fetchPCaseFailure(error));
		});
	};
};

export function createPCase(pcase) {
	return function(dispatch) {
		dispatch(requestCreatePCase(pcase));
		return axios.post(`${ROOT_URL}/pcases`, pcase).then((response) => {
			dispatch(createPCaseSuccess(response.data));
		}).catch((error, res) => {
			dispatch(fetchPCaseFailure(error));
		})
	};
};

export function updatePcaseUserInfo(pcase) {
	return function(dispatch) {
		dispatch(requestUpdatePCase(pcase));
		return axios.put(`${ROOT_URL}/pcases/${pcase.id}/user_info`, pcase.pcase_user_infos[0]).then((response) => {
			dispatch(updatePCaseSuccess(response.data));
		}).catch((error) => {
			dispatch(fetchPCaseFailure(error));
		});
	};
};

export function updatePCase(pcase) {
	const pcaseObj = {
		id: pcase.id,
		title: pcase.title,
		api: pcase.api,
		browsers: pcase.browsers,
		category_id: pcase.category_id,
		code: pcase.code,
		environment_id: pcase.environment_id,
		height: pcase.height,
		width: pcase.width,
		star: pcase.star,
		url: pcase.url,
		user_id: pcase.user_id,
		description: pcase.description,
		api_code: pcase.api_code
	};

	return function(dispatch) {
		dispatch(requestUpdatePCase(pcase));
		return axios.put(`${ROOT_URL}/pcases/${pcase.id}`, pcaseObj).then((response) => {
			dispatch(updatePCaseSuccess(response.data));
		}).catch((error) => {
			dispatch(fetchPCaseFailure(error));
		})
	}
}

export function deletePcase(id) {
	return function(dispatch) {
		dispatch(requestDeletePCase(id));
		return axios.delete(`${ROOT_URL}/pcases/${id}`).then((response) => {
			dispatch(deletePCaseSuccess(id));
		}).catch((error) => {
			dispatch(fetchPCaseFailure(error));
		});
	};
};

export function addLabelToPcase(id, label_id) {
	return function(dispatch) {
		return axios.put(`${ROOT_URL}/pcases/${id}/label/${label_id}`).then((response) => {
		}).catch((error) => {
		});
	};
};

export function removeLabelFromPcase(id, label_id) {
	return function(dispatch) {
		return axios.delete(`${ROOT_URL}/pcases/${id}/label/${label_id}`).then((response) => {
		}).catch((error) => {
		});
	};
};


//remover daqui
export function resetAllPCaseUserInfo(id) {
	return function(dispatch) {
		return axios.put(`${ROOT_URL}/users/${id}/resetStatus`, {status: 'PENDING'}).then((response) => {
		}).catch((error) => {
		});
	};
};
