import axios from 'axios';

import * as actionTypes from './actionTypes';

const ROOT_URL = '/v1';

export const setLoggedUserStart = () => {
	return {
		type: actionTypes.SET_LOGGED_USER_START,
		loading: true
	};
};

export const setLoggedUserSuccess = (user) => {
	return {
		type: actionTypes.SET_LOGGED_USER_SUCCESS,
		user
	};
};

export const setLoggedUserError = (error) => {
	return {
		type: actionTypes.SET_LOGGED_USER_ERROR,
		loading: false,
		error: error
	}
};

export const auth = (email, password) => {
	return dispatch => {
		dispatch(setLoggedUserStart());
		return axios.post(`${ROOT_URL}/auth`, {email, password})
			.then((response) => {
				dispatch(setLoggedUserSuccess(response.data.user));
			}).catch(error => {
				dispatch(setLoggedUserError(error));
			});
	};
};

export const authCheckState = () => {
	return dispatch => {
		if(envVariables.user) dispatch(setLoggedUserSuccess(envVariables.user));
	}
};
