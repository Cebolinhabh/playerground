import axios from 'axios';
import * as actionTypes from '../actions/actionTypes';

const ROOT_URL = '/v1';

export function requestLabels() {
	return {
		type: actionTypes.FETCH_LABEL_START
	};		
};

export function fetchLabelsSuccess(labels) {
	return {
		type: actionTypes.FETCH_LABEL_SUCCESS,
		labels
	};
};

export function fetchLabelsFailure(error) {
	return {
		type: actionTypes.FETCH_LABELS_ERROR,
		error
	};
};

export function requestLabel() {
	return {
		type: actionTypes.FETCH_LABEL_START
	};		
};

export function fetchLabelSuccess(label) {
	return {
		type: actionTypes.FETCH_LABEL_SUCCESS,
		label
	};
};

export function fetchLabelFailure(error) {
	return {
		type: actionTypes.FETCH_LABEL_ERROR,
		error
	};
};

export function setLabelActive(label) {
	return {
		type: actionTypes.SET_LABELS_ACTIVE,
		label
	};
};

export function requestCreateLabel(label) {
	return {
		type: actionTypes.CREATE_LABEL_START
	};		
};

export function createLabelSuccess(label) {
	return {
		type: actionTypes.CREATE_LABEL_SUCCESS,
		label
	};
};

/** Async actions **/
export function fetchLabels(cb) {
	return function(dispatch) {
		dispatch(requestLabels());
		return axios.get(`${ROOT_URL}/labels`).then((response) => {
			dispatch(fetchLabelsSuccess(response.data));
		}).catch((error) => {
			dispatch(fetchLabelsFailure(error));
		});
	};
};

export function fetchLabel(id, params) {
	return function(dispatch) {
		dispatch(requestLabel());
		return axios.get(`${ROOT_URL}/labels/${id}`, {params}).then((response) => {
			dispatch(fetchLabelSuccess(response.data));
		}).catch((error) => {
			dispatch(fetchLabelFailure(error));
		});
	};
};

export function createLabel(branch) {
	return function(dispatch) {
		dispatch(requestCreateLabel(branch));
		return axios.post(`${ROOT_URL}/labels`, branch).then((response) => {
			dispatch(createLabelSuccess(response.data));
		}).catch((error) => {
			dispatch(fetchLabelsFailure(error));
		})
	};
};