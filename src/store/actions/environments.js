import axios from 'axios';
import * as actionTypes from './actionTypes';

const ROOT_URL = '/v1';

export function requestEnvironments() {
	return {
		type: actionTypes.FETCH_ENVIRONMENTS_START
	};
};

export function fetchEnvironmentsSuccess(environments) {
	return {
		type: actionTypes.FETCH_ENVIRONMENTS_SUCCESS,
		environments
	};
};

export function fetchEnvironmentsError(error) {
	return {
		type: actionTypes.FETCH_ENVIRONMENTS_ERROR,
		error
	};
};

export function setEnvironmentActive(environment) {
	return {
		type: actionTypes.SET_ENVIRONMENT_ACTIVE,
		environment
	};
};

/** Async actions **/
export function fetchEnvironments(environment) {
	return function(dispatch) {
		dispatch(requestEnvironments());
		return axios.get(`${ROOT_URL}/environments`).then((response) => {
			let activeEnv;

			if (Object.keys(environment).length > 0) activeEnv = response.data.filter(env => env.id == environment.env_id)[0];
			else activeEnv = response.data.filter(env => env.isDefault)[0];
			
			dispatch(fetchEnvironmentsSuccess(response.data));
			dispatch(setEnvironmentActive(activeEnv));
		}).catch((error) => {
			dispatch(fetchEnvironmentsError(error));
		});
	};
};
