//	Auth Actions
export const SET_LOGGED_USER_START = 'SET_LOGGED_USER_START';
export const SET_LOGGED_USER_SUCCESS = 'SET_LOGGED_USER_SUCCESS';
export const SET_LOGGED_USER_ERROR = 'SET_LOGGED_USER_ERROR';

//  Env Actions
export const FETCH_ENVIRONMENTS_START = 'FETCH_ENVIRONMENTS_START';
export const FETCH_ENVIRONMENTS_SUCCESS = 'FETCH_ENVIRONMENTS_SUCCESS';
export const FETCH_ENVIRONMENTS_ERROR = 'FETCH_ENVIRONMENTS_ERROR';
export const SET_ENVIRONMENT_ACTIVE = 'SET_ENVIRONMENT_ACTIVE';

//  Categories
export const FETCH_CATEGORY_START = 'FETCH_CATEGORY_START';
export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';
export const FETCH_CATEGORY_ERROR = 'FETCH_CATEGORY_ERROR';
export const FETCH_CATEGORIES_START = 'FETCH_CATEGORIES_START';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CATEGORIES_ERROR = 'FETCH_CATEGORIES_ERROR';
export const RESET_CATEGORY_START = 'RESET_CATEGORY_START';
export const RESET_CATEGORY_SUCCESS = 'RESET_CATEGORY_SUCCESS';
export const CREATE_CATEGORY_START = 'CREATE_CATEGORY_START';
export const CREATE_CATEGORY_SUCCESS = 'CREATE_CATEGORY_SUCCESS';
export const RESET_CATEGORY = 'RESET_CATEGORY';

//  Pcases
export const FETCH_PCASE_START = 'FETCH_PCASE_START';
export const FETCH_PCASE_SUCCESS = 'FETCH_PCASE_SUCCESS';
export const FETCH_PCASE_ERROR = 'FETCH_PCASE_ERROR';
export const RESET_PCASE_START = 'RESET_PCASE_START';
export const SET_FAV_PCASE_START = 'SET_FAV_PCASE_START';
export const SET_FAV_PCASE_SUCCESS = 'SET_FAV_PCASE_SUCCESS';
export const FETCH_FAV_PCASES_START = 'FETCH_FAV_PCASES_START';
export const FETCH_FAV_PCASES_SUCCESS = 'FETCH_FAV_PCASES_SUCCESS';
export const CREATE_PCASE_START = 'CREATE_PCASE_START';
export const CREATE_PCASE_SUCCESS = 'CREATE_PCASE_SUCCESS';
export const UPDATE_PCASE_START = 'UPDATE_PCASE_START';
export const UPDATE_PCASE_SUCCESS = 'UPDATE_PCASE_SUCCESS';
export const DELETE_PCASE_START = 'DELETE_PCASE_START';
export const DELETE_PCASE_SUCCESS = 'DELETE_PCASE_SUCCESS';
export const RESET_PCASE = 'RESET_PCASE';

//  Labels
export const FETCH_LABELS_START = 'FETCH_LABELS_START';
export const FETCH_LABELS_SUCCESS = 'FETCH_LABELS_SUCCESS';
export const FETCH_LABELS_ERROR = 'FETCH_LABELS_ERROR';
export const FETCH_LABEL_START = 'FETCH_LABEL_START';
export const FETCH_LABEL_SUCCESS = 'FETCH_LABEL_SUCCESS';
export const FETCH_LABEL_ERROR = 'FETCH_LABEL_ERROR';
export const CREATE_LABEL_START = 'CREATE_LABEL_START';
export const CREATE_LABEL_SUCCESS = 'CREATE_LABEL_SUCCESS';
export const SET_LABELS_ACTIVE = 'SET_LABELS_ACTIVE';

//  Search
export const SEARCH_START = 'SEARCH_START';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const SEARCH_ERROR = 'SEARCH_ERROR';
export const CLEAR_SEARCH = 'CLEAR_SEARCH';