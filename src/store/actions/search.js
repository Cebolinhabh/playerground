import axios from 'axios';
import * as actionTypes from './actionTypes';

const ROOT_URL = '/v1';

export function requestSearch(query, device) {
	return {
		type: actionTypes.SEARCH_START,
		query,
		device
	};
};

export function searchSuccess(pcases) {
	return {
		type: actionTypes.SEARCH_SUCCESS,
		pcases
	};
};

export function searchFailure(error) {
	return {
		type: actionTypes.SEARCH_ERROR,
		error
	};
};

export function clearSearch() {
	return {
		type: actionTypes.CLEAR_SEARCH
	};
};

/** Async actions **/
export function fetchSearch(query, envId, device) {
	return function(dispatch) {
		dispatch(requestSearch(query, device));
		return axios.get(`${ROOT_URL}/pcases/search`, {
			params: {
				q: query,
				env: envId
			}
		}).then((response) => {
			dispatch(searchSuccess(response.data))
		}).catch((error) => {
			dispatch(searchFailure(error));
		});
	};
};
