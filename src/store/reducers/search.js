import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../helpers/utility';

const initialState = {
	query: '',
	pcases: [],
	error: null,
	loading: false,
	active: false,
	device: 'DESKTOP'
};

const searchStart = (state, action) => {
	return updateObject(state, {
		query: action.query,
		device: action.device,
		loading: true,
		active: true
	});
};

const searchSuccess = (state, action) => {
	return updateObject(state, {
		pcases: action.pcases,
		loading: false
	});
};

const searchError = (state, action) => {
	return updateObject(state, {
		loading: false,
		error: action.error,
		active: true
	});
};

const searchClear = (state, action) => {
	return updateObject(state, {
		query: '',
		pcases: [],
		error: null,
		active: false,
		loading: false
	});
};

const reducer = (state=initialState, action) => {
	switch(action.type) {
		case actionTypes.SEARCH_START: return searchStart(state, action);
		case actionTypes.SEARCH_SUCCESS: return searchSuccess(state, action); 
		case actionTypes.SEARCH_ERROR: return searchError(state, action); 
		case actionTypes.CLEAR_SEARCH: return searchClear(state, action);
		default: return state;	
	};
};

export default reducer;
