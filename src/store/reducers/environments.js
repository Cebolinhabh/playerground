import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../helpers/utility';

const initialState = {
	list: { environments: [], error: null, loading: false },
	active: { environment: null, error: null, loading: false }
};

const environmentsStart = (state, action) => {
	return updateObject(state, {
		list: {
			environments: [], error: null, loading: true
		}
	});
};

const environmentsSuccess = (state, action) => {
	return updateObject(state, {
		list: {
			environments: action.environments, error: null, loading: false
		}
	});
};

const environmentsError = (state, action) => {
	return updateObject(state, {
		list: {
			environments: [], error: action.error, loading: false
		}
	});	
};

const setEnvironmentActive = (state, action) => {
	return updateObject(state, {
		active: {
			environment: action.environment, error: null, loading: false
		}
	});
};

const reducer = (state=initialState, action) => {
	switch(action.type) {
		case actionTypes.FETCH_ENVIRONMENTS_START:  return environmentsStart(state, action);
		case actionTypes.FETCH_ENVIRONMENTS_SUCCESS: return environmentsSuccess(state, action);
		case actionTypes.FETCH_ENVIRONMENTS_ERROR: return environmentsError(state, action);
		case actionTypes.SET_ENVIRONMENT_ACTIVE: return setEnvironmentActive(state, action);
		default: return state;	
	};
};

export default reducer;
