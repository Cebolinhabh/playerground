import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../helpers/utility';

const initialState = {
	list: { categories: [], error: null, loading: false },
	created: { category: null, error: null, loading: false },
	active: { category: null, error: null, loading: false }
};

const categoriesStart = (state, action) => {
	return updateObject(state, {
		list: {
			categories: [], error: null, loading: true
		}
	});
};

const categoriesSuccess = (state, action) => {
	return updateObject(state, {
		list: {
			categories: action.categories, error: null, loading: false
		}
	});
};

const categoriesError = (state, action) => {
	return updateObject(state, {
		list: {
			categories: [], error: true, loading: false
		}
	});
};

const setCategoryActive = (state, action) => {
	return updateObject(state, {
		active: {
			category: action.enviroment, error: null, loading: false
		}
	});
};

const categoryStart = (state, action) => {
	return updateObject(state, {
		active: {
			category: null, error: null, loading: true
		}
	});
};

const categorySuccess = (state, action) => {
	return updateObject(state, {
		active: {
			category: action.category, error: null, loading: false
		}
	});
};

const categoryError = (state, action) => {
	return updateObject(state, {
		list: {
			category: null, error: action.error, loading: false
		}
	});
};

const createCategoryStart = (state, action) => {
	return updateObject(state, {
		created: {
			category: null, error: null, loading: true
		}
	});
};

const createCategorySuccess = (state, action) => {
	const categories = state.list.categories.concat(action.category);

	return updateObject(state, {
		created: {
			category: action.category, error: null, loading: false
		},
		list: {
			categories: categories, error: null, loading: false
		}
	});
};

const resetCategory = (state, action) => {
	return updateObject(state, {
		active: { category: null, error: null, loading: false }
	});
};

function categories(state = initialState, action) {
	let error;
	switch (action.type) {
		case actionTypes.FETCH_CATEGORIES_START: return categoriesStart(state, action);
		case actionTypes.FETCH_CATEGORIES_SUCCESS: return categoriesSuccess(state, action);
		case actionTypes.FETCH_CATEGORIES_ERROR: return categoriesError(state, action);
		case actionTypes.FETCH_CATEGORY_START: return categoryStart(state, action);
		case actionTypes.FETCH_CATEGORY_SUCCESS: return categorySuccess(state, action);
		case actionTypes.FETCH_CATEGORY_ERROR: return categoryError(state, action);
		case actionTypes.CREATE_CATEGORY_START: return createCategoryStart(state, action);
		case actionTypes.CREATE_CATEGORY_SUCCESS: return createCategorySuccess(state, action);
		case actionTypes.RESET_CATEGORY: return resetCategory(state, action);
		default: return state;
	}
}

export default categories;