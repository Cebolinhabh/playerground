import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../helpers/utility';

const initialState = {
	id: null,
	github_id: null,
	email: null,
	avatar_url: null,
	name: null,
	role: null,
	error: null,
	loading: false
};

const authSuccess = (state, action) => {
	return updateObject(state, {
		...action.user,
		loading: false,
		error: null
	})
};

const authStart = (state, action) => {
	return updateObject(state, {
		loading: true
	});
};

const authError = (state, action) => {
	return updateObject(state, {
		loading: false,
		error: action.error
	})
};

const reducer = (state=initialState, action) => {
	switch(action.type) {
		case actionTypes.SET_LOGGED_USER_START: return authStart(state, action)
		case actionTypes.SET_LOGGED_USER_SUCCESS: return authSuccess(state, action)
		case actionTypes.SET_LOGGED_USER_ERROR: return authError(state, action)
		default: return state;
	};
};

export default reducer;

