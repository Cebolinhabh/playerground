import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../helpers/utility';

const initialState = {
	list: { pcases: [], error: null, loading: false },
	created: { pcase: null, error: null, loading: false },
	active: { pcase: null, error: null, loading: false },
	favorite: { pcases: [], error: null, loading: false }
};

const pcaseStart = (state, action) => {
	return updateObject(state, {
		active: {
			pcase: null, error: null, loading: true
		}
	});
};

const pcaseSuccess = (state, action) => {
	return updateObject(state, {
		active: {
			pcase: action.pcase, error: null, loading: false
		}
	});
};

const pcaseError = (state, action) => {
	return updateObject(state, {
		active: {
			pcase: null, error: action.error, loading: false
		}
	});
};

const pcaseFavSuccess = (state, action) => {
	return updateObject(state, {
		favorite: {
			pcases: action.favPcases, error: null, loading: false
		}
	});
};

const setPcaseFav = (state, action) => {
	return updateObject(state, {
		favorite: {
			pcases: state.pcases, error: null, loading: true
		}
	});
};

const setPcaseFavSuccess = (state, action) => {
	const pcase = getPCaseById(state.list.pcases, action.id);
	return updateObject(state, {
		favorite: {
			pcases: state.pcases, error: null, loading: true
		}
	});
};

const createPcaseStart = (state, action) => {
	return updateObject(state, {
		created: {
			pcase: null, error: null, loading: true
		}
	});
};

const createPcaseSuccess = (state, action) => {
	return updateObject(state, {
		created: {
			pcase: action.pcase, error: null, loading: false
		}
	});
};

const updatePcase = (state, action) => {
	return updateObject(state, {
		active: {
			pcase: action.pcase, error: null, loading: true
		}
	});
};

const updatePcaseSuccess = (state, action) => {
	const updated = action.pcase;
	const updatedPcase = {...state.active.pcase, updated};

	return updateObject(state, {
		created: {
			pcase: updatedPcase, error: null, loading: false
		}
	});
};

const deletePcaseSuccess = (state, action) => {
	return updateObject(state, {
		active: {
			pcase: null, error: null, loading: false
		}
	});
};

const resetPcase = (state, action) => {
	return updateObject(state, {
		active: {
			pcase: null, error: null, loading: false
		}
	});
};

const reducer = (state=initialState, action) => {
	switch(action.type) {
		case actionTypes.FETCH_PCASE_START: return pcaseStart(state, action);
		case actionTypes.FETCH_PCASE_SUCCESS: return pcaseSuccess(state, action);
		case actionTypes.FETCH_PCASE_ERROR: return pcaseError(state, action);
		case actionTypes.SET_FAV_PCASE_START: return setPcaseFav(state, action);
		case actionTypes.SET_FAV_PCASE_SUCCESS: return setPcaseFavSuccess(state, action);
		case actionTypes.FETCH_FAV_PCASES_SUCCESS: return pcaseFavSuccess(state, action);
		case actionTypes.CREATE_PCASE_START: return createPcaseStart(state, action);
		case actionTypes.CREATE_PCASE_SUCCESS: return createPcaseSuccess(state, action);
		case actionTypes.UPDATE_PCASE_START: return updatePcase(state, action);
		case actionTypes.UPDATE_PCASE_SUCCESS: return updatePcaseSuccess(state, action);
		case actionTypes.DELETE_PCASE_SUCCESS: return deletePcaseSuccess(state, action);
		case actionTypes.RESET_PCASE: return resetPcase(state, action);
		default: return state;
	}
}

export default reducer;

/** Utils **/
const getPcaseById = (id, pcases) => {
	return pcases.find(pcase => pcase.id == id);
};

const getIndexPcaseById = (id, pcases) => {
	return pcases.map((pcase) => { return pcase.id; }).indexOf(id);
};