import * as actionTypes from '../actions/actionTypes';

const initialState = {
	list: { labels: [], error: null, loading: false },
	created: { label: null, error: null, loading: false },
	active: { label: null, error: null, loading: false },
	deleted: { labels: [], error: null, loading: false }
};	

const labelStart = (state, action) => {
	return updateObject(state, {
		active: {
			label: null, error: null, loading: true
		}
	});
};

const labelSuccess = (state, action) => {
	return updateObject(state, {
		active: {
			label: action.label, error: null, loading: false
		}
	});
};

const labelError = (state, action) => {
	return updateObject(state, {
		active: {
			label: null, error: action.error, loading: false
		}
	});	
};

const labelsStart = (state, action) => {
	return updateObject(state, {
		list: {
			labels: [], error: null, loading: true
		}
	});
};

const labelsSuccess = (state, action) => {
	return updateObject(state, {
		list: {
			labels: action.labels, error: null, loading: false
		}
	});
};

const labelsError = (state, action) => {
	return updateObject(state, {
		list: {
			labels: [], error: error, loading: false
		}
	});
};

const createLabelSuccess = (state, action) => {
	state.list.labels = state.list.labels.concat(action.label);
	return updateObject(state, {
		list: {
			labels: state.list.labels, error: null, loading: false
		}
	});
};

const reducer = (state=initialState, action) => {
	switch(action.type) {
		case actionTypes.FETCH_LABEL_START: return labelStart(state, action); 
		case actionTypes.FETCH_LABEL_SUCCESS: return labelSuccess(state, action);
		case actionTypes.FETCH_LABEL_ERROR: return labelError(state, action);
		case actionTypes.FETCH_LABELS_START: return labelsStart(state, action);
		case actionTypes.FETCH_LABELS_SUCCESS: return labelsSuccess(state, action);
		case actionTypes.FETCH_LABELS_ERROR: return labelsError(state, action);
		case actionTypes.CREATE_LABEL_SUCCESS: return createLabelSuccess(state, action); 
		default: return state;	
	};
};

export default reducer;