import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';

import * as actions from './store/actions';

import Layout from './hoc/MainLayout/MainLayout';
import LoginPage from './containers/Login/LoginPage';

class App extends Component {
	componentDidMount() {
		this.props.onTrySigned();
	}

	render() {
		let routes;
		if ( this.props.isAuthenticated ) {
			routes = (
				<Switch>
					<Route path="/login" component={LoginPage}/>
					<Route path="/" component={Layout}/>
					<Redirect to={{
						pathname: '/',
						search: this.props.location.search
					}}/>
				</Switch>
			)
		} else {
			routes = (
				<Switch>
					<Route path="/login" exact component={LoginPage}/>
					<Redirect to={{
						pathname: '/login',
						search: this.props.location.search,
						originalRoute: this.props.location.pathname
					}}/>
				</Switch>
			)
		}

		return (
			<Fragment>
				{routes}
			</Fragment>
		)
	}
}

//	Redux
const mapStateToProps = state => {
	return {
		isAuthenticated: state.auth.id !== null,
		user: state.auth
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onTrySigned: () => dispatch(actions.authCheckState())
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
