const categories = [
  {
    'id': 1,
    'name': 'Advertising'
  },
  {
    'id': 2,
    'name': 'Overlay'
  },
  {
    'id': 3,
    'name': 'HLS'
  },
  {
    'id': 4,
    'name': 'Live'
  },
  {
    'id': 5,
    'name': 'Live Rail'
  }  
];

export default categories;