const users = [
  {
    'id': 1,
    'name': 'Thiago Miranda de Oliveira',
    'email': 'thiago.miranda@sambatech.com.br',
    'password': 'samba123',
    'created_at': '2016-08-31T15:36:05.000Z',
    'updated_at': '2016-08-31T15:36:05.000Z'
  },
  {
    'id': 2,
    'name': 'Leandro Zanol',
    'email': 'leandro.zanol@sambatech.com.br',
    'password': 'samba456',
    'created_at': '2016-08-31T15:36:05.000Z',
    'updated_at': '2016-08-31T15:36:05.000Z'
  },
  {
    'id': 3,
    'name': 'Priscila Magalhães',
    'email': 'priscila.magalhaes@sambatech.com.br',
    'password': 'samba789',
    'created_at': '2016-08-31T15:36:05.000Z',
    'updated_at': '2016-08-31T15:36:05.000Z'
  }
];

export default users;