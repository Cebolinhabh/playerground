const environments = [
  {
    'id': 1,
    'name': 'web4-7091',
    'url': '//playerapitest2.liquidplatform.com:7091/embed/'
  },
  {
    'id': 2,
    'name': 'web4-7021',
    'url': '//playerapitest2.liquidplatform.com:7021/embed/'
  },
  {
    'id': 3,
    'name': 'staging',
    'url': '//staging.webtv.liquidplatform.com/pApiv2/embed/'
  },
  {
    'id': 4,
    'name': 'prod',
    'url': '//fast.player.liquidplatform.com/pApiv2/embed/'
  }
];

export default environments;