const browsers = [
  {
    'id': 1,
    'name': 'Google Chrome',
    'mobile': false,
    'icon': 'chrome'
  },
  {
    'id': 2,
    'name': 'Internet Explorer 10',
    'mobile': false,
    'icon': 'ie'
  },
  {
    'id': 3,
    'name': 'Firefox',
    'mobile': false,
    'icon': 'firefox'
  },
  {
    'id': 4,
    'name': 'Google Chrome',
    'mobile': true,
    'icon': 'chrome'
  }
];

export default browsers;