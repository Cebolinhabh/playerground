const pcases = [
      {
        'id': 1,
        'title': 'Teste IMA3',
        'description': 'Testando IMA3',
        'width': '640px',
        'height': '480px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': false,
        'browsers': '1,2',
        'environment_id': 1,
        'category_id': 1,
        'user_id': 1,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 1,
          'name': 'Thiago Miranda de Oliveira',
          'email': 'thiago.miranda@sambatech.com.br'
        },
        'environment': {
          'url': '//playerapitest2.liquidplatform.com:7091/embed/'
        }
      },
      {
        'id': 4,
        'title': 'Teste HLS Akamai',
        'description': 'Testando AES Akamai HLS',
        'width': '500px',
        'height': '300px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': false,
        'browsers': '1,2',
        'environment_id': 1,
        'category_id': 3,
        'user_id': 1,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 1,
          'name': 'Thiago Miranda de Oliveira',
          'email': 'thiago.miranda@sambatech.com.br'
        },
        'environment': {
          'url': '//playerapitest2.liquidplatform.com:7091/embed/'
        }
      },
      {
        'id': 5,
        'title': 'Teste Live SBT',
        'description': 'Live de desenho SBT',
        'width': '720px',
        'height': '480px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': true,
        'browsers': '1,2',
        'environment_id': 3,
        'category_id': 4,
        'user_id': 1,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 1,
          'name': 'Thiago Miranda de Oliveira',
          'email': 'thiago.miranda@sambatech.com.br'
        },
        'environment': {
          'url': '//staging.webtv.liquidplatform.com/pApiv2/embed/'
        }
      },
      {
        'id': 2,
        'title': 'Teste Live Rail',
        'description': 'Teste liverail pre roll ',
        'width': '100%',
        'height': '100%',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': false,
        'browsers': '1,2',
        'environment_id': 1,
        'category_id': 2,
        'user_id': 2,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 2,
          'name': 'Leandro Zanol',
          'email': 'leandro.zanol@sambatech.com.br'
        },
        'environment': {
          'url': '//playerapitest2.liquidplatform.com:7091/embed/'
        }
      },
      {
        'id': 3,
        'title': 'Teste HLS Wowza',
        'description': 'Teste das novas definições Wowza HLS',
        'width': '720px',
        'height': '360px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': true,
        'browsers': '1,2,3',
        'environment_id': 2,
        'category_id': 3,
        'user_id': 2,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 2,
          'name': 'Leandro Zanol',
          'email': 'leandro.zanol@sambatech.com.br'
        },
        'environment': {
          'url': '//playerapitest2.liquidplatform.com:7021/embed/'
        }
      },
      {
        'id': 9,
        'title': 'Teste com Live Dinamarca',
        'description': 'Teste com o live dinamarques',
        'width': '720px',
        'height': '360px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': true,
        'browsers': '1,2',
        'environment_id': 2,
        'category_id': 4,
        'user_id': 2,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 2,
          'name': 'Leandro Zanol',
          'email': 'leandro.zanol@sambatech.com.br'
        },
        'environment': {
          'url': '//playerapitest2.liquidplatform.com:7021/embed/'
        }
      },
      {
        'id': 6,
        'title': 'Teste com overlay',
        'description': 'Não sei o que é overlay',
        'width': '640px',
        'height': '360px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': false,
        'browsers': '1,2',
        'environment_id': 1,
        'category_id': 2,
        'user_id': 3,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 3,
          'name': 'Priscila Magalhães',
          'email': 'priscila.magalhaes@sambatech.com.br'
        },
        'environment': {
          'url': '//playerapitest2.liquidplatform.com:7091/embed/'
        }
      },
      {
        'id': 7,
        'title': 'Teste com overlay mini',
        'description': 'To com sono e não sei o que é overlay',
        'width': '240px',
        'height': '180px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e/eca15e688910ec384bc8bce8f59db420',
        'code': null,
        'api': false,
        'star': false,
        'browsers': '1,2',
        'environment_id': 1,
        'category_id': 2,
        'user_id': 3,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 3,
          'name': 'Priscila Magalhães',
          'email': 'priscila.magalhaes@sambatech.com.br'
        },
        'environment': {
          'url': '//playerapitest2.liquidplatform.com:7091/embed/'
        }
      },
      {
        'id': 8,
        'title': 'Teste com áudio HLS',
        'description': 'Teste com o áudio hls',
        'width': '480px',
        'height': '102px',
        'url': '18f78d75bc3bbb24054da7ea80633b0e?alternativeLive=/live.m3u8&type=AUDIO',
        'code': null,
        'api': false,
        'star': false,
        'browsers': '1,2',
        'environment_id': 4,
        'category_id': 3,
        'user_id': 3,
        'created_at': '2016-09-01T13:06:17.000Z',
        'updated_at': '2016-09-01T13:06:17.000Z',
        'user': {
          'id': 3,
          'name': 'Priscila Magalhães',
          'email': 'priscila.magalhaes@sambatech.com.br'
        },
        'environment': {
          'url': '//fast.player.liquidplatform.com/pApiv2/embed/'
        }
      }
    ];

export default pcases;