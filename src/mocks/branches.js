const branches = [
  {
    'id': 1,
    'name': 'master',
    'gitURL': 'https://github.com/sambatech-desenv/player',
    'scriptURL': null,
    'status': 'PENDING',
    'created_at': '2016-08-31T15:36:05.000Z',
    'updated_at': '2016-08-31T15:36:05.000Z'
  },
  {
    'id': 2,
    'name': 'ft-closeCaption-543',
    'gitURL': 'https://github.com/sambatech-desenv/player/tree/ft-closeCaption-543',
    'scriptURL': null,
    'status': 'SUCCESS',
    'created_at': '2016-08-31T15:36:05.000Z',
    'updated_at': '2016-08-31T15:36:05.000Z'
  }
];

export default branches;