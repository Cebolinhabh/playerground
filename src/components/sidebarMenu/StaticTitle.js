'use strict';

import React from 'react';

class StaticTitle extends React.Component {

  render() {
    const {id, name, url, description} = this.props.active;

    return (
   		<div className="page-playlist-title cat-label">
  			<h2>{name}</h2>
  			<p>{description}</p>
            <a href={url} target="_blank">{url}</a>
  		</div>
    );
  }

}

StaticTitle.displayName = 'SidebarMenuStaticTitleComponent';

// Uncomment properties you need
// SliderComponent.propTypes = {};
// SliderComponent.defaultProps = {};

export default StaticTitle;
