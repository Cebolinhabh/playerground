'use strict';

import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import { getClassByStatus, treatDate } from '../../helpers/utility';

const list = (props) => {
	const { pcases, match, currentCategory } = props;
	let resetCasesHtml;

	const pcasesList = pcases.map((pcase) => {
		const url = `/${currentCategory.slug}/pcases/${pcase.id}`;
		const createdAt = treatDate(pcase.created_at);
		let className = pcase.pcase_user_infos[0] ? getClassByStatus(pcase.pcase_user_infos[0].status) : '';

		className = (match.params.pcase_id == pcase.id) ? `${className} active` : className;

		return (
			<li key={pcase.id} className={className}>
				<Link to={{
						pathname: url,
						search: props.location.search
					}}>
					<span className="title">{pcase.title}</span>
					<span className="publish-date">{createdAt}</span>
					<span className="description">{pcase.description}</span>
				</Link>
			</li>
		)
	});

	if ((currentCategory.pcase_user_infos && currentCategory.pcase_user_infos.length > 0)) {
		resetCasesHtml = <li onClick={props.resetStatus} className="clear">
							<a href="#">
								<span>Limpar status</span>
								<i className="fa fa-times" aria-hidden="true"></i>
							</a>
						</li>;
	}
	

	return (
		<ul className="list-unstyled">{resetCasesHtml}{pcasesList}</ul>
	)
};

export default withRouter(list);