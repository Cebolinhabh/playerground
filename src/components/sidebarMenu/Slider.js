'use strict';

import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';

const slider = (props) => {
	const { categories, currentCategory } = props;
	let currentCategoryIndex, leftArrow, rightArrow, slider, style;
	
	if (categories && currentCategory) {
		currentCategoryIndex = categories.map((cat) => cat.id).indexOf(currentCategory.id);
		leftArrow = (categories[currentCategoryIndex - 1]) ?
			renderArrow(categories[currentCategoryIndex - 1].slug, 'prev', props.onArrowClick) : <a className="playlist-icon" />;
		rightArrow = (categories[currentCategoryIndex + 1]) ?
			renderArrow(categories[currentCategoryIndex + 1].slug, 'next', props.onArrowClick) : <a className="playlist-icon" />;

		style = currentCategory.type === 'ADMIN' ? { color: '#ffffff' } :
			currentCategory.type === 'SUPPORT' ? { color: '#f7ce5e' } : { color: '#367a00' };

		slider = (
			<Fragment>
				{leftArrow}
				<h2 style={style}>{currentCategory.name}</h2>
				{rightArrow}
			</Fragment>
		);
	} else {
		slider = (
			<h2>&nbsp;</h2>
		)
	}

	return (
		<div className="page-playlist-title">
			{slider}
		</div>
	);
};

const renderArrow = (slug, cssClass, trigger) => {
	const css1 = `playlist-icon playlist-title-${cssClass}`;
	const css2 = `icon-14-bread-crumb-${cssClass}-white`;
	return <a onClick={(event) => { trigger(event)}} data-slug={slug} href="javascript:;" className={css1}><i data-slug={slug} className={css2}></i></a>;
}

export default slider;
