import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as actions from '../../store/actions';

import Spinner from '../UI/Spinner/Spinner';
import Slider from './Slider';
import SItems from './List';
import StaticTitle from './StaticTitle';

class Sidebar extends Component {

	componentDidMount() {
		const { activeEnvironment, fetchCategory, activeCategory, match } = this.props;
		
		if (activeCategory.category && activeCategory.category.slug !== match.params.category_slug)
			fetchCategory(match.params.category_slug, { env: activeEnvironment.id });

		if (activeEnvironment && !activeCategory.category)
			fetchCategory(match.params.category_slug, { env: activeEnvironment.id });
	}

	componentWillReceiveProps(nextProps) {
		const { activeEnvironment, fetchCategory, activeCategory, match } = nextProps;

		if (activeEnvironment && !activeCategory.loading && (this.props.match.params.category_slug !== nextProps.match.params.category_slug || !activeCategory.category))
			fetchCategory(match.params.category_slug, { env: activeEnvironment.id });
	}

	changeCategory = (event) => {
		const { user, fetchCategory, activeEnvironment } = this.props;
		const catSlug = event.target.getAttribute('data-slug');

		this.props.fetchCategory(catSlug, { env: activeEnvironment.id });
		event.preventDefault();
	}

	resetStatus = () => {
		const { fetchCategory, updatePCaseUserInfoByCategory, activeEnvironment, activeCategory } = this.props;

		this.props.updatePCaseUserInfoByCategory(activeCategory.category.id).then(fetchCategory(activeCategory.category.id,  { env: activeEnvironment.id }));
	}

	render() {
		const { categories, activeCategory, className } = this.props;
		const currentCategory = activeCategory.category;

		if (!currentCategory) return <div />;

		return (
			<div className={className}>
				<Slider onArrowClick={(event) => this.changeCategory(event)} categories={categories} currentCategory={currentCategory} />
				<div className="page-playlist-list gmd-shadow-1">
					<SItems pcases={currentCategory.pcases} currentCategory={currentCategory} resetStatus={this.resetStatus} />
				</div>
			</div>
		)
	}

};

const mapStateToProps = (state, ownProps) => {
	return {
		categories: state.categories.list.categories,
		activeCategory: state.categories.active,
		user: state.auth,
		activeEnvironment: state.environments.active.environment
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchCategory: (name, env) => dispatch(actions.fetchCategory(name, env)),
		updatePCaseUserInfoByCategory: (catId) => dispatch(actions.updatePCaseUserInfoByCategory(catId))
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidebar));