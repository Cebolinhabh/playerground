'use strict';

import React from 'react';

const messages = (props) => (
	<div className={props.className} role="alert">
		<a href="#" onClick={props.close} className="close" data-dismiss="alert" aria-label="close" title="close">×</a>{props.text}
	</div>
);

export default messages;
