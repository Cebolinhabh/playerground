import React from 'react';

const input = ( props ) => {
    let inputElement = null;
    const inputClasses = [];

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push('invalid');
    }
    
    switch ( props.elementType ) {
        case ( 'input' ):
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed}
                onBlur={props.blur ? props.blur: null} />;
            break;
        case ( 'textarea' ):
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
            break;
        case ( 'select' ):
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    value={props.value}
                    onChange={props.changed}>
                    {props.elementConfig.options.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>
            );
            break;
        case ( 'checkbox' ):
            inputElement = (
                <div>
                    <input
                        className={inputClasses.join(' ')}
                        value={props.value}
                        onChange={props.changed}
						type={props.elementType}
						checked={props.value}
                        {...props.elementConfig}
                        id={props.label.toLowerCase()}>
                    </input>
                    <label htmlFor={props.label.toLowerCase()}>{props.label}</label>
                </div>
            );
            break;
        default:
            inputElement = <input
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />;
    }

    return (
        <div className={props.wrapperCss}>
            {props.elementType !== 'checkbox' ? <label className={props.labelCss}>{props.label}</label> : ''}
            {inputElement}
            {props.sibling}
        </div>
    );

};

export default input;