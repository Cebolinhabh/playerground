import React from 'react';

const formActions = (props) => (
	<div className="group-form group-btn">
		<button disabled={props.disabled} className="btn btn-primary" type="submit">Salvar</button>
		<button onClick={props.reset} className="btn btn-primary btn-reset" type="reset">Cancelar</button>
	</div>
);

export default formActions;