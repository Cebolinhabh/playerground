'use strict';

import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { updateObject } from '../../helpers/utility';
import * as actions from '../../store/actions';

import Input from '../UI/Input/Input';

class HeaderComponent extends Component {

	state = {
		changeEnvForm: {
			environment: {
				id: 'environment',
				elementType: 'select',
				elementConfig: {
					options: []
				},
				value: '',
				validation: {},
				valid: true,
				wrapperCss: 'select-branch hide-mobile',
				labelCss: ''
			}
		},
		searchForm: {
			id: 'search',
			search: {
				elementType: 'input',
				elementConfig: {
					type: 'search',
					placeholder: 'Buscar'
				},
				sibling: <button type="reset"><i className="fa fa-search"></i></button>,
				value: '',
				validation: {},
				valid: true,
				touched: false
			}
		}
	};

	selectChangedHandler = (event, id) => {
		const { environments, history } = this.props;
		const updatedEnv = updateObject(this.state.changeEnvForm, {
			environment: updateObject(this.state.changeEnvForm.environment, {
				value: event.target.value,
				touched: true
			})
		});

		this.setState({changeEnvForm: updatedEnv});

		//	Changing environment
		const environment = environments.find((env) => env.id == event.target.value);
		let query;

		this.props.setEnvironmentActive(environment);

		if(!environment.isDefault) query = `?env_id=${environment.id}`;
		else query = '';
		history.push(`/${query}`);
	}

	searchChangedHandler = (event) => {
		const value = event.target.value;
		const updatedSearch = updateObject(this.state.searchForm, {
			search: updateObject(this.state.searchForm.search, {
				value,
				touched: true
			})
		});
		this.setState({searchForm: updatedSearch});

		if (value.length > 3) {
			this.props.fetchSearch(value, this.props.activeEnvironment.id, 'DESKTOP');
		} else {
			this.props.clearSearch();
		}
	}

	blurHandler = (event) => {
		const updatedSearch = updateObject(this.state.searchForm, {
			search: updateObject(this.state.searchForm.search, {
				value: '',
				touched: false
			})
		});

		this.setState({searchForm: updatedSearch});
	};

	render() {
		const { user, environments, search, activeEnvironment, history } = this.props;
		let style, selectConfig, envForm, searchForm;

		if (environments.length > 0) {
			selectConfig = {
				options: environments.map(env => ({ value: env.id, displayValue: env.name }))
			};

			const changeEnvElement = this.state.changeEnvForm.environment;

			envForm = (
				<form>
					<Input
						key={changeEnvElement.id}
						elementType={changeEnvElement.elementType}
						elementConfig={selectConfig || changeEnvElement.elementConfig}
						value={activeEnvironment ? activeEnvironment.id : changeEnvElement.value}
						invalid={!changeEnvElement.valid}
						shouldValidate={changeEnvElement.validation}
						touched={changeEnvElement.touched}
						changed={( event ) => this.selectChangedHandler( event )}
						wrapperCss={changeEnvElement.wrapperCss}
						labelCss={changeEnvElement.labelCss} />
				</form>
			);
		}

		const searchElement = this.state.searchForm.search;
		searchForm = (
			<form>
				<Input
					key={searchElement.id}
					elementType={searchElement.elementType}
					elementConfig={searchElement.elementConfig}
					value={searchElement.value}
					invalid={!searchElement.valid}
					shouldValidate={searchElement.validation}
					touched={searchElement.touched}
					changed={( event ) => this.searchChangedHandler( event )}
					blur={(event) => this.blurHandler(event)}
					wrapperCss={searchElement.wrapperCss}
					sibling={search.loading ? <button type="submit"><i className="fa fa-spinner"></i></button> : searchElement.sibling}
					labelCss={searchElement.labelCss} />
			</form>
		);

		//	Role style
		if (user != null)
			style = user.role === 'ADMIN' ? { color: '#ffffff' } : user.role === 'SUPPORT' ? { color: '#f7ce5e' } : { color: '#367a00' };


		return (
			<header>
				<div className="row">
					<div className="container-fluid header-widget">
						<div className="col-md-2 col-sm-2 ">
							<h1 className="header-widget-logo">
								<i className="switch-responsive-menu fa fa-bars hide-desktop" aria-hidden="true" onClick={(event) => this.props.toggleNavigationMenu(event)}></i>
								<a href="">PLAYERGROUND</a>
								{/* <Link to="/" query={this.props.location.query}> PLAYERGROUND</Link> */}
							</h1>
						</div>
						<div className="col-md-10 col-sm-10 header-widget-login">
							<div className="wrap-user-logado">
								<span style={style} className="name-user">{this.props.user.name}</span>
								<img className="photo-user" src={this.props.user.avatar_url} />
								<a href="/v1/auth/logout">(Logout)</a>
							</div>
							<div className="form-search-header">
								{searchForm}
							</div>

							{user != null && user.role != 'CLIENT' ?
								envForm : ''}
						</div>
					</div>
				</div>
			</header>
		);
	}
};

const mapStateToProps = (state, ownProps) => {
	return {
		user: state.auth,
		environments: state.environments.list.environments,
		activeEnvironment: state.environments.active.environment,
		search: state.search
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchSearch: (query, envId, device) => dispatch(actions.fetchSearch(query, envId, device)),
		clearSearch: () => dispatch(actions.clearSearch()),
		setEnvironmentActive: (environment) => dispatch(actions.setEnvironmentActive(environment))
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderComponent));


// class HeaderComponent extends Component {
// 	constructor(props) {
// 		super(props);
// 	}

// 	componentDidMount() {
// 		//this.fetchBranches();
// 		this.fetchEnvironments();
// 	}

// 	/** Ajax Functions **/
// 	fetchEnvironments = () => {
// 		const { fetchEnvironments, setEnvironmentActive } = this.props;
// 		fetchEnvironments().then(() => {
// 			const { environments } = this.props.environments.list;
// 			const { env_id } = this.props.location.search;
// 			let activeEnv;

// 			if (environments.length == 0) return;

// 			if (typeof env_id != 'undefined') {
// 				activeEnv = environments.filter(env => env.id == env_id)[0];
// 			} else {
// 				activeEnv = environments.filter(env => env.isDefault)[0];
// 			}

// 			//CLIENT should see only staging/prod
// 			const { user } = this.props.auth;
// 			if (user != null && user.role === 'CLIENT') {
// 				activeEnv = environments.filter(env => env.name === 'staging')[0];
// 			}

// 			setEnvironmentActive(activeEnv);
// 			this.fetchCategories({ env: activeEnv.id });
// 		});
// 	}

// 	fetchCategories(obj) {
// 		const { fetchCategories } = this.props;
// 		fetchCategories(obj);
// 	}

// 	fetchBranches(id) {
// 		const { fetchBranches, setBranchActive } = this.props;
// 		fetchBranches().then(() => {
// 			const { branches } = this.props.branches.list;
// 			const { branch_id } = this.props.location.search;
// 			let activeBranch;

// 			if (typeof branch_id != 'undefined') {
// 				activeBranch = branches.filter(branch => branch.id == branch_id)[0];
// 			} else {
// 				activeBranch = branches.filter(branch => branch.isDefault)[0];
// 			}
// 			setBranchActive(activeBranch);
// 		});
// 	}

// 	/** Elements binds **/
// 	changeEnvironment(e) {
// 		const { environments, setEnvironmentActive } = this.props;
// 		const envId = e.currentTarget.value;
// 		const environment = environments.list.environments.find((environment) => environment.id == envId);
// 		setEnvironmentActive(environment);
// 		this.fetchCategories({ env: environment.id });

// 		let query;
// 		if (!environment.isDefault) {
// 			query = `?env_id=${environment.id}`;
// 		} else {
// 			query = '';
// 		}

// 		this.props.history.push(`/${query}`);
// 	}

// 	changeBranch(e) {
// 		const { fetchBranches, setBranchActive } = this.props;
// 		const { branches } = this.props.branches.list;
// 		const branchId = e.currentTarget.value;
// 		const branch = branches.find((branch) => branch.id == branchId);

// 		setBranchActive(branch);

// 		let query;
// 		if (!branch.isDefault) {
// 			query = { ...this.props.location.query, branch_id: branch.id }
// 		} else {
// 			delete this.props.location.query.branch_id;
// 			query = { ...this.props.location.query }
// 		}

// 		this.context.router.push({
// 			...this.props.location,
// 			query
// 		});
// 	}

// 	makeSearch = (e) => {
// 		this.props.makeSearch(e);
// 	}

// 	onSearchFocus = (e) => {
// 		if (this.props.search.query == '')
// 			e.currentTarget.value = '';
// 	}

// 	render() {
// 		const { branches } = this.props.branches.list;
// 		const { environments } = this.props.environments.list;
// 		let style;
// 		const { user } = this.props.auth;

// 		if (user != null)
// 			style = user.role === 'ADMIN' ? { color: '#ffffff' } : user.role === 'SUPPORT' ? { color: '#f7ce5e' } : { color: '#367a00' };

// 		let envSelected;
// 		const envOptions = environments.map((environment) => {
// 			if (this.props.environments.active.environment != null && this.props.environments.active.environment.id == environment.id) {
// 				envSelected = environment.id;
// 			} else if (environment.isDefault == true) {
// 				envSelected = environment.id;
// 			}
// 			return <option key={environment.id} value={environment.id} label={environment.name}>{environment.name}</option>;
// 		});

// 		let branchSelected;
// 		const branchOptions = branches.map((branch) => {
// 			if (this.props.branches.active.branch != null && this.props.branches.active.branch.id == branch.id) {
// 				branchSelected = branch.id;
// 			} else if (branch.isDefault == true) {
// 				branchSelected = branch.id;
// 			}
// 			return <option key={branch.id} value={branch.id} label={branch.name}>{branch.name}</option>;
// 		});

// 		return (
// 			<header className="">
// 				<div className="row">
// 					<div className="container-fluid header-widget">
// 						<div onClick={this.props.toggleNavigationMenu} className="col-md-2 col-sm-2 ">
// 							<h1 className="header-widget-logo">
// 								<i className="switch-responsive-menu fa fa-bars hide-desktop" aria-hidden="true"></i>
// 								<Link to="/" query={this.props.location.query}> PLAYERGROUND
// 							</Link>
// 							</h1>
// 						</div>
// 						<div className="col-md-10 col-sm-10 header-widget-login">
// 							{user != null ? <div className="wrap-user-logado">
// 								<span style={style} className="name-user">{user.name}</span>
// 								<img className="photo-user" src={user.avatar_url} />
// 								<a href="/auth/logout">(Logout)</a>
// 							</div> :
// 								<a href="/auth/github" className="icon-mobile-git"><i className="fa fa-github-alt" aria-hidden="true"></i></a>
// 							}
// 							<div className="form-search-header">
// 								<form>
// 									<input data-type="desktop" type="text" placeholder="Buscar" onChange={this.makeSearch} onFocus={this.onSearchFocus} />
// 									<button type="submit"><i className="fa fa-search"></i></button>
// 								</form>
// 							</div>

// 							{branchOptions.length > 0 ?
// 								<div className="select-branch hide-mobile hide">
// 									<select onChange={this.changeBranch.bind(this)} value={branchSelected} name="Branches">
// 										{branchOptions}
// 									</select>
// 								</div> : ''}

// 							{envOptions.length > 0 && user != null && user.role != 'CLIENT' ?
// 								<div className="select-branch hide-mobile">
// 									<select onChange={this.changeEnvironment.bind(this)} value={envSelected} name="Environments">
// 										{envOptions}
// 									</select>
// 								</div> : ''}
// 						</div>
// 					</div>
// 				</div>
// 			</header>
// 		);
// 	}
// }

// HeaderComponent.displayName = 'HeaderHeaderComponent';

// HeaderComponent.contextTypes = {
// 	router: PropTypes.object.isRequired
// }

// export default HeaderComponent;
