import React from 'react';
import {Field, reduxForm} from 'redux-form';

export const FieldTemplate = ({ input, label, element, type, tip, placeholder, meta: { touched, error } }) => (
  <div className="group-form">
    <span htmlFor={input.name}>{label}</span>
    <Field {...input} component={element} placeholder={placeholder} type={type}/>
    <span className="obs">{tip}</span>
    {touched && error && <span className="obs">{error}</span>}
  </div>  
)

export const CheckFieldsTemplate = ({ input, label, element, type, tip, meta: { touched, error }}) => (
	<div className="group-form group-check">
		<div>
		  <Field {...input} component="input" name={input.name} type="checkbox" id="test6"/>
		  <label htmlFor="test6">{label}</label>
		</div>
		<span className="obs">{tip}</span>
	</div>
)

export const SelectFieldsTemplate = ({ input, label, element, type, options, tip, title, meta: { touched, error }}) => (
	<div className="group-form">
		<span>{label}</span>
		<Field {...input} component="select" name={input.name} type="select" value="2">
			<option>{title}</option>
			{options.map(option => <option key={option.id} value={option.id}>{option.name}</option>)}
		</Field>
		<span className="obs">{tip}</span>
		{touched && error && <span className="obs">{error}</span>}
	</div>
)

export const RadioColorFieldTemplate = ({input, label, element, type, options, tip, title, meta: { touched: error }}) => (
	<div className="group-form">
		<span>{label}</span>
		<div className="wrap-color"><ul>{generateColors(0, 8, input)}</ul><ul>{generateColors(9, 18, input)}</ul></div>
	</div>	
); 

export const LoginInputTemplate = ({input, label, element, type, options, tip, placeholder, title, meta: {touched, error}}) => (
	<div className="gmd-group-input">
		<input type={type} {...input} placeholder={placeholder}/>
		{/* {touched && error && <span>{error}</span>} */}
		{!error && <span>{label}</span>}
	</div>
);

//Pensar em melhor maneira
let cValue;
const generateColors = (start, end, input) => {
	const colors = [
		'#d50000',
		'#c51162',
		'#aa00ff',
		'#6200ea',
		'#304ffe',
		'#2962ff',
		'#0091ea',
		'#00b8d4',
		'#00c853',
		'#388E3C',
		'#64dd17',
		'#aeea00',
		'#ffd600',
		'#ffab00',
		'#ff6d00',
		'#dd2c00',
		'#3e2723',
		'#546e7a'							
	];

	const onBlur = (evt) => {
		cValue = evt.currentTarget.value;
	}
	
	return colors.map((color, index) => {
		if(index >= start && index <= end) return <li key={index}><input {...input} type="radio" name="color" value={color} onBlur={onBlur} checked={color === cValue}/><span className="color-cooser-color" style={{backgroundColor: color}}></span></li>
	});
};
