'use strict';

import React, { Component } from 'react';

class SelectComponent extends Component {
  render() {
    let selected;
  	const options = this.props.data.map((obj) => {
      if(obj[this.props.selectedField] == true) {
        selected = obj.id;
      }
      return <option key={obj.id} value={obj.id} label={obj[this.props.text]}>{this.props.text}</option>;
    });

    return (
      <select onChange={this.props.onSelect} defaultValue={selected} name={this.props.label}>
      	{options}
      </select>
    );
  }
}

SelectComponent.displayName = 'InputsSelectComponent';

// Uncomment properties you need
// SelectComponent.propTypes = {};
SelectComponent.defaultProps = {
	label: 'type',
  text: 'label',
  selectedField: 'isDefault',
	data: [{
		id: 1,
		label: 'Name 1',
    isDefault: true
	}]
};

export default SelectComponent;
