'use strict';

import React from 'react';

class SubmitActionsComponent extends React.Component {
  render() {
    return (
      <div className="group-form group-btn">
	  	<button disabled={this.props.disabled} onClick={this.props.reset} className="btn btn-primary btn-reset" type="reset">Cancelar</button>
	  	<button disabled={this.props.disabled} className="btn btn-primary" type="submit">Enviar</button>
      </div>
    );
  }
}

SubmitActionsComponent.displayName = 'InputsSubmitActionsComponent';

// Uncomment properties you need
// SubmitActionsComponent.propTypes = {};
// SubmitActionsComponent.defaultProps = {};

export default SubmitActionsComponent;
