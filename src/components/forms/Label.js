'use strict';

import React, {Component, PropTypes} from 'react';
import {Field, reduxForm} from 'redux-form';
import SubmitActions from '../inputs/SubmitActionsComponent';
import {FieldTemplate, SelectFieldsTemplate, RadioColorFieldTemplate} from '../inputs/FieldsTemplate';

import Messages from '../helpers/MessagesComponent';

//Validation
const validate = values => {
  const errors = {}
  if (!values.name) {
    errors.name = 'O nome é requerido!'
  } else if (values.name.length < 3 || values.name.length > 32) {
    errors.name = 'O nome do label deve ter entre 3 a 32 caracteres'
  }
  return errors;
}

class LabelComponent extends React.Component {

	constructor(props) {
		super(props);

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleSubmitted = this.handleSubmitted.bind(this);
	}

	componentDidMount() {
		this.props.initialize({color: '#ffffff'});
	}

	handleSubmit(data) {
		return this.props.createLabel(data).then(this.handleSubmitted);
	}

	handleSubmitted() {
		const { error } = this.props;
		this.props.reset();
	}

	render() {
		const { error, valid, handleSubmit, pristine, reset, submitting, submitSucceeded, submitFailed } = this.props;

		const message = { 
				submitSucceeded, 
				submitFailed, 
				type: (submitSucceeded ) ? 'success' : 'danger',
				text: (submitSucceeded ) ? 'Label criado com sucesso!!!' : 
											`Oops! Um erro ao criar o label!!!${(error ? '\n -> ' + error : '')}`
			};
  	
	    return (
	      <div className="container-fluid page-content">
		    <div className="col-md-8">
		      <div className="wrap-formulario">
		        <h1>Cadastro - Labels</h1>
		        <Messages messageInfo={message} />
		        <form onSubmit={handleSubmit(this.handleSubmit)}>
		          <Field name="name" label="Nome" component={FieldTemplate} type="text" placeholder="Nome" element="input"/>
		          <Field name="description" label="Descrição" component={FieldTemplate} type="text" placeholder="Descrição do label" element="textarea"/>
							<Field name="url" label="URL ( opcional )" component={FieldTemplate} type="text" placeholder="URL do label ( Github ou Zendesk )" element="input"/>
              <Field name="color" label="Cor" component={RadioColorFieldTemplate} type="radio" element="radio"/>
		          <SubmitActions disabled={pristine || submitting} reset={reset}/>
		        </form>
		      </div>
		    </div>      	
	      </div>
	    );
	  }
	}

LabelComponent.displayName = 'FormsLabelComponent';

// Uncomment properties you need
// LabelComponent.propTypes = {};
// LabelComponent.defaultProps = {};


LabelComponent = reduxForm({
	form: 'label',
	fields: ['name', 'description', 'url', 'color'],
	validate
})(LabelComponent);

export default LabelComponent;