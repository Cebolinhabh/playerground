'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as actions from '../../store/actions/index';
import { urlToMediaObj, objToQuery, updateObject, checkValidity, resetForm } from '../../helpers/utility';
// import SubmitActions from '../inputs/SubmitActionsComponent';
// import { FieldTemplate, SelectFieldsTemplate } from '../inputs/FieldsTemplate';

import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import Messages from '../UI/Messages/Messages';
import Spinner from '../UI/Spinner/Spinner';
import FormActions from '../UI/FormActions/FormActions';

class Pcase extends Component {

	state = {
		pcaseForm: {
			category_id: {
				elementType: 'select',
				elementConfig: {
					options: []
				},
				value: '',
				validation: {},
				valid: true,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: '',
				label: 'Categoria'
			},
			title: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Insira um título de fácil procura'
				},
				value: '',
				validation: {
					required: true,
					minLength: 3
				},
				valid: false,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: '',
				label: 'Título'
			},
			width: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Insira a largura apenas com números'
				},
				value: '',
				validation: {
					required: true,
					minLength: 3,
					isNumeric: true
				},
				valid: false,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: '',
				label: 'Largura'
			},
			height: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Insira a altura apenas com números'
				},
				value: '',
				validation: {
					required: true,
					minLength: 3,
					isNumeric: true
				},
				valid: false,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: '',
				label: 'Altura'
			},
			description: {
				elementType: 'textarea',
				elementConfig: {
					type: 'text',
					placeholder: 'Descrição do pcase'
				},
				value: '',
				validation: {
					required: true,
					minLength: 3
				},
				valid: false,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: '',
				label: 'Descrição'
			},
			url: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'URL do iframe'
				},
				value: '',
				validation: {
					required: true,
					minLength: 32
				},
				valid: false,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: '',
				label: 'URL'
			},
			api: {
				elementType: 'checkbox',
				elementConfig: {
					placeholder: 'URL do iframe'
				},
				value: false,
				validation: {},
				valid: true,
				touched: false,
				wrapperCss: 'group-form group-check',
				labelCss: '',
				label: 'API'
			},
			api_code: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Insira uma interação com a nossa API'
				},
				value: '',
				validation: {},
				valid: true,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: '',
				label: 'Código API ( opcional )'
			}
		},
		formIsValid: false,
		submitted: {
			error: false,
			success: false
		}
	}

	componentWillReceiveProps(nextProps) {
		const { activeCategory, activeEnvironment, activePcase, match } = nextProps;

		if (!activeCategory || !activeEnvironment) return;
		if (activeCategory && activeCategory.slug !== match.params.category_slug) return;
		if (activePcase.error && this.props.match.params.pcase_id === match.params.pcase_id) return;

		if (!activePcase.pcase && !activePcase.loading) return this.fetchPcase(match.params.pcase_id, activeEnvironment.id, activeCategory.id);
		if (activePcase.pcase && activePcase.pcase.id !== +match.params.pcase_id) return this.fetchPcase(match.params.pcase_id, activeEnvironment.id, activeCategory.id);
	}

	componentDidMount() {
		const { activePcase, categories, match } = this.props;

		if (activePcase.pcase && match.params.pcase_id) this.handleInitializeData();
		if (categories.length > 0) this.populateCategory();
	}

	fetchPcase(id, envId, catId) {
		const { fetchPcase, user, activeEnvironment, environments } = this.props;

		fetchPcase(id, envId, catId).then(() => {
			this.handleInitializeData();
		}).catch(error => {console.info(error)});
	}

	handleInitializeData() {
		const { pcase } = this.props.activePcase;
		const codeParsed = JSON.parse(pcase.code);
		let data = {...pcase, ...codeParsed};

		if (data.live) {
			data.url = `${data.ph}/live/${data.live}${objToQuery(data.playerParams)}`;
		} else {
			data.url = `${data.ph}/${data.m}${objToQuery(data.playerParams)}`;
		}

		for (let key in data) {
			if (this.state.pcaseForm[key]) {
				this.state.pcaseForm[key].value = data[key];
				this.state.pcaseForm[key].valid = true;
				this.state.pcaseForm[key].touched = true;
			}
			if (key === 'api_code') {
				this.state.pcaseForm[key].value = (data[key]) ? data[key] : '';
			}
		}

		this.setState({
			pcaseForm: updateObject(this.state.pcaseForm),
			formIsValid: true
		});
	}

	populateCategory() {

	}

	fetchPCase(id, envId, catId) {
		this.loading = true;
		this.props.fetchPCase(id, envId, catId).then(() => {
			this.handleInitializeData(this.props.activePcase.pcase);
		});
	}

	updatePCase(pcase) {
		return this.props.updatePCase(pcase);
	}

	inputChangedHandler = (event, controlName) => {
		let formIsValid = true;
		const updatedPcaseForm = updateObject(this.state.pcaseForm, {
			[controlName]: updateObject(this.state.pcaseForm[controlName], {
				value: event.target.type === 'checkbox' ? event.target.checked : event.target.value,
				valid: checkValidity(event.target.value, this.state.pcaseForm[controlName].validation),
				touched: true
			})
		});

        for (let inputIdentifier in updatedPcaseForm) {
            formIsValid = updatedPcaseForm[inputIdentifier].valid && formIsValid;
		}

		this.setState({
			pcaseForm: updatedPcaseForm,
			formIsValid: formIsValid
		});
	}

	handleSubmit = (event) => {
		const { user, activeEnvironment, activePcase, history, location} = this.props;
		const action = (activePcase.pcase) ? 'updatePcase' : 'createPcase';
		let formData = {}, code;

		event.preventDefault();

        for (let formElementIdentifier in this.state.pcaseForm) {
            formData[formElementIdentifier] = this.state.pcaseForm[formElementIdentifier].value;
		}

		code = urlToMediaObj(formData.url);
		code.width = formData.width;
		code.height = formData.height;

		formData = updateObject(formData, {
			user_id: user.id,
			environment_id: activeEnvironment.id,
			code: JSON.stringify(code, (key, value) => {
				if (value === null) return undefined;
				return value;
			}),
			api_code: formData.api_code.length > 0 ? formData.api_code.trim() : null
		})

		if (activePcase.pcase) formData = updateObject(activePcase.pcase, formData);

		//	Deletando objetos desnecessários
		delete formData.url;
		delete formData.width;
		delete formData.height;

		this.setState({
			submitted: {
				error: false,
				success: true
			}
		});

		this.props[action](formData).then(() => {
			this.setState({
				submitted: {
					error: false,
					success: true
				}
			});
			action === 'createPcase' && this.reset();
			action === 'updatePcase' && history.push(location.pathname.replace('/edit', ''));
		}).catch(error => {
			this.setState({
				submitted: {
					error: true,
					success: false
				}
			});
			action === 'createPcase' && this.reset();
		});
	}

	reset = (event) => {
        for (let formElementIdentifier in this.state.pcaseForm) {
			this.state.pcaseForm[formElementIdentifier].value =
				(formElementIdentifier === 'category_id') ? this.props.categories[0].id :
				(formElementIdentifier === 'api') ? false : '';
			this.state.pcaseForm[formElementIdentifier].valid = false;
			this.state.pcaseForm[formElementIdentifier].touched = false;
		}

		this.setState({
			pcaseForm: updateObject(this.state.pcaseForm),
			formIsValid: false
		});
	}

	clearMessage = () => {
		this.setState({
			submitted: {
				error: false,
				success: false
			}
		});
	}

	render() {
		const { user, categories } = this.props;
		let formElementsArray = [], categoriesList, form, categoryConfig = {options: []}, messages;

		for (let key in this.state.pcaseForm) {
			formElementsArray.push({
				id: key,
				config: this.state.pcaseForm[key]
			});
		}

		if (categories.length > 0) {
			categoryConfig.options = categories.map(cat => ({ value: cat.id, displayValue: cat.name }));
			if (this.state.pcaseForm.category_id.value.length === 0)
				this.state.pcaseForm.category_id.value = categories[0].id;
		}

		form = formElementsArray.map(formElement => {
			const elementConfig = formElement.id === 'category_id' ? categoryConfig :
				formElement.config.elementConfig;

			return <Input
						key={formElement.id}
						elementType={formElement.config.elementType}
						elementConfig={elementConfig}
						value={formElement.config.value}
						invalid={!formElement.config.valid}
						shouldValidate={formElement.config.validation}
						touched={formElement.config.touched}
						changed={( event ) => this.inputChangedHandler( event, formElement.id )}
						wrapperCss={formElement.config.wrapperCss}
						label={formElement.config.label}
						labelCss={formElement.config.labelCss} />

		});

		categoriesList = categories.filter(cat => {
			if (user.role === 'ADMIN') return true;
			if (user.role != 'ADMIN') return cat.type === user.role;
		});

		if (this.state.submitted.error) {
			messages = <Messages close={this.clearMessage} className="alert alert-danger" text="Erro ao criar o pcase. Favor tente novamente." />
		}

		if (this.state.submitted.success) {
			messages = <Messages close={this.clearMessage} className="alert alert-success" text="Pcase criado com sucesso!" />
		}

		return (
			<div className="col-md-8">
				<div className="wrap-formulario">
					<h1>Cadastro - Casos de teste</h1>
					{messages}
					<form onSubmit={this.handleSubmit}>
						{form}
						<FormActions disabled={!this.state.formIsValid} reset={this.reset} />
					</form>
				</div>
			</div>
		);
	}
}

//	Redux

const mapStateToProps = (state, ownProps) => {
	return {
		user: state.auth,
		activeEnvironment: state.environments.active.environment,
		activeCategory: state.categories.active.category,
		activePcase: state.pcases.active,
		environments: state.environments.list.environments,
		categories: state.categories.list.categories
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		createPcase: (data) => dispatch(actions.createPCase(data)),
		fetchPcase: (id, envId, catId) => dispatch(actions.fetchPcase(id, envId, catId)),
		updatePcase: (data) => dispatch(actions.updatePCase(data))
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Pcase));


