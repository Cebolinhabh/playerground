'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';
import { updateObject, checkValidity, resetForm } from '../../helpers/utility';
// import SubmitActions from '../inputs/SubmitActionsComponent';
// import { FieldTemplate, SelectFieldsTemplate } from '../inputs/FieldsTemplate';

import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import Messages from '../UI/Messages/Messages';
import Spinner from '../UI/Spinner/Spinner';
import FormActions from '../UI/FormActions/FormActions';

class Category extends Component {

	state = {
		categoryForm: {
			type: {
				elementType: 'select',
				elementConfig: {
					options: [
						{ value: 'ADMIN', displayValue: 'ADMIN' },
						{ value: 'SUPPORT', displayValue: 'SUPPORT' },
						{ value: 'CLIENT', displayValue: 'CLIENT' }
					]
				},
				value: 'ADMIN',
				validation: {},
				valid: true,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: ''
			},
			name: {
				elementType: 'input',
				elementConfig: {
					type: 'text',
					placeholder: 'Nome'
				},
				value: '',
				validation: {
					required: true,
					minLength: 3
				},
				valid: false,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: ''
			},
			description: {
				elementType: 'textarea',
				elementConfig: {
					type: 'text',
					placeholder: 'Descrição'
				},
				value: '',
				validation: {
					required: true,
					minLength: 3
				},
				valid: false,
				touched: false,
				wrapperCss: 'group-form',
				labelCss: ''
			}
		},
		formIsValid: false,
		submitted: {
			error: false,
			success: false
		}
	}

	inputChangedHandler = (event, controlName) => {
		let formIsValid = true;
		const updatedCategoryForm = updateObject(this.state.categoryForm, {
			[controlName]: updateObject(this.state.categoryForm[controlName], {
				value: event.target.value,
				valid: checkValidity(event.target.value, this.state.categoryForm[controlName].validation),
				touched: true
			})
		});

        for (let inputIdentifier in updatedCategoryForm) {
            formIsValid = updatedCategoryForm[inputIdentifier].valid && formIsValid;
        }
		this.setState({
			categoryForm: updatedCategoryForm,
			formIsValid: formIsValid
		});
	}

	handleSubmit = (event) => {
		const formData = {};

		event.preventDefault();

        for (let formElementIdentifier in this.state.categoryForm) {
            formData[formElementIdentifier] = this.state.categoryForm[formElementIdentifier].value;
		}

		this.props.createCategory(formData).then(() => {
			this.setState({
				submitted: {
					error: false,
					success: true
				}
			});
			this.reset();
		}).catch(error => {
			this.setState({
				submitted: {
					error: true,
					success: false
				}
			});
			this.reset();
		});
	}

	reset = () => {
        for (let formElementIdentifier in this.state.categoryForm) {
			this.state.categoryForm[formElementIdentifier].value =
				(formElementIdentifier === 'type') ? 'ADMIN' : '';
			this.state.categoryForm[formElementIdentifier].valid =
				(formElementIdentifier === 'type') ? true : false;
			this.state.categoryForm[formElementIdentifier].touched = false;
		}

		this.setState({
			categoryForm: updateObject(this.state.categoryForm),
			formIsValid: false
		});
	}

	clearMessage = () => {
		this.setState({
			submitted: {
				error: false,
				success: false
			}
		});
	}

	render() {
		const formElementsArray = [];
		let messages;

		for (let key in this.state.categoryForm) {
			formElementsArray.push({
				id: key,
				config: this.state.categoryForm[key]
			});
		}

		let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
				changed={( event ) => this.inputChangedHandler( event, formElement.id )}
				wrapperCss={formElement.config.wrapperCss}
				labelCss={formElement.config.labelCss} />
		));

		if (this.state.submitted.error) {
			messages = <Messages close={this.clearMessage} className="alert alert-danger" text="Erro ao criar a categoria. Favor tente novamente." />
		}

		if (this.state.submitted.success) {
			messages = <Messages close={this.clearMessage} className="alert alert-success" text="Categoria criada com sucesso!" />
		}

		return (
			<div className="col-md-8">
				<div className="wrap-formulario">
					<h1>Cadastro - Categorias</h1>
					{messages}
					<form onSubmit={this.handleSubmit}>
						{form}
						<FormActions disabled={!this.state.formIsValid} reset={this.reset} />
					</form>
				</div>
			</div>
		);
	}
};

//	Redux

const mapStateToProps = (state, ownProps) => {
	return {
		prop: state.prop
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		createCategory: (data) => dispatch(actions.createCategory(data))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Category);
