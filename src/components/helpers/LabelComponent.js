'use strict';

import React from 'react';

class LabelComponent extends React.Component {
	constructor(props) {
		super(props);

        this.fetchLabels = this.fetchLabels.bind(this);
        
	}

    componentDidMount() {
        this.fetchLabels();
    }

    fetchLabels() {
        const { fetchLabels } = this.props;
        
        fetchLabels();
    }

    addLabelToPcase(label_id, evt) {
        const { pcase } = this.props.pcases.active;
        const { addLabelToPcase } = this.props;
        const target = evt.currentTarget;

        addLabelToPcase(pcase.id, label_id).then(() => {
            target.classList.add('active');
        });
        evt.stopPropagation();
    }

    removeLabelFromPcase(label_id, evt) {
        const { pcase } = this.props.pcases.active;
        const { removeLabelFromPcase } = this.props;
        const target = evt.currentTarget;
        
        removeLabelFromPcase(pcase.id, label_id).then(() => {
            target.parentNode.classList.remove('active');    
        });
        evt.stopPropagation();        
    }

    shouldComponentUpdate(nextProps, nextState) {
        const { pcase, loading, error } = nextProps.pcases.active;
        if(pcase != null) return true;
        return false;
    }

	render() {
        const { pcase } = this.props.pcases.active;
        const { labels } = this.props.labels.list;
        
        if(pcase == null || labels.length === 0) return <div/>;
        
        const listLabels = labels.map((label) => {
            const pcaseHasLabel = pcase.labels.reduce((flag, clabel) => {
                if(clabel.id === label.id) flag = true;
                return flag;
            }, false);
            const active = pcaseHasLabel ? 'active' : '';
            return   <li onClick={this.addLabelToPcase.bind(this, label.id)} key={label.id} className={active}>
                        <i className="fa fa-check" aria-hidden="true"></i>
                        <div className="color" style={{backgroundColor: label.color}}></div>
                        <span className="title-label" title={label.description}>{label.name}</span>
                        <i onClick={this.removeLabelFromPcase.bind(this, label.id)} className="fa fa-times" aria-hidden="true"></i>
                    </li>
        });

		return(
            <div className="page-content-information hide-mobile">
                <span className="title-branch">Labels:</span>
                <ul className="wrap-labels-select">
                    { listLabels }
                </ul>
            </div>
        );
  	}
}

LabelComponent.displayName = 'HelpersLabelComponent';

// Uncomment properties you need
// LabelComponent.propTypes = {};
// LabelComponent.defaultProps = {};

export default LabelComponent;