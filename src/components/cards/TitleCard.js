import React from 'react';

const titleCard = (props) => {
	let mainClassName = `card-dark ${props.mainClassName}`;
	let infoClassName = `card-information ${props.infoClassName ? props.infoClassName : ''}`;

	return (
		<div className={mainClassName}>
			<div className={infoClassName}>
				{props.children}
			</div>
		</div>
	)
};

export default titleCard;