'use strict';

import React, { Component } from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';
import { getClassByStatus, updateObject } from '../../helpers/utility';

import Input from '../UI/Input/Input';

class SideNavigation extends Component {

	state = {
		fetchedFav: false,
		searchMobile: false,
		searchForm: {
			id: 'search',
			search: {
				elementType: 'input',
				elementConfig: {
					type: 'search',
					placeholder: 'Buscar'
				},
				sibling: <i className="fa fa-search" aria-hidden="true"></i>,
				value: '',
				validation: {},
				valid: true,
				touched: false,
				wrapperCss: 'wrap-busca-mobile show-mobile',

			}
		}
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.activeEnvironment !== nextProps.activeEnvironment) {
			this.props.fetchFavPCases(nextProps.activeEnvironment);
		}
	}

	searchChangedHandler = (event) => {
		const value = event.target.value;
		const updatedSearch = updateObject(this.state.searchForm, {
			search: updateObject(this.state.searchForm.search, {
				value,
				touched: true
			})
		});
		this.setState({ searchForm: updatedSearch });

		if (value.length > 3) {
			this.props.fetchSearch(value, this.props.activeEnvironment.id, 'MOBILE');
		} else {
			this.props.clearSearch();
		}
	}

	render() {
		const { user, categories, activeEnvironment, favPcases, search, clearSearch, location, activeCategory } = this.props;
		let categoriesList, favPcasesRender, searchElement, searchForm, pcasesList, searchList;

		categoriesList = categories.map((cat) => {
			const style = cat.type === 'ADMIN' ? { color: '#ffffff' } : cat.type === 'SUPPORT' ? { color: '#f7ce5e' } : { color: '#367a00' };
			let url, className;
			
			className = (activeCategory && (activeCategory.id === cat.id)) ? 'active' : '';

			if (cat.pcases && cat.pcases.length > 0) url = `/${cat.slug}`;
			return <li key={cat.id} className={className}>
				{url ? <Link to={{
					pathname: url,
					search: this.props.location.search
				}}>{cat.name}</Link> : <a className="item-disabled">{cat.name}</a>}
			</li>;
		});

		favPcasesRender = favPcases.map((pcase) => {
			const normalizeCatName = pcase.category.slug;
			const url = `/${normalizeCatName}/pcases/${pcase.id}`;
			const className = (user != null && pcase.pcase_user_infos.length > 0) ? getClassByStatus(pcase.pcase_user_infos[0].status) : '';
			return <li key={pcase.id} className={className}>
				<Link to={{ pathname: url }}>{pcase.title}</Link>
			</li>;
		});

		searchElement = this.state.searchForm.search;
		searchForm = (
			<form>
				<Input
					key={searchElement.id}
					elementType={searchElement.elementType}
					elementConfig={searchElement.elementConfig}
					value={searchElement.value}
					invalid={!searchElement.valid}
					shouldValidate={searchElement.validation}
					touched={searchElement.touched}
					changed={(event) => this.searchChangedHandler(event)}
					wrapperCss={searchElement.wrapperCss}
					sibling={search.loading ? <button type="submit"><i className="fa fa-spinner"></i></button> : searchElement.sibling}
					labelCss={searchElement.labelCss} />
			</form>
		);

		if (search.device === 'MOBILE' && search.active) {
			pcasesList = search.pcases.map((pcase) => {
				const url = '/' + pcase.category.slug + '/pcases/' + pcase.id;

				return <li key={pcase.id} onClick={clearSearch}>
					<Link to={{
							pathname: url,
							search: location.search
						}}>
						<span className="category">{pcase.category.name}</span>
						<p>{pcase.title}</p>
					</Link>
				</li>;
			});

			searchList = <div className="fade-menu-mobile">
							<ul>
								{pcasesList}
							</ul>
						</div>;
		}

		return (
			<div className={this.props.slideMenu}>
				<span className="btn-voltar-menu show-mobile mobile-bg-black switch-responsive-menu">
					<span className="title-menu ">PlayerGround</span>
					<i className="fa fa-times" aria-hidden="true" onClick={(event) => this.props.toggleNavigationMenu(event)}></i>
				</span>
				{searchList}
				{searchForm}
				<div className="item-menu">
					<NavLink onClick={this.props.toggleNavigationMenu} to="/">
						<span className="menu-expand">
							<i className="fa fa-home"></i>
							<span className="description">Home</span>
						</span>
					</NavLink>
				</div>
				{user != null && user.role != 'CLIENT' ? <div className="item-menu">
					<span className="menu-expand">
						<i className="fa fa-pencil-square-o"></i>
						<span className="description">Cadastro</span>
					</span>
					<ul className="dropdown">
						<li className="title">
							<span>Cadastro</span>
						</li>
						<li className="selected">
							<NavLink to="/new/pcase">Casos de teste</NavLink>
						</li>
						{user.role === 'ADMIN' ? <li>
							<NavLink to="/new/category">Categorias</NavLink>
						</li> : ''}
					</ul>
				</div> : ''}

				{favPcases.length > 0 && <div className="item-menu">
					<span className="menu-expand"><i className="fa fa-star"></i><span className="description">Favoritos</span></span>
					<ul className="dropdown">
						<li className="title"><span>Favoritos</span></li>
						{favPcasesRender}
					</ul>
				</div>}

				<div className="item-menu" onClick={this.props.toggleNavigationMenu}>
					<Link to="/">
						<span className="menu-expand">
							<i className="fa fa-cubes" ></i>
							<span className="description">Casos</span>
						</span>
					</Link>
					<ul className="dropdown">
						<li className="title">
							<i className="fa fa-angle-left" aria-hidden="true"></i>
							<span>Categorias</span>
						</li>
						{categoriesList}
					</ul>
				</div>
				<div className="item-menu hide">
					<span className="menu-expand"><i className="fa fa-bolt" ></i></span>
				</div>

			</div>
		)
	}
}

//	Redux
const mapStateToProps = (state, ownProps) => {
	return {
		user: state.auth,
		categories: state.categories.list.categories,
		activeEnvironment: state.environments.active.environment,
		favPcases: state.pcases.favorite.pcases,
		search: state.search,
		activeCategory: state.categories.active.category
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchFavPCases: (environment) => dispatch(actions.fetchFavPcases({ env: environment.id })),
		clearSearch: () => dispatch(actions.clearSearch()),
		fetchSearch: (query, envId, device) => dispatch(actions.fetchSearch(query, envId, device))
	};
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SideNavigation));
