import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';

class SearchResults extends Component {

	render() {
		const { search, location, clearSearch } = this.props;
		const fadeSearchClass = 'fade-search ' + ((search.active) ? '' : 'hidden');
		const wrapCampoClass = 'wrap-campo-busca ' + ((search.active) ? '' : 'hidden');
		let pcasesList;

		if (search.device !== 'DESKTOP') return <div/>

		pcasesList = search.pcases.map((pcase) => {
			const url = '/' + pcase.category.slug + '/pcases/' + pcase.id;
			const path = location.query;

			return <li key={pcase.id} onClick={clearSearch}>
				<Link to={{
					pathname: url,
					search: location.search
				}} query={path}>
					<span className="category">{pcase.category.name}</span>
					<p>{pcase.title}</p>
				</Link>
			</li>;
		});
		
		return (
			<div>
				<div className={fadeSearchClass}></div>
				<div className={wrapCampoClass}>
					<div className="container">
						<div className="col-md-10 col-md-offset-1">
							<div className="header-busca">
								<h6>Resultado da busca</h6>
								<span>{pcasesList.length} ocorrências localizadas para : <b>{search.query}</b> </span>
							</div>
							<div className="container-busca">
								<ul>
									{pcasesList}
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
};

//	Redux

const mapStateToProps = (state, ownProps) => {
	return {
		search: state.search
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		clearSearch: () => dispatch(actions.clearSearch())
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchResults));
