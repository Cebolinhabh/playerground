import React, { Component } from 'react';
import { connect } from 'react-redux';
import Terminal from 'terminal-in-react';

import * as actions from '../../store/actions';

import { objToQuery, createAndAppendScripts } from '../../helpers/utility';

class Player extends Component {
	state = {
		cssControlbar: 'wrap-controls hidden',
		dimmedLight: false,
		code: null,
		envURL: null,
		html5: null
	}

	play = () => {
		this.player.play();
		console.info('Método chamado: player.play()');
	}

	pause = () => {
		this.player.pause();
		console.info('Método chamado: player.pause()');
	}

	seek = () => {
		this.player.seek(20);
		console.info('Método chamado: player.seek()');
	}

	status = () => {
		this.player.getStatus((m) => console.table(m));
		console.info('Método chamado: player.getStatus(function(status){console.info(status);})');
	}

	dimLights = () => {
		this.setState(prevState => {
            return { dimmedLight: !prevState.dimmedLight };
        });
		this.player.dimLights(this.state.dimmedLight);
		console.info('Método chamado: player.dimLights(${this.state.dimmedLight})');
	}

	runApiCode = () => {
		eval(this.api_code);
	}

	componentDidMount() {
		const { pcase } = this.props.activePcase;
		let code;
		if (!pcase) return;

		code = this.treatAndParseCode(pcase.code);
		
		this.setState({
			html5: code.playerParams.html5,
			envURL: pcase.url,
			code
		});

		// player API
		if (pcase.api) {
			//	Saindo do state para não dar refresh
			this.currentApiEnv = pcase.environment.staticURL;
			createAndAppendScripts(pcase.environment.staticURL,
				{"samba-player-api":"player"}, this.playerApiLoadHandler, true);
			return;
		}
	}

	componentDidUpdate() {
		const { pcase } = this.props.activePcase;
		
		if (pcase && !pcase.api) this.props.onSourceReady(this.refs.playerContainer.innerHTML);
		if (this.currentApiEnv !== pcase.environment.staticURL) {
			if (pcase.api) {
				createAndAppendScripts(pcase.environment.staticURL,
				{"samba-player-api":"player"}, this.playerApiLoadHandler, true);
				this.currentApiEnv = pcase.environment.staticURL;
			}
		}
	}

	render() {
		const { pcase } = this.props.activePcase;
		const { code } = this.state;
		let url, terminal;

		if (!code || !pcase) return <div/>;

		if (code.live) {
			url = `${pcase.environment.url}${code.ph}/live/${code.live}${objToQuery(code.playerParams)}`;
		} else if(code.playlistId) {
			url = `${pcase.environment.url}${code.ph}/playlist/${code.playlistId}${objToQuery(code.playerParams)}`;
		} else {
			url = `${pcase.environment.url}${code.ph}/${code.m}${objToQuery(code.playerParams)}`;
		}

		if(pcase.api_code != null)
			this.api_code = pcase.api_code;

		let embed;

		// if embedded player
		if (!pcase.api)
			embed = <iframe src={url} allowFullScreen width={code.width} height={code.height} scrolling="no" frameBorder="0"/>;

		if (pcase.api)
			terminal = 	<Terminal
				color='green'
				backgroundColor='black'
				barColor='black'
				hideTopBar={true}
				allowTabs={false}
				style={{ fontWeight: "bold", fontSize: "1.5em", height: "200px"}}
				commands={{
					'player.play()': this.play,
					'player.pause()': this.pause,
					'player.seek()' : this.seek,
					'player.status()': this.status,
					'player.dimLights()': this.dimLights
				}}
				descriptions={{
					'player.play': 'Toca a mídia',
					'player.pause': 'Pausa a mídia',
					'player.seek' : 'Avança a mídia para o segundo 20',
					'player.status': 'Retorna o status da mídia',
					'player.dimLights': 'Diminui as luzes'
				}}
				msg='Escreva help e descubra os métodos do nosso player'
			/>

		return (
			<div>
				<div ref="playerContainer" className="page-player">{embed}</div>
				<div className={this.state.cssControlbar}>
					<button className="btn-controls" onClick={this.play}>
						<i className="fa fa-play" aria-hidden="true"></i>
						<span className="name-button">Play</span>
					</button>
					<button className="btn-controls" onClick={this.pause}>
						<i className="fa fa-pause" aria-hidden="true"></i>
						<span className="name-button">Pause</span>
					</button>
					<button className="btn-controls" onClick={this.seek}>
						<i className="fa fa-backward" aria-hidden="true"></i>
						<i className="fa fa-forward" aria-hidden="true"></i>
						<span className="name-button">Seek to 00:20</span>
					</button>
					<button className="btn-controls" onClick={this.status}>
						<i className="fa fa-sliders" aria-hidden="true"></i>
						<span className="name-button">Player status</span>
					</button>
					<button className="btn-controls" onClick={this.dimLights} style={{zIndex:1040, position: 'relative'}}>
						<i className="fa fa-lightbulb-o" aria-hidden="true"></i>
						<span className="name-button">Apague as luzes</span>
					</button>
				</div>
				<div className="wrap-controls">
					{terminal}
				</div>
			</div>
		);
	}

	componentWillUnmount() {
		// TODO: remove all listeners
		this.player = null;
	}

	playerApiLoadHandler = () => {
		const container = this.refs.playerContainer;

		if (container == null) {
			console.error(`${this.constructor.name}`, "Player container not found.");
			return;
		}

		container.innerHTML = '';
		this.state.code.events =  { '*': this.eventListener };

		this.player = new SambaPlayer(container, this.state.code);

		if(this.api_code) this.runApiCode();

		let codeStringified = JSON.stringify(this.state.code, (key, value) => {
			if(typeof value == 'function') return 'eventListener';
			return value;
		}, '\t');

		let code = `var playerContainer = document.querySelector('.page-player');\nvar player = new SambaPlayer(playerContainer, ${codeStringified});`;
		if(this.api_code) code = `${code}\n\n${this.api_code}`;

		this.props.onSourceReady(code);
	}

	treatAndParseCode(code) {
		const parsedCode = JSON.parse(code);

		if(parsedCode.playerParams) {
			for(let k in parsedCode.playerParams) {
				if(isUrl(parsedCode.playerParams[k]) && k != 'thumbnailURL') parsedCode.playerParams[k] = `[${parsedCode.playerParams[k]}]`;
			}
		} else {
			parsedCode.playerParams = {};
		}

		function isUrl(s) {
			const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			return regexp.test(s);
		}
		return parsedCode;
	}

	eventListener = (e) => {
		const event = (typeof e.eventParam === 'object') ? JSON.stringify(e.eventParam) : (e.eventParam) ? e.eventParam : 'null';
		if(console) console.info(`Evento disparado: ${e.event} - ${event}`);
		if(e.event === 'onLoad') this.setState({cssControlbar: 'wrap-controls'});
	}
}

//	Redux
const mapStateToProps = (state, ownProps) => {
	return {
		user: state.auth,
		activeEnvironment: state.environments.active.environment,
		currentCategory: state.categories.active.category,
		activePcase: state.pcases.active
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchPcase: (id, envId, catId) => dispatch(actions.fetchPCase(id, envId, catId))
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Player);
