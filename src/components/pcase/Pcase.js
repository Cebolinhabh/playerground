import React, { Component, Fragment } from 'react';
import { Link, browserHistory, withRouter } from 'react-router-dom';
import CopyToClipboard from 'react-copy-to-clipboard';
import Highlight from 'react-highlight';
import { connect } from 'react-redux';

import * as actions from '../../store/actions';

import Select from '../inputs/SelectComponent';
import Player from './Player';
import Messages from '../UI/Messages/Messages';
import Spinner from '../UI/Spinner/Spinner';

import { addURLParameter, removeURLParameter, treatDate, objToQuery } from '../../helpers/utility';

class Pcase extends Component {

	state = {
		successIcon: 'sucess',
		errorIcon: 'error',
		favIcon: 'fa show fa-star-o',
		sourceCode: 'Loading source...',
		copyLinkURL: '',
		code: null
	}


	componentDidUpdate() {
		const { activeCategory, activeEnvironment, activePcase, match } = this.props;

		if (!activeCategory || !activeEnvironment) return;
		
		if (activePcase.pcase && activePcase.pcase.id !== +match.params.pcase_id) {
			return this.fetchPcase(match.params.pcase_id, activeEnvironment.id, activeCategory.id);
		}
	}

	componentWillReceiveProps(nextProps) {
		const { activeCategory, activeEnvironment, activePcase, match, fetchCategory } = nextProps;

		if (!activeCategory || !activeEnvironment) return;

		if (activeCategory && activeCategory.slug !== match.params.category_slug && activePcase.pcase && activePcase.pcase.id !== +match.params.pcase_id) 
			return fetchCategory(match.params.category_slug, { env: activeEnvironment.id });

		if (activePcase.error && this.props.match.params.pcase_id === match.params.pcase_id) return;

		if (!activePcase.pcase && !activePcase.loading) return this.fetchPcase(match.params.pcase_id, activeEnvironment.id, activeCategory.id);
		//if (activePcase.pcase && activePcase.pcase.id !== +match.params.pcase_id) return this.fetchPcase(match.params.pcase_id, activeEnvironment.id, activeCategory.id);
	}

	fetchPcase(id, envId, catId) {
		const { fetchPcase, user, activeEnvironment, environments } = this.props;

		fetchPcase(id, envId, catId).then(() => {
			const { pcase } = this.props.activePcase;

			this.setState({
				code: JSON.parse(pcase.code)
			});

			if (this.state.code.playerParams == null)
				this.state.code.playerParams = {};

			//Testando CLIENT
			if (user.role === 'CLIENT') {
				pcase.environment = environments.filter(env => env.name === 'prod')[0];;
			}

			this.setState({ envValue: activeEnvironment.url });
			this.treatState(pcase);
			this.generateCopyLink();
		}).catch(error => {});
	}

	sourceReadyHandler = (source) => {
		this.setState({ sourceCode: source });
	}

	onCopiedLink = () => {
		alert(`${this.state.copyLinkURL} copiado para seu clipboard!!!!`);
	}

	generateCopyLink = () => {
		const { pcase } = this.props.activePcase;
		const ph = `ph=${this.state.code.ph}`;
		const m = `&m=${this.state.code.m || ''}`;
		this.setState({
			copyLinkURL: `?${ph + m}&${objToQuery(this.state.code.playerParams, false)}`
		});
	}

	treatState = (pcase) => {
		const user_info = pcase.pcase_user_infos;

		if (user_info.length > 0) {
			if (user_info[0].favorite == true) {
				this.setState({ favIcon: 'fa show fa-star' });
			} else {
				this.setState({ favIcon: 'fa show fa-star-o' });
			}

			if (user_info[0].status === 'SUCCESS') {
				this.setState({ errorIcon: 'error', successIcon: 'sucess active' });
			} else if (user_info[0].status === 'ERROR') {
				this.setState({ errorIcon: 'error active', successIcon: 'sucess' });
			} else {
				this.setState({ errorIcon: 'error', successIcon: 'sucess' });
			}
		} else {
			this.setState({ favIcon: 'fa show fa-star-o', errorIcon: 'error', successIcon: 'sucess' });
		}
	}

	deletePase = () => {
		const { pcase } = this.props.activePcase;
		const { category_slug } = this.props.match.params;
		const { deletePcase, history, activeCategory } = this.props;
		let confirm = window.confirm(`Deseja excluir o caso ${pcase.title}?`);

		if (confirm)
			deletePcase(pcase.id).then(() => {
				history.push('/');
			});
	}

	setInfo = (e) => {
		const type = e.currentTarget.getAttribute('data-type');
		const { pcase } = this.props.activePcase;

		pcase.pcase_user_infos = (pcase.pcase_user_infos.length > 0) ?
			pcase.pcase_user_infos :
			[{ favorite: false, status: 'PENDING', category_id: pcase.category_id }]; //Melhorar isso

		if (type === 'favorite') {
			const isFav = (e.currentTarget.getAttribute('data-isfav') === 'true');
			pcase.pcase_user_infos[0].favorite = !isFav;
		} else {
			const status = e.currentTarget.getAttribute('data-status');
			pcase.pcase_user_infos[0].status = status.toUpperCase();
		}

		this.props.updatePcaseUserInfo(pcase).then(() => {
			//Atualizando Favoritos
			const { activeEnvironment, match, fetchFavPcases } = this.props;
			const name = match.params.category_slug;

			fetchFavPcases(activeEnvironment.id);
			this.treatState(pcase);
			this.props.fetchCategory(name, { env: activeEnvironment.id });
			this.props.fetchCategories({ env: activeEnvironment.id });
		});
	}

	changeEnviroment = (event) => {
		const [url, staticURL] = event.target.value.split(',');
		const { pcase } = this.props.activePcase;

		this.setState({
			envValue: event.target.value
		});
		pcase.environment = { ...pcase.environment, url, staticURL };
	}

	render() {
		const { activeEnvironment, user, activePcase, location } = this.props;
		const { pcase, loading, error } = activePcase;
		let pcaseHtml, path, urlEdit, createdAt, highlightSintaxe, envSiblingsOptions,
		descriptionClassName, isPending;

		if (loading || error || !pcase) {
			pcaseHtml = <div className="col-md-8 page-content-player" >
							<div className="page-player"></div>
							<div className="page-content-information">
								<div className="col-md-6"></div>
								<div className="col-md-12 col-sm-12 col-xs-12 publish-description">
									{error ? <Messages className="alert alert-danger" text="Erro ao requisitar o pCase. Verifique se categoria e ambiente estão corretos." /> : ''}
									{loading ? <Spinner/> : ''}
								</div>
							</div>
						</div>
		}

		if (pcase) {
			path = location.query;
			urlEdit = `/${this.props.match.params.category_slug}/pcases/${pcase.id}/edit`;
			createdAt = treatDate(pcase.created_at);
			highlightSintaxe = pcase.api ? 'javascript hide-mobile' : 'html hide-mobile';

			envSiblingsOptions = activeEnvironment.siblings.map(sibling => {
				return <option key={sibling.name} value={sibling.url + ',' + sibling.staticURL}>{sibling.name}</option>;
			});

			descriptionClassName = activeEnvironment.siblings.length > 0 ? 'col-md-7 col-sm-6 col-xs-12 publish-description' : 'col-md-12 col-sm-12 col-xs-12 publish-description';
			isPending = (pcase.pcase_user_infos[0]) ? pcase.pcase_user_infos[0].status === 'PENDING' : true;

			pcaseHtml = <div className="col-md-8 page-content-player">
							<Player onSourceReady={this.sourceReadyHandler} currentSelectedEnvironment={this.state.envValue}/>
							<div className="page-content-information">
								<div className="col-md-6 col-sm-6 col-xs-12">
									<div className="title">
										<h3>{pcase.title}</h3>
									</div>
									<span className="publish-date">
										{createdAt}
									</span>
								</div>
								{ user.role != 'CLIENT' &&
									<div className="col-md-6 col-sm-6 col-xs-12 information-options">
										<div className="more">
											<span className="icon-more"><i className="fa fa-ellipsis-v"></i></span>
											<div className="box">
												<ul>
													{(user.id == pcase.user_id) ?
														<li>
															<Link to={{
																pathname: urlEdit,
																search: path
															}}><i className="fa fa-pencil-square-o"></i> Editar
															</Link></li> : ''}
													{(user.id == pcase.user_id) ? <li><a onClick={this.deletePase} href="javascript:;"><i className="fa fa-trash"></i> Excluir</a></li> : ''}
													<CopyToClipboard text={this.state.copyLinkURL} onCopy={this.onCopiedLink}>
														<li>
															<a href="javascript:;"><i className="fa fa-scissors"></i>Copiar Link</a>
														</li>
													</CopyToClipboard>
												</ul>
											</div>
										</div>

										<div className="favoritos" data-isfav={(pcase.pcase_user_infos.length > 0) ? pcase.pcase_user_infos[0].favorite : false} onClick={this.setInfo} data-type="favorite">
											<i className={this.state.favIcon}></i>
										</div>
										<div className="wrap-status">
											<span data-type="status" data-status="success" onClick={this.setInfo} className={!isPending ? this.state.successIcon : ''}><i className="fa fa-check" aria-hidden="true"></i> Sucesso</span>
											<span data-type="status" data-status="error" onClick={this.setInfo} className={!isPending ? this.state.errorIcon : ''}><i className="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro  </span>
										</div>
									</div>
								}

								<div className={descriptionClassName}>
									<p>{pcase.description}</p>
								</div>

								{activeEnvironment.siblings.length > 0 && user.role != 'CLIENT' ? <div className="col-md-5 col-sm-6 col-xs-12ß publish-description">
									<span className="title-branch">Selecione o ambiente que deseja testar:</span>
									<div className="select-branch">
										<select value={this.state.envValue} onChange={this.changeEnviroment}>
											<option key={activeEnvironment.name} value={activeEnvironment.url + ',' + activeEnvironment.staticURL}>{activeEnvironment.name}</option>
											{envSiblingsOptions}
										</select>
									</div>
								</div> : ''}

								<hr className="col-md-12 col-sm-12 hide-mobile" />
								<Highlight className={highlightSintaxe}>{this.state.sourceCode}</Highlight>
							</div>
						</div>
		}

		return (
			<Fragment>
				{pcaseHtml}
			</Fragment>
		);
	}
};

//	Redux

const mapStateToProps = (state, ownProps) => {
	return {
		user: state.auth,
		activeEnvironment: state.environments.active.environment,
		activeCategory: state.categories.active.category,
		activePcase: state.pcases.active,
		environments: state.environments.list.environments
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		fetchPcase: (id, envId, catId) => dispatch(actions.fetchPcase(id, envId, catId)),
		deletePcase: (id) => dispatch(actions.deletePcase(id)),
		updatePcaseUserInfo: (pcase) => dispatch(actions.updatePcaseUserInfo(pcase)),
		fetchFavPcases: (envId) => dispatch(actions.fetchFavPcases({ env: envId })),
		fetchCategory: (slug, env) => dispatch(actions.fetchCategory(slug, env)),
		fetchCategories: (env) => dispatch(actions.fetchCategories(env))
	};
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Pcase));



// class PcaseComponent extends Component {
// 	constructor(props) {
// 		super(props);

// 		this.fetching = false;

// 		this.state = {
// 			html5Icon: 'fa fa-html5',
// 			flashIcon: 'fa fa-bolt',
// 			successIcon: 'sucess',
// 			errorIcon: 'error',
// 			favIcon: 'fa show fa-star-o',
// 			sourceCode: 'Loading source...',
// 			copyLinkURL: ''
// 		};
// 	}

// 	componentDidMount() {
// 		const { pcase_id, category_slug } = this.props.match.params;
// 		const { environment } = this.props.environments.active;
// 		const { category } = this.props.categories.active;

// 		if ((environment && category) && category.slug === category_slug)
// 			this.fetchPCase(pcase_id, environment.id, category.id);
// 	}

// 	componentWillReceiveProps(nextProps) {
// 		const { pcase_id, category_slug } = nextProps.match.params;
// 		const { environment } = nextProps.environments.active;
// 		const { category } = nextProps.categories.active;
// 		const { pcase } = nextProps.pcases.active;

// 		const pcaseActiveId = (pcase) ? pcase.id : null;

// 		if (!environment || !category) return;

// 		if ((pcase_id != pcaseActiveId && category.slug === category_slug) && !this.fetching) {
// 			this.fetchPCase(pcase_id, environment.id, category.id);
// 		}
// 	}

// 	shouldComponentUpdate(nextProps, nextState) {
// 		const shouldUpdate = (nextProps.pcases.active.pcase &&
// 			typeof nextProps.pcases.active.pcase.id != 'undefined') ? true : (nextProps.pcases.active.error) ? true : false;

// 		return shouldUpdate;
// 	}

// 	treatState = (pcase) => {
// 		this.treatTypeState(this.code.playerParams.html5);

// 		//Favorite and success/error
// 		const user_info = pcase.pcase_user_infos;
// 		if (user_info.length > 0) {

// 			if (user_info[0].favorite == true) {
// 				this.setState({ favIcon: 'fa show fa-star' });
// 			} else {
// 				this.setState({ favIcon: 'fa show fa-star-o' });
// 			}

// 			if (user_info[0].status === 'SUCCESS') {
// 				this.setState({ errorIcon: 'error', successIcon: 'sucess active' });
// 			} else if (user_info[0].status === 'ERROR') {
// 				this.setState({ errorIcon: 'error active', successIcon: 'sucess' });
// 			} else {
// 				this.setState({ errorIcon: 'error', successIcon: 'sucess' });
// 			}
// 		} else {
// 			this.setState({ favIcon: 'fa show fa-star-o', errorIcon: 'error', successIcon: 'sucess' });
// 		}
// 	}

// 	treatTypeState(isHtml5) {
// 		let html5Icon, flashIcon;

// 		//Html5 or flash
// 		if (isHtml5) {
// 			html5Icon = 'fa fa-html5 fa-html5-actived';
// 			flashIcon = 'fa fa-bolt fa-bolt';
// 			this.code.playerParams.html5 = true;
// 		} else {
// 			html5Icon = 'fa fa-html5';
// 			flashIcon = 'fa fa-bolt fa-bolt-actived';
// 			this.code.playerParams.html5 = false;
// 		}

// 		this.props.pcases.active.pcase.code = JSON.stringify(this.code);

// 		this.setState({ html5Icon, flashIcon });
// 	}

// 	changeEnviroment(evt) {
// 		const [url, staticURL] = evt.target.value.split(',');

// 		this.setState({
// 			envValue: evt.target.value
// 		});
// 		this.props.pcases.active.pcase.environment = { ...this.props.pcases.active.pcase.environment, url, staticURL };

// 	}

// 	/** Ajax calls **/
// 	fetchPCase(id, envId, catId) {
// 		const { user } = this.props.auth;
// 		const { fetchPCase } = this.props;
// 		const { environment } = this.props.environments.active;
// 		this.fetching = true;

// 		fetchPCase(id, envId, catId).then((res) => {
// 			const pcase = this.props.pcases.active.pcase;

// 			if (!pcase) return;

// 			this.code = JSON.parse(pcase.code);

// 			if (this.code.playerParams == null)
// 				this.code.playerParams = {};

// 			//Resetando environment
// 			let { environment } = this.props.environments.active;

// 			//Testando CLIENT
// 			if (user != null && user.role === 'CLIENT') {
// 				environment = this.props.environments.list.environments.filter(env => env.name === 'prod')[0];
// 				this.props.pcases.active.pcase.environment = environment;
// 			}

// 			if (environment) this.setState({ envValue: environment.url });

// 			this.fetching = false;

// 			this.treatState(pcase);
// 			this.generateCopyLink();
// 		});
// 	}

// 	deletePCase() {
// 		const { pcase } = this.props.pcases.active;
// 		const { category_slug } = this.props.match.params;
// 		let confirm = window.confirm(`Deseja excluir o caso ${pcase.title}?`);
// 		if (confirm)
// 			this.props.deletePCase(pcase.id).then(() => {
// 				browserHistory.push(`/${category_slug}`);
// 			});
// 	}

// 	/**Binds**/
// 	setInfo = (e) => {
// 		const type = e.currentTarget.getAttribute('data-type');
// 		const { pcase } = this.props.pcases.active;
// 		pcase.pcase_user_infos = (pcase.pcase_user_infos.length > 0) ? pcase.pcase_user_infos : [{ favorite: false, status: 'PENDING', category_id: pcase.category_id }]; //Melhorar isso

// 		if (type === 'favorite') {
// 			const isFav = (e.currentTarget.getAttribute('data-isfav') === 'true');
// 			pcase.pcase_user_infos[0].favorite = !isFav;
// 		} else {
// 			const status = e.currentTarget.getAttribute('data-status');
// 			pcase.pcase_user_infos[0].status = status.toUpperCase();
// 		}

// 		this.props.updatePCaseUserInfo(pcase).then(() => {
// 			const pcase = this.props.pcases.active.pcase;
// 			this.treatState(pcase);

// 			//Atualizando Favoritos
// 			const { environment } = this.props.environments.active;
// 			this.props.fetchFavPcases({ env: environment.id });

// 			//Atualizando a listagem de categorias
// 			const name = this.props.match.params.category_slug;
// 			this.props.fetchCategory(name, { env: environment.id });
// 			this.props.fetchCategories({ env: environment.id });
// 		});
// 	}

// 	activateVideoType = (e) => {
// 		const iEl = e.currentTarget.children[0];
// 		const type = iEl.getAttribute('data-type');
// 		const isActived = (iEl.className.indexOf('actived') > -1);

// 		isActived || this.treatTypeState(type == 'html5');

// 		return false;
// 	}

// 	generateCopyLink = () => {
// 		const { pcase } = this.props.pcases.active;
// 		const ph = `ph=${this.code.ph}`;
// 		const m = `&m=${this.code.m || ''}`;
// 		this.setState({
// 			copyLinkURL: `?${ph + m}&${objToQuery(this.code.playerParams, false)}`
// 		});
// 	}

// 	onCopiedLink = () => {
// 		alert(`${this.state.copyLinkURL} copiado para seu clipboard!!!!`);
// 	}

// 	// Populates page with current player source code.
// 	sourceReadyHandler = (source) => {
// 		this.setState({ sourceCode: source });
// 	}

// 	renderDefault() {
// 		const { error } = this.props.pcases.active;
// 		const msg = (error) ? 'Erro ao requisitar o pCase. Verifique se categoria e ambiente estão corretos.' : '';

// 		return <div className="col-md-8 page-content-player" >
// 			<div className="page-player"></div>
// 			<div className="page-content-information">
// 				<div className="col-md-6"></div>
// 				<div className="col-md-6 information-options"></div>
// 				<hr className="col-md-12" />
// 				<div className="col-md-12 col-sm-12 col-xs-12 publish-description"><p>{msg}</p></div>
// 				<textarea readOnly="true" rows="7" ></textarea>
// 			</div>
// 		</div>
// 	}

// 	render() {
// 		const { pcase, loading, error } = this.props.pcases.active;
// 		const path = this.props.location.query;

// 		if (loading || error || !pcase) { return (this.renderDefault()); }

// 		const urlEdit = `/${this.props.match.params.category_slug}/pcases/${pcase.id}/edit`;
// 		const { user } = this.props.auth;
// 		const createdAt = treatDate(pcase.created_at);
// 		const highlightSintaxe = pcase.api ? 'javascript hide-mobile' : 'html hide-mobile';

// 		const { environment } = this.props.environments.active;
// 		const envSiblingsOptions = environment.siblings.map(sibling => {
// 			return <option key={sibling.name} value={sibling.url + ',' + sibling.staticURL}>{sibling.name}</option>;
// 		});

// 		const descriptionClassName = environment.siblings.length > 0 ? 'col-md-7 col-sm-6 col-xs-12 publish-description' : 'col-md-12 col-sm-12 col-xs-12 publish-description';

// 		const isPending = (pcase.pcase_user_infos[0]) ? pcase.pcase_user_infos[0].status === 'PENDING' : true;

// 		return (
// 			<div className="col-md-8 page-content-player">
// 				<PlayerComponent branch={this.props.branches.active.branch} pcase={pcase} onSourceReady={this.sourceReadyHandler} />
// 				<div className="page-content-information">
// 					<div className="col-md-6 col-sm-6 col-xs-12">
// 						<div className="title">
// 							<h3>{pcase.title}</h3>
// 							<div className="select-type">
// 								<a onClick={this.activateVideoType} href="javascript:;"><i data-type='html5' className={this.state.html5Icon}></i></a>
// 								<a onClick={this.activateVideoType} href="javascript:;"><i data-type='flash' className={this.state.flashIcon}></i></a>
// 							</div>
// 						</div>
// 						<span className="publish-date">
// 							{createdAt}
// 						</span>
// 					</div>
// 					{user != null && user.role != 'CLIENT' &&
// 						<div className="col-md-6 col-sm-6 col-xs-12 information-options">
// 							<div className="more">
// 								<span className="icon-more"><i className="fa fa-ellipsis-v"></i></span>
// 								<div className="box">
// 									<ul>
// 										{(user.id == pcase.user_id) ? <li><Link to={urlEdit} query={path}><i className="fa fa-pencil-square-o"></i> Editar</Link></li> : ''}
// 										{(user.id == pcase.user_id) ? <li><a onClick={this.deletePCase} href="javascript:;"><i className="fa fa-trash"></i> Excluir</a></li> : ''}
// 										<CopyToClipboard text={this.state.copyLinkURL} onCopy={this.onCopiedLink}>
// 											<li><a href="javascript:;"><i className="fa fa-scissors"></i>
// 												Copiar Link</a></li>
// 										</CopyToClipboard>
// 									</ul>
// 								</div>
// 							</div>

// 							<div className="favoritos" data-isfav={(pcase.pcase_user_infos.length > 0) ? pcase.pcase_user_infos[0].favorite : false} onClick={this.setInfo} data-type="favorite">
// 								<i className={this.state.favIcon}></i>
// 							</div>
// 							<div className="wrap-status">
// 								<span data-type="status" data-status="success" onClick={this.setInfo} className={!isPending ? this.state.successIcon : ''}><i className="fa fa-check" aria-hidden="true"></i> Sucesso</span>
// 								<span data-type="status" data-status="error" onClick={this.setInfo} className={!isPending ? this.state.errorIcon : ''}><i className="fa fa-exclamation-triangle" aria-hidden="true"></i> Erro  </span>
// 							</div>
// 						</div>
// 					}

// 					<div className={descriptionClassName}>
// 						<p>{pcase.description}</p>
// 					</div>

// 					{environment.siblings.length > 0 && user != null && user.role != 'CLIENT' ? <div className="col-md-5 col-sm-6 col-xs-12ß publish-description">
// 						<span className="title-branch">Selecione o ambiente que deseja testar:</span>
// 						<div className="select-branch">
// 							<select value={this.state.envValue} onChange={this.changeEnviroment}>
// 								<option key={environment.name} value={environment.url + ',' + environment.staticURL}>{environment.name}</option>
// 								{envSiblingsOptions}
// 							</select>
// 						</div>
// 					</div> : ''}

// 					<hr className="col-md-12 col-sm-12 hide-mobile" />
// 					<Highlight className={highlightSintaxe}>{this.state.sourceCode}</Highlight>
// 				</div>
// 			</div>
// 		);
// 	}
// }

// export default PcaseComponent;
