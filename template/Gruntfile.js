module.exports = function( grunt ) {

  grunt.initConfig({

    uglify : {
      options : {
        mangle : false
      },

      my_target : {
        files : {
          'javascript/main.js' : [ 'assets/_js/main.js' ]
        }
      }
    }, // uglify



    compass: {
			dist: {
				options: {
					sassDir: 'assets/_sass/',
          cssDir:  'css/',
          sourcemap: false
				}
			}
		},//compass


    watch : {
      dist : {
        files : [
          'assets/_js/**/*',
          'assets/_sass/**/*'
        ],

        tasks : [ 'uglify' , 'compass'],

        options: {
          livereload: true,
        },
      }
    } // watch

  });


  // Plugins do Grunt
  grunt.loadNpmTasks( 'grunt-contrib-uglify' );
  //grunt.loadNpmTasks( 'grunt-contrib-sass' );
  //grunt.loadNpmTasks('grunt-livereload');
  grunt.loadNpmTasks( 'grunt-contrib-watch' );
  grunt.loadNpmTasks( 'grunt-contrib-compass' );


  // Tarefas que serão executadas
  grunt.registerTask( 'default', [ 'uglify', 'compass' ] );

  // Tarefa para Watch
  grunt.registerTask( 'w', [ 'watch' ] );

};
