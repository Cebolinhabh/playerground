'use strict';

import Code from 'code';
import Lab from 'lab';

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const before = lab.before;
const after = lab.after;
const expect = Code.expect;

//Testing server and model
import server from '../../server';
import { sequelize } from '../../api/models';

//Saved cookie
let cookie;

before((done) => [
	server.on('start', () => {
		done();
	})
]);

describe('Static routes', () => {

	it('Get default index', (done) => {
		const options = {method: 'GET', url: '/'};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(response.headers['content-type']).to.equal('text/html; charset=utf-8');
			done();
		});
	});

	it('Get wrong route', (done) => {
		const options = {method: 'GET', url: '/wrongroute/wrongroute'};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(404);
			done();
		});
	});

	it('Get asset', (done) => {
		const options = {method: 'GET', url: '/assets/styles/font-awesome/font-awesome.css'};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(response.headers['content-type']).to.equal('text/css; charset=utf-8');
			done();
		});
	});

});

describe('User routes', () => {

	it('Create new user', (done) => {
		const options = {
			method: 'POST',
			url: '/v1/users',
			payload: {
				name: 'Thiago Miranda',
				email: 'thiagomir@gmail.com',
				password: 'thiago123'
			}

		};

		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(result.name).to.equal(options.payload.name);
			expect(result.email).to.equal(options.payload.email);
			expect(result.password).to.equal(options.payload.password);
			done();
		});
	});

	it('Get users', (done) => {
		const options = {method: 'GET', url: '/v1/users'};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(result.length).to.equal(1);
			done();
		});

	});

});

describe('Auth routes', () => {
	it('Login with valid user', (done) => {
		const options = {
			method: 'POST',
			url: '/v1/login',
			payload: {
				email: 'thiagomir@gmail.com',
				password: 'thiago123'
			}
		};

		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(result.success).to.equal(true);
			expect(response.headers['set-cookie']).to.exist();
			cookie = response.headers['set-cookie'][0].match(/(?:[^\x00-\x20\(\)<>@\,;\:\\"\/\[\]\?\=\{\}\x7F]+)\s*=\s*(?:([^\x00-\x20\"\,\;\\\x7F]*))/);
			done();
		});
	});

	it('Access only logged route', (done) => {
		const options = {
			method: 'GET',
			url: '/v1/favorites',
			headers: {
				cookie: cookie[0]
			}
		};

		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	it('Login with non valid user', (done) => {
		const options = {
			method: 'POST',
			url: '/v1/login',
			payload: {
				email: 'thiagomir1@gmail.com',
				password: 'thiago123'
			}
		};

		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(401);
			expect(result.error).to.equal('Unauthorized');
			expect(response.headers['set-cookie']).to.not.exist();
			done();
		});

	});

	it('Logout', (done) => {
		const options = {
			method: 'GET',
			url: '/v1/logout',
			headers: {
				cookie: cookie[0]
			}
		};

		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(result.success).to.equal(true);
			const logoutCookie = response.headers['set-cookie'][0].match(/(?:[^\x00-\x20\(\)<>@\,;\:\\"\/\[\]\?\=\{\}\x7F]+)\s*=\s*(?:([^\x00-\x20\"\,\;\\\x7F]*))/);
			expect(logoutCookie[0]).to.equal('app-cookie=');
			done();
		});

	});

});

describe('Favorites routes', () => {

	it('Put favorite location', (done) => {
		const options = {
			method: 'PUT',
			url: '/v1/favorites',
			payload: {
				url: 'http://google.com',
				query: '192.168.0.1',
				country: 'Brazil',
				region: 'MG',
				city: 'Belo Horizonte',
				timezone: 'GMT-03:00',
				lat: 54.12323,
				lon: 67.1512
			},
			headers: {
				cookie: cookie[0]
			}

		};

		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(result.url).to.equal(options.payload.url);
			expect(result.query).to.equal(options.payload.query);
			expect(result.country).to.equal(options.payload.country);
			expect(result.region).to.equal(options.payload.region);
			expect(result.city).to.equal(options.payload.city);
			expect(result.timezone).to.equal(options.payload.timezone);
			expect(result.lat).to.equal(options.payload.lat);
			expect(result.lon).to.equal(options.payload.lon);
			done();
		});
	});

	it('Get favorites by logged user', (done) => {
		const options = {method: 'GET', url: '/v1/favorites', headers: {cookie: cookie[0]}};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(result.length).to.equal(1);
			done();
		});

	});

	it('Get favorites by user not logged', (done) => {
		const options = {method: 'GET', url: '/v1/favorites'};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(401);
			done();
		});
	});

	it('Get favorites by user with wrong cookie', (done) => {
		const options = {method: 'GET', url: '/v1/favorites', headers: {cookie: 'app-cookie=test123'}};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(401);
			done();
		});
	});

	it('Delete favorite', (done) => {
		const options = {method: 'DELETE', url: '/v1/favorites/1', headers: {cookie: cookie[0]}};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			done();
		});
	});

	it('Get favorites after deletion', (done) => {
		const options = {method: 'GET', url: '/v1/favorites', headers: {cookie: cookie[0]}};
		server.inject(options, (response) => {
			const result = response.result;
			expect(response.statusCode).to.equal(200);
			expect(result.length).to.equal(0);
			done();
		});
	});


	lab.test('cleanups after test', (done, onCleanup) => {

	    onCleanup((next) => {
	    	sequelize.sync({force: true}).then(() => {
	    		return next();
	    	});
	    });

	    done();
	});
});

