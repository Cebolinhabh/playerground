# Tutorial de instalação #
1. Clone o projeto
2. Certifique-se do node v5.x.x+ instalado
3. Execute o `npm install` no diretório do projeto
4. Para criar mocks execute o comando `npm run db` ( aperte Y no prompt )
5. Rode o servidor em development com `npm start`

# Para subir o projeto

### Passo-a-passo
- [Subir localmente](https://github.com/sambatech/devandtestplayer/blob/playerground/README.md)
- Popular o banco:
`npm run db:prod` (rodar somente 1x)
- Associar aplicação já existente:
`eb create`
  - **AZ:** us-west-2b (Oregon)
  - **App:** Playerground
- Efetuar o deploy:
`npm run deploy`
### EC2
- **Name:** Playerground
- **Type:**  t2.small
- **ID:** i-0152cdb7e7126e302
- **SG:** awseb-e-mhpyshpptq-stack-AWSEBSecurityGroup-KMA0YHIK9YMR (sg-890d99f1)
- **Role:** aws-elasticbeanstalk-ec2-role
- **AZ:** us-west-2b (Oregon)
### RDS
- **Name:** playerground
- **SG:** rds-launch-wizard-3 (sg-a142c8d9)
- **AZ:** us-west-2b (Oregon)
### Monitoramento (PM2/Nodejs)
- https://app.keymetrics.io/#/bucket/5898ac1e65b72c980851aab0/dashboard

# Compatibilidade #

Desktop: Compatível com as versões mais atualizadas do Safari, Chrome, Firefox e Edge. Também é compatível com IE9+.
Mobile: Compatível com Chrome, Firefox e Safari Mobile. Testes efetuados no Android e iOS.

# Acesso #
Para controle de criação de pcases é necessário logar com o Github.
Após logar no Github e queira logar via email e senha entre em contato com o Thiago Miranda.

Login de usuário cliente padrão ( para uso público ):

**Login:** cliente@sambatech.com.br<br>
**Senha:** cl13nt3s4mb4



Endereço origem:
http://sample-env.4dyimdftmp.us-west-2.elasticbeanstalk.com/

Endereço final:
http://playerground.sambatech.com/

# Issue original #
https://github.com/sambatech-desenv/player/issues/541

# Nova estrutura

### Containers

Basicamente cada rota tem como elemento "main" um container

### Componentes

Elementos reaproveitáveis e/ou indepentende de rotas
