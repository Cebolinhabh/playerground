import {models, sequelize} from '../server/api/models';
import prompt from 'prompt';

prompt.start();

prompt.get({
	properties: {
		syncDb: {
			description: 'Deseja criar uma nova database? Isso destruirá todos seus dados existentes, deseja continuar? (y/N)'
		},
		createEssential: {
			description: 'Deseja criar ambientes e branchs default ( recomendável )? (y/N)'
		},
		createMock: {
			description: 'Deseja criar mocks ( não aconselhável em produção )? (y/N)' 	
		},
		runUpdates: {
			description: 'Deseja rodar os updates ( último dia 13/08/2017 )? (y/N)'
		}
	}
}, (err, result) => {
	const resSync = (result.syncDb) ? (result.syncDb.toLowerCase() === 'y' || result.syncDb.toLowerCase() === 'yes') : false;  
	const resMock = (result.createMock) ? (result.createMock.toLowerCase() === 'y' || result.createMock.toLowerCase() === 'yes') : false;
	const resEssential = (result.createEssential) ? (result.createEssential.toLowerCase() === 'y' || result.createEssential.toLowerCase() === 'yes') : false;
	if(resSync && resEssential && resMock) {
		syncDatabase(createEssential, createMocks);
	}else if(resSync && resEssential){
		syncDatabase(createEssential);
	}else if(resSync){
		syncDatabase(() => process.exit());
	}else if(runUpdates) {
		syncDatabaseWithoutForce(runUpdates);
	}else {
		process.exit();
	}
});

/** Conectando ao banco e sincronizando **/
function syncDatabase(cb, cbmocks) {
	sequelize.sync({force: true}).then(() => {
		console.log('Criando tabelas...');
		if(cb) cb(cbmocks);
	});
}

/** Conectando database sem force */
function syncDatabaseWithoutForce(cb) {
	sequelize.sync({force: false}).then(() => {
		console.log('Conectando no banco sem force');
		if(cb) cb();
	});
}

/** Construindo o básico **/
function createEssential(cbmocks) {
	/** Environment **/
	const Environment = models.environment;
	const environmentArray = [
		{
			name: 'web1-13000',
			url: '//web1.qa.sambatech.com:13000/embed/',
			isDefault: true,
			staticURL: '//player.sambatech.com.br/v3_dev/samba.player.api.js?iframeURL=web1-13000'
		},
		{
			name: 'staging',
			url: '//staging-player-api.sambavideos.sambatech.com/v3/embed/',
			isDefault: false,
			staticURL: '//player.sambatech.com.br/v3_staging/samba.player.api.js?iframeURL=staging'
		},
		{
			name: 'prod',
			url: '//fast.player.liquidplatform.com/pApiv2/embed/',
			isDefault: false,
			staticURL: '//player.sambatech.com.br/v3/samba.player.api.js'
		}	
	];

	Environment.bulkCreate(environmentArray).then(() => {console.log('Ambientes criados!!!')});	

	/** Branchs **/
	const Branch = models.branch;
	const branchArray = [
		{
			name: 'master',
			gitURL: 'https://github.com/sambatech-desenv/player',
			status: 'PENDING',
			isDefault: true
		}
	];

	Branch.bulkCreate(branchArray).then(() => {console.log('Branches criados!!!')});
	
	cbmocks && cbmocks();
}

/** Mocks de usuários e pcases **/
function createMocks() {
		/** Users **/
		const User = models.user;
		const usersArray = [
			{
				name: 'Leandro Zanol',
				email: 'leandro.zanol@sambatech.com.br',
				password: 'samba456',
				role: 'SUPPORT'
			},
			{
				name: 'Priscila Magalhães',
				email: 'priscila.magalhaes@sambatech.com.br',
				password: 'samba789',
				role: 'ADMIN'
			}	
		];

		User.bulkCreate(usersArray).then(() => {console.log('Users criados!!!')});

		/** Categories **/
		const Category = models.category;
		const categoryArray = [
			{
				name: 'Advertising',
				description: 'Casos com propaganda',
				type: 'ADMIN'
			},
			{
				name: 'Overlay',
				description: 'Casos com overlay',
				type: 'ADMIN'
			},
			{
				name: 'HLS',
				description: 'Casos HLS',
				type: 'ADMIN'
			},
			{
				name: 'Live',
				description: 'Casos live',
				type: 'ADMIN'
			},
			{
				name: 'Live Rail',
				description: 'Casos com propaganda live rail',
				type: 'ADMIN'
			},
			{
				name: 'Suporte',
				description: 'Casos do suporte type SUPPORT',
				type: 'SUPPORT'
			}	
		];

		Category.bulkCreate(categoryArray).then(() => {console.log('Categorias criadas!!!')});


		/** Browsers **/
		const Browser = models.browser;
		const browserArray = [
			{
				name: 'Google Chrome',
				mobile: false,
				icon: 'chrome'
			},
			{
				name: 'Internet Explorer 10',
				mobile: false,
				icon: 'ie'
			},
			{
				name: 'Firefox',
				mobile: false,
				icon: 'firefox'
			},
			{
				name: 'Google Chrome',
				mobile: true,
				icon: 'chrome'
			}
		];

		Browser.bulkCreate(browserArray).then(() => {console.log('Browsers criados!!!')});

		/** PCase **/
		const Pcase = models.pcase;
		const pcaseArray = [
			{
				title: 'Teste IMA3',
				description: 'Testando IMA3',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 1,
				category_id: 1,
				user_id: 1
			},
			{
				title: 'Teste Live Rail',
				description: 'Teste liverail pre roll ',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 1,
				category_id: 2,
				user_id: 2
			},
			{
				title: 'Teste HLS Wowza',
				description: 'Teste das novas definições Wowza HLS',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2,3',
				environment_id: 2,
				category_id: 3,
				user_id: 2
			},
			{
				title: 'Teste HLS Akamai',
				description: 'Testando AES Akamai HLS',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 1,
				category_id: 3,
				user_id: 1
			},
			{
				title: 'Teste Live SBT',
				description: 'Live de desenho SBT',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 3,
				category_id: 4,
				user_id: 1
			},
			{
				title: 'Teste com overlay',
				description: 'Não sei o que é overlay',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 1,
				category_id: 2,
				user_id: 2
			},
			{
				title: 'Teste com overlay mini',
				description: 'To com sono e não sei o que é overlay',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 1,
				category_id: 2,
				user_id: 1
			},
			{
				title: 'Teste com áudio HLS',
				description: 'Teste com o áudio hls',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 4,
				category_id: 3,
				user_id: 2
			},
			{
				title: 'Teste com Live Dinamarca',
				description: 'Teste com o live dinamarques',
				code: '{"width":640,"height":480,"ph":"18f78d75bc3bbb24054da7ea80633b0e","m":"eca15e688910ec384bc8bce8f59db420"}',
				api: false,
				browsers: '1,2',
				environment_id: 2,
				category_id: 4,
				user_id: 2
			}					
		];

		Pcase.bulkCreate(pcaseArray, {returning: true}).then(() => {console.log('Pcases criados!!!')});
}

/** Rodando os updates **/
function runUpdates() {
	//slugifyCategories();
	//addEnvSibling();
	addClientUser();

	/**
	 * Add client user
	 */
	function addClientUser() {
		const User = models.user;
		const user = {
				name: 'Cliente Sambatech',
				email: 'cliente@sambatech.com.br',
				password: 'cl13nt3s4mb4',
				role: 'CLIENT',
				github_id: 695559,
				avatar_url: 'https://www.shareicon.net/data/256x256/2016/09/05/825195_miscellaneous_512x512.png'
		}
		User.create(user);
	}

	/**
	 * Add sibling environment
	 */
	function addEnvSibling() {
		const Environment = models.environment;

		Environment.findOne({
			where: {
				id: 3
			},
			include: {
				model: Environment,
				as: 'siblings',
				through: {
					attributes: []
				}
			}
		}).then((env) => {
			Environment.findOne({
				where: {
					id: 4
				},
				include: {
				model: Environment,
				as: 'siblings',
				through: {
					attributes: []
				}
			},
			}).then((env1) => {
				//env.addSibling(env1);
				//env1.addSibling(env);
			})
		});
	}

	/**
	 * Slugify categories
	 */
	function slugifyCategories() {
		const Category = models.category;
		Category.findAll({}).then(categories => {
			categories.forEach((cat) => {
				const slugName = makeSlug(cat.name);
				Category.update({slug: slugName}, {
					where: {
						id: cat.id
					}
				});
			});
		});

		function makeSlug(val, replaceBy) {
			let replaceByParam = replaceBy || '-';
			const mapaAcentosHex 	= { 
				a : /[\xE0-\xE6]/g,
				A : /[\xC0-\xC6]/g,
				e : /[\xE8-\xEB]/g, 
				E : /[\xC8-\xCB]/g, 
				i : /[\xEC-\xEF]/g, 
				I : /[\xCC-\xCF]/g, 
				o : /[\xF2-\xF6]/g, 
				O : /[\xD2-\xD6]/g,
				u : /[\xF9-\xFC]/g,
				U : /[\xD9-\xDC]/g,
				c : /\xE7/g,
				C : /\xC7/g,
				n : /\xF1/g,
				N : /\xD1/g,
			};
			
			for ( let letra in mapaAcentosHex ) {
				const expressaoRegular = mapaAcentosHex[letra];
				val = val.replace( expressaoRegular, letra );
			}
			
			val = val.toLowerCase();
			val = val.replace(/[^a-z0-9\-]/g, " ");
			
			val = val.replace(/ {2,}/g, " ");
				
			val = val.trim();    
			val = val.replace(/\s/g, replaceByParam);
			
			return val;
		}
	}

}
