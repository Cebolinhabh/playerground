'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

(function () {
    var MainController = (function () {
        function MainController($http, $scope, $stateParams) {
            _classCallCheck(this, MainController);

            this.id = $stateParams.id;
            this.$http = $http;
            this.awesomeThings = [];
            this.pcase = {};

            $scope.$on('$destroy', function () {});

            this.getCases();
        }

        _createClass(MainController, [{
            key: 'getCases',
            value: function getCases() {}
        }, {
            key: 'selectCase',
            value: function selectCase(current) {
                this.pcase = current;
            }
        }]);

        return MainController;
    })();

    angular.module('devandtestplayerApp').controller('MainController', MainController);
})();
//# sourceMappingURL=main.controller.js.map
