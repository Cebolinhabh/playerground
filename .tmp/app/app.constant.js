"use strict";

(function (angular, undefined) {
	angular.module("devandtestplayerApp.constants", []).constant("appConfig", {
		"userRoles": ["guest", "user", "admin"]
	});
})(angular);
//# sourceMappingURL=app.constant.js.map
