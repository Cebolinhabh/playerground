'use strict';

(function () {

    function CategoryFactory($http) {

        return {
            getCategories: function getCategories() {
                return $http.get('/api/categorys').success(function (response) {
                    return response.data;
                });
            },
            getCategoryById: function getCategoryById(id) {
                return $http.get('/api/categorys/' + id).success(function (response) {
                    return response.data;
                });
            },
            filterCategoryByName: function filterCategoryByName(name, categories) {
                var index = 0;
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = categories[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var cat = _step.value;

                        cat.index = index;
                        if (cat.name === name) return cat;
                        index++;
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator['return']) {
                            _iterator['return']();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            },
            getCurrentCategoryIndex: function getCurrentCategoryIndex(name, categories) {
                var index = 0;
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = categories[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var cat = _step2.value;

                        if (cat.name === name) return index;
                        index++;
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2['return']) {
                            _iterator2['return']();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            },
            makeIndex: function makeIndex(name, categories) {
                var index = 0;
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                    for (var _iterator3 = categories[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                        var cat = _step3.value;

                        cat.index = index;
                    }
                } catch (err) {
                    _didIteratorError3 = true;
                    _iteratorError3 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion3 && _iterator3['return']) {
                            _iterator3['return']();
                        }
                    } finally {
                        if (_didIteratorError3) {
                            throw _iteratorError3;
                        }
                    }
                }

                return categories;
            }
        };
    }

    angular.module('devandtestplayerApp').factory('category', CategoryFactory);
})();
//# sourceMappingURL=category.factory.js.map
