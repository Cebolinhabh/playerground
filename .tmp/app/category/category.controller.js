'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

(function () {
    var CategoryController = (function () {
        function CategoryController($scope, $state, $stateParams, category) {
            var _this = this;

            _classCallCheck(this, CategoryController);

            this.$state = $state;
            this.categoryFactory = category;
            this.cName = $stateParams.category;
            this.category = {};
            this.categories = [];
            this.currentCatIndex;
            this.getCategories();

            $scope.$watch(function () {
                return _this.currentCatIndex;
            }, function (newValue, oldValue) {
                if (newValue !== undefined) _this.$state.go('category', { category: _this.category.name }, { notify: false, location: "replace" });
            });
        }

        _createClass(CategoryController, [{
            key: 'getCategories',
            value: function getCategories() {
                var _this2 = this;

                return this.categoryFactory.getCategories().success(function (data) {
                    _this2.categories = _this2.categoryFactory.makeIndex(_this2.cName, data);
                    var cat = _this2.categoryFactory.filterCategoryByName(_this2.cName, _this2.categories);

                    _this2.getCurrentCategory(cat._id).then(function () {
                        _this2.currentCatIndex = cat.index;
                    });
                });
            }
        }, {
            key: 'getCurrentCategory',
            value: function getCurrentCategory(id) {
                var _this3 = this;

                return this.categoryFactory.getCategoryById(id).success(function (data) {
                    _this3.category = data;
                });
            }
        }, {
            key: 'prevCategory',
            value: function prevCategory() {
                var _this4 = this;

                var prevIndex = this.currentCatIndex - 1;
                if (this.categories[prevIndex]) {
                    this.getCurrentCategory(this.categories[prevIndex]._id).then(function () {
                        _this4.currentCatIndex = prevIndex;
                    });
                } else {
                    this.getCurrentCategory(this.categories[this.categories.length - 1]._id).then(function () {
                        _this4.currentCatIndex = _this4.categories.length;
                    });
                }
            }
        }, {
            key: 'nextCategory',
            value: function nextCategory() {
                var _this5 = this;

                var nextIndex = this.currentCatIndex + 1;
                if (this.categories[nextIndex]) {
                    this.getCurrentCategory(this.categories[nextIndex]._id).then(function () {
                        _this5.currentCatIndex = nextIndex;
                    });
                } else {
                    this.getCurrentCategory(this.categories[0]._id).then(function () {
                        _this5.currentCatIndex = 0;
                    });
                }
            }
        }]);

        return CategoryController;
    })();

    angular.module('devandtestplayerApp').controller('CategoryCtrl', CategoryController);
})();
//# sourceMappingURL=category.controller.js.map
