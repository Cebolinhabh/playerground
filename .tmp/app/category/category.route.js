'use strict';

angular.module('devandtestplayerApp').config(function ($stateProvider) {
  $stateProvider.state('category', {
    url: '/:category/',
    templateUrl: 'app/category/category.html',
    controller: 'CategoryCtrl',
    controllerAs: 'catCtrl'
  });
});
//# sourceMappingURL=category.route.js.map
