'use strict';

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

(function () {
    var PcaseController = (function () {
        function PcaseController($scope, $stateParams, pcase) {
            _classCallCheck(this, PcaseController);

            this.id = $stateParams.id;
            this.pcase = {};
            this.pcaseFactory = pcase;
            this.baseUrl = 'http://playerapitest2.liquidplatform.com:7091/embed/';

            $scope.$on('$destroy', function () {});

            this.getCases();
        }

        _createClass(PcaseController, [{
            key: 'getCases',
            value: function getCases() {
                var _this = this;

                this.pcaseFactory.getCaseById(this.id).success(function (data) {
                    _this.pcase = data;
                    _this.pcase.url = _this.baseUrl + _this.pcase.CaseParam.url;
                });
            }
        }]);

        return PcaseController;
    })();

    angular.module('devandtestplayerApp').controller('PcaseCtrl', PcaseController);
})();
//# sourceMappingURL=pcase.controller.js.map
