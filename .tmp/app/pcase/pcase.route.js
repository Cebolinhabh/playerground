'use strict';

angular.module('devandtestplayerApp').config(function ($stateProvider) {
  $stateProvider.state('category.pcase', {
    url: ':id/:branch/',
    templateUrl: 'app/pcase/pcase.html',
    controller: 'PcaseCtrl',
    controllerAs: 'pcCtrl'
  });
});
//# sourceMappingURL=pcase.route.js.map
