'use strict';

(function () {

    function PcaseCategory($http) {

        return {
            getCases: function getCases() {
                return $http.get('/api/cases').success(function (response) {
                    return response.data;
                });
            },
            getCaseById: function getCaseById(pcaseId) {
                return $http.get('/api/cases/' + pcaseId).success(function (response) {
                    return response.data;
                });
            },
            getCaseByUser: function getCaseByUser() {
                return $http.get('/api/cases', { params: { userId: userId } }).success(function (response) {
                    return response.data;
                });
            },
            getCaseByUserAndCategory: function getCaseByUserAndCategory(userId, categoryId) {
                return $http.get('/api/cases', { params: { userId: userId, categoryId: categoryId } }).success(function (response) {
                    return response.data;
                });
            }
        };
    }

    angular.module('devandtestplayerApp').factory('pcase', PcaseCategory);
})();
//# sourceMappingURL=pcase.factory.js.map
