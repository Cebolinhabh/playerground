'use strict';

angular.module('devandtestplayerApp', ['devandtestplayerApp.auth', 'devandtestplayerApp.admin', 'devandtestplayerApp.constants', 'ngCookies', 'ngResource', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'validation.match']).config(function ($urlRouterProvider, $locationProvider, $sceProvider) {
  $urlRouterProvider.otherwise('/');

  //$locationProvider.html5Mode(true);

  //Disable check
  $sceProvider.enabled(false);
});
//# sourceMappingURL=app.js.map
