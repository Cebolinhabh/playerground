"use strict";

var parent, ink, d, x, y;$(".gmd-waves").click(function (e) {
  parent = $(this), 0 == parent.find(".ink").length && parent.prepend("<span class='ink'></span>"), ink = parent.find(".ink"), ink.removeClass("animate"), ink.height() || ink.width() || (d = Math.max(parent.outerWidth(), parent.outerHeight()), ink.css({ height: d, width: d })), x = e.pageX - parent.offset().left - ink.width() / 2, y = e.pageY - parent.offset().top - ink.height() / 2, ink.css({ top: y + "px", left: x + "px" }).addClass("animate");
});
//# sourceMappingURL=beework.js.map
