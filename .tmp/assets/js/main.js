'use strict';

$(document).ready(function () {
  $('.icon').on('click', function () {
    $(this).toggleClass('active');
  });
});

//Script para o menu lateral
$(document).ready(function () {
  $('.control').click(function () {
    $('body').addClass('mode-search');
    $('.input-search').focus();
  });

  $('.icon-close').click(function () {
    $('body').removeClass('mode-search');
  });

  $(".menu-is-close").click(function () {
    $(".wrap-menu").toggleClass("actived");
    $("body").toggleClass("menu-open");
    $(".menu-fade").toggleClass("actived");
    $("#icon-menu").removeClass("menu-is-close");
    $("#icon-menu").toggleClass("actived");
  });

  $(".menu-fade").click(function () {
    $(".wrap-menu").removeClass("actived");
    $("body").removeClass("menu-open");
    $(".menu-fade").removeClass("actived");
    $("#icon-menu").removeClass("actived");
    $(".icon").removeClass('active');
  });
});

//MENU dropdown
$(document).ready(function () {
  $("li.menu-dropdown").click(function () {
    $(this).toggleClass("actived");
  });
});

//Open modal Login
$(".open-modal-login").on("click", function () {
  $(".fade-top").toggleClass("actived");
  $(".popup-cadastro").toggleClass("actived");
  $("body").toggleClass("menu-open");
});

$(".icon-close-cadastro").on("click", function () {
  $(".fade-top").toggleClass("actived");
  $(".popup-cadastro").toggleClass("actived");
  $("body").toggleClass("menu-open");
});

$(".fade-top").on("click", function () {
  $(".fade-top").removeClass("actived");
  $(".popup-cadastro").removeClass("actived");
  $("body").removeClass("menu-open");
});

//BTN login loading
$(".btn-login").click(function (e) {
  e.preventDefault();
  $(".fade-interno").addClass("actived");
  $(".group-loading").addClass("actived");

  setTimeout(function () {
    $(".fade-interno").removeClass("actived");$(".group-loading").removeClass("actived");
  }, 5000);
});
//# sourceMappingURL=main.js.map
