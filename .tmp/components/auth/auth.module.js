'use strict';

angular.module('devandtestplayerApp.auth', ['devandtestplayerApp.constants', 'devandtestplayerApp.util', 'ngCookies', 'ui.router']).config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
});
//# sourceMappingURL=auth.module.js.map
