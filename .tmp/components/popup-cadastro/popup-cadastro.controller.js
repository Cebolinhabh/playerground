'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var PopupCadastroController =
//end-non-standard

function PopupCadastroController(Auth) {
  _classCallCheck(this, PopupCadastroController);

  this.menu = [{
    'title': 'Home',
    'state': 'main'
  }];
  this.isCollapsed = true;

  this.isLoggedIn = Auth.isLoggedIn;
  this.isAdmin = Auth.isAdmin;
  this.getCurrentUser = Auth.getCurrentUser;
};

angular.module('devandtestplayerApp').controller('PopupCadastroController', PopupCadastroController);

//start-non-standard
//# sourceMappingURL=popup-cadastro.controller.js.map
