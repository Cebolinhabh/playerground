'use strict';

angular.module('devandtestplayerApp').directive('popupCadastro', function () {
  return {
    templateUrl: 'components/popup-cadastro/popup-cadastro.html',
    restrict: 'E',
    controller: 'PopupCadastroController',
    controllerAs: 'popupCadastro',
    replace: true
  };
});
//# sourceMappingURL=popup-cadastro.directive.js.map
