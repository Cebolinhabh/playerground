'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var HeaderController =
//end-non-standard

function HeaderController(Auth) {
  _classCallCheck(this, HeaderController);

  this.menu = [{
    'title': 'Home',
    'state': 'main'
  }];
  this.isCollapsed = true;

  this.isLoggedIn = Auth.isLoggedIn;
  this.isAdmin = Auth.isAdmin;
  this.getCurrentUser = Auth.getCurrentUser;
};

angular.module('devandtestplayerApp').controller('HeaderController', HeaderController);

//start-non-standard
//# sourceMappingURL=header.controller.js.map
