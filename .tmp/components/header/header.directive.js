'use strict';

angular.module('devandtestplayerApp').directive('header', function () {
  return {
    templateUrl: 'components/header/header.html',
    restrict: 'E',
    controller: 'HeaderController',
    controllerAs: 'header'
  };
});
//# sourceMappingURL=header.directive.js.map
