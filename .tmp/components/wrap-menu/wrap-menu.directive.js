'use strict';

angular.module('devandtestplayerApp').directive('wrapMenu', function () {
  return {
    templateUrl: 'components/wrap-menu/wrap-menu.html',
    restrict: 'E',
    controller: 'WrapMenuController',
    controllerAs: 'wrapMenu',
    replace: true
  };
});
//# sourceMappingURL=wrap-menu.directive.js.map
