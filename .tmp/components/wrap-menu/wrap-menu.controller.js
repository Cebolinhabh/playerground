'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var WrapMenuController =
//end-non-standard

function WrapMenuController(Auth) {
  _classCallCheck(this, WrapMenuController);

  this.menu = [{
    'title': 'Home',
    'state': 'main'
  }];
  this.isCollapsed = true;

  this.isLoggedIn = Auth.isLoggedIn;
  this.isAdmin = Auth.isAdmin;
  this.getCurrentUser = Auth.getCurrentUser;
};

angular.module('devandtestplayerApp').controller('WrapMenuController', WrapMenuController);

//start-non-standard
//# sourceMappingURL=wrap-menu.controller.js.map
